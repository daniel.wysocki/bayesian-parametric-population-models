import numpy

def populate_subparser(subparsers):
    subparser = subparsers.add_parser("plot_observations")

    subparser.add_argument("plot_filename")

    # Can accept event likelihoods in multiple ways.  Exactly one must be
    # provided.
    event_likelihood_group = subparser.add_mutually_exclusive_group(
        required=True,
    )
    event_likelihood_group.add_argument(
        "--event-likelihoods-gp",
        nargs="+",
        help="List of Gaussian process files describing each event's "
             "likelihood."
    )
    event_likelihood_group.add_argument(
        "--event-likelihoods-rescaled-gp",
        nargs="+",
        help="List of files.  The first is a Gaussian process file, describing "
             "a reference event's likelihood.  The rest are JSON files with "
             "the fields 'shift' and 'scale', by which we transform the "
             "reference event's likelihood."
    )

    subparser.add_argument("--with-spin", action="store_true")

    subparser.add_argument("--seed", type=int)

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    from .detection import load_detections
    from ....astro_models.detections.cbc.gaussian import CBCGaussianDetection

    import itertools

    import os
    import glob
    import warnings

    import numpy
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt

    random_state = numpy.random.RandomState(cli_args.seed)

    # Load in the observed data
    if cli_args.event_likelihoods_gp is not None:
        likelihood_args = cli_args.event_likelihoods_gp
        likelihood_mode = "gp"
    elif cli_args.event_likelihoods_rescaled_gp is not None:
        likelihood_args = cli_args.event_likelihoods_rescaled_gp
        likelihood_mode = "gp_rescaled"
    detections = load_detections(likelihood_args, mode=likelihood_mode)

    colors = itertools.cycle("C{}".format(i) for i in range(10))

    nrows = 2
    if cli_args.with_spin:
        ncols = 2
        figsize = (10, 10)
    else:
        ncols = 1
        figsize = (6, 10)

    fig, axes = plt.subplots(
        nrows=nrows, ncols=ncols, sharex="col", sharey="row",
        figsize=figsize,
    )

    if cli_args.with_spin:
        ((ax_mceta, ax_chieta), (ax_mclambda, ax_chilambda)) = axes
    else:
        ax_mceta, ax_mclambda = axes

    for detection in detections:
        color = next(colors)

        # Compute grids
        observables, grids, steps = make_mesh(detection, cli_args.with_spin)

        # Compute the likelihood on the entire grid
        like = detection.likelihood([
            obs.flatten() for obs in observables
        ]).reshape(observables[0].shape)

        # Compute marginals for (mc, eta) and (mc, lambdaTilde)
        marg_like_mc_eta = numpy.trapz(like, dx=steps["LambdaTilde"], axis=2)
        marg_like_mc_lam = numpy.trapz(like, dx=steps["eta"], axis=1)

        # Compute marginals for (mc, eta) and (mc, lambdaTilde)
        marg_like_mc_eta = numpy.trapz(like, dx=steps["LambdaTilde"], axis=-1)
        marg_like_mc_lam = numpy.trapz(like, dx=steps["eta"], axis=1)
        if cli_args.with_spin:
            # Still need to marginalize out spin.
            marg_like_mc_eta = (
                numpy.trapz(marg_like_mc_eta, dx=steps["chieff"], axis=-1)
            )
            marg_like_mc_lam = (
                numpy.trapz(marg_like_mc_lam, dx=steps["chieff"], axis=-2)
            )

            # Also compute marginals for (chieff, eta) and (chieff, lambdaTilde)
            marg_like_chi_eta = numpy.trapz(
                numpy.trapz(like, dx=steps["LambdaTilde"], axis=-1),
                dx=steps["mc"], axis=0,
            )
            marg_like_chi_lam = numpy.trapz(
                numpy.trapz(like, dx=steps["eta"], axis=1),
                dx=steps["mc"], axis=0,
            )

        ax_mceta.contour(
            grids["mc"], grids["eta"], marg_like_mc_eta.T, 10,
            colors=color, linestyles="solid",
        )
        ax_mclambda.contour(
            grids["mc"], grids["LambdaTilde"], marg_like_mc_lam.T, 10,
            colors=color, linestyles="solid",
        )

        if cli_args.with_spin:
            ax_chieta.contour(
                grids["chieff"], grids["eta"], marg_like_chi_eta, 10,
                colors=color, linestyles="solid",
            )
            ax_chilambda.contour(
                grids["chieff"], grids["LambdaTilde"], marg_like_chi_lam.T, 10,
                colors=color, linestyles="solid",
            )

        # If known, plot the true values as well.
        if detection.truth is not None:
            mc_true, eta_true = detection.truth[:2]
            lambdaTilde_true = detection.truth[-1]
            if cli_args.with_spin:
                chieff_true = detection.truth[2]

            ax_mceta.scatter(
                [mc_true], [eta_true],
                marker="x", color="black", s=5,
                zorder=10,
            )
            ax_mclambda.scatter(
                [mc_true], [lambdaTilde_true],
                marker="x", color="black", s=5,
                zorder=10,
            )

            if cli_args.with_spin:
                ax_chieta.scatter(
                    [chieff_true], [eta_true],
                    marker="x", color="black", s=5,
                    zorder=10,
                )
                ax_chilambda.scatter(
                    [chieff_true], [lambdaTilde_true],
                    marker="x", color="black", s=5,
                    zorder=10,
                )

    ax_mclambda.set_xlabel(r"$\mathcal{M}_c \, [M_\odot]$")
    ax_mclambda.set_ylabel(r"$\tilde{\Lambda}$")
    ax_mceta.set_ylabel(r"$\eta$")

    if cli_args.with_spin:
        ax_chilambda.set_xlabel(r"$\chi_{\mathrm{eff}}$")

    fig.savefig(cli_args.plot_filename)


def make_mesh(detection, with_spin):
    import numpy

    from ....coordinate import CoordinateSystem
    from ....astro_models.coordinates import (
        Mc_source_coord, eta_coord, chieff_coord, LambdaTilde_coord,
    )

    def min_max(x):
        return x.min(), x.max()

    if with_spin:
        expected_coord_system = CoordinateSystem(
            Mc_source_coord, eta_coord, chieff_coord, LambdaTilde_coord,
        )
        if detection.coord_system != expected_coord_system:
            raise ValueError(
                "Detection coordinate system error. Expected: {}; Got: {}."
                .format(expected_coord_system, detection.coord_system)
            )
        mc_min, mc_max = min_max(detection.gaussian_process.x[:,0])
        eta_min, eta_max = min_max(detection.gaussian_process.x[:,1])
        chieff_min, chieff_max = min_max(detection.gaussian_process.x[:,2])
        LambdaTilde_min, LambdaTilde_max = min_max(
            detection.gaussian_process.x[:,3]
        )
    else:
        expected_coord_system = CoordinateSystem(
            Mc_source_coord, eta_coord, LambdaTilde_coord,
        )
        if detection.coord_system != expected_coord_system:
            raise ValueError(
                "Detection coordinate system error. Expected: {}; Got: {}."
                .format(expected_coord_system, detection.coord_system)
            )
        mc_min, mc_max = min_max(detection.gaussian_process.x[:,0])
        eta_min, eta_max = min_max(detection.gaussian_process.x[:,1])
        LambdaTilde_min, LambdaTilde_max = min_max(
            detection.gaussian_process.x[:,2]
        )

    # Broaden mc a bit
    mc_min -= 0.1
    mc_max += 0.1

    mc_grid, mc_step = numpy.linspace(mc_min, mc_max, 50, retstep=True)
    eta_grid, eta_step = numpy.linspace(eta_min, eta_max, 30, retstep=True)
    LambdaTilde_grid, LambdaTilde_step = numpy.linspace(
        LambdaTilde_min, LambdaTilde_max, 80, retstep=True,
    )

    grids = {
        "mc" : mc_grid,
        "eta" : eta_grid,
        "LambdaTilde" : LambdaTilde_grid,
    }
    steps = {
        "mc" : mc_step,
        "eta" : eta_step,
        "LambdaTilde" : LambdaTilde_step,
    }

    if with_spin:
        chieff_grid, chieff_step = numpy.linspace(
            chieff_min, chieff_max, 20, retstep=True,
        )
        grids["chieff"] = chieff_grid
        steps["chieff"] = chieff_step

        observables = list(
            numpy.meshgrid(
                mc_grid, eta_grid, chieff_grid, LambdaTilde_grid,
                indexing="ij",
            )
        )
    else:
        observables = list(
            numpy.meshgrid(
                mc_grid, eta_grid, LambdaTilde_grid,
                indexing="ij",
            )
        )

    return observables, grids, steps
