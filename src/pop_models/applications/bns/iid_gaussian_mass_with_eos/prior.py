import typing
import numpy
import scipy.stats
import h5py
from pop_models import posterior
from pop_models.astro_models import eos
from pop_models.astro_models.building_blocks import beta
from pop_models.types import Numeric, Observables, Parameters, WhereType
from pop_models.utils import debug_verbose

largest_ns_mass = 1.97

# m_min_abs = 0.9
# m_max_abs = 2.9

# rate_min, rate_max = 0.0, 2500.0
# m_mean_min, m_mean_max = 0.9, 2.9
# m_stdev_min, m_stdev_max = 0.0001, 2.0
# gamma1_min, gamma1_max = +0.20, +2.00
# gamma2_min, gamma2_max = -1.60, +1.70
# gamma3_min, gamma3_max = -0.60, +0.60
# gamma4_min, gamma4_max = -0.02, +0.02


MassRangeType = typing.List[typing.Tuple[Numeric,Numeric]]

def is_valid_eos(
        parameters, prior_settings,
        eos_coordinates="spectral", largest_ns_mass=largest_ns_mass,
        require_mass_ranges: typing.Optional[MassRangeType]=None,
    ):
    import lalsimulation

    gamma1, gamma2, gamma3, gamma4 = eos.eos_coords_to_spectral(
        eos.get_eos_parameters(parameters, eos_coordinates),
        eos_coordinates,
    )

    params_shape = gamma1.shape

    valid = (
        (gamma1 >= prior_settings["gamma1"]["params"]["min"]) &
        (gamma1 <= prior_settings["gamma1"]["params"]["max"]) &
        (gamma2 >= prior_settings["gamma2"]["params"]["min"]) &
        (gamma2 <= prior_settings["gamma2"]["params"]["max"]) &
        (gamma3 >= prior_settings["gamma3"]["params"]["min"]) &
        (gamma3 <= prior_settings["gamma3"]["params"]["max"]) &
        (gamma4 >= prior_settings["gamma4"]["params"]["min"]) &
        (gamma4 <= prior_settings["gamma4"]["params"]["max"])
    )

    for i in numpy.ndindex(*params_shape):
        # Skip already invalidated samples
        if not valid[i]:
            continue

        parameters_at_i = {
            param_name : values[i]
            for param_name, values in parameters.items()
        }
        spectral_parameters_at_i = (gamma1[i], gamma2[i], gamma3[i], gamma4[i])

        debug_verbose(
            spectral_parameters_at_i,
            mode="eos_params_display", flush=True,
        )

        try:
            if not eos.is_valid_adiabatic_index(spectral_parameters_at_i):
                G = eos.spectral_eos_adiabatic_index(
                    eos._x_grid, spectral_parameters_at_i,
                )
                debug_verbose(
                    "Invalid adiabatic index between",
                    G.min(), "-", G.max(),
                    "for params",
                    parameters_at_i,
                    mode="eos_reject", flush=True,
                )
                valid[i] = False
                continue

            eos_at_i = eos.spectral_eos(spectral_parameters_at_i)

            if not eos.has_enough_points(eos_at_i):
                debug_verbose(
                    "Not enough points for params",
                    parameters_at_i,
                    mode="eos_reject", flush=True,
                )
                valid[i] = False
                continue

            debug_verbose(
                "is_valid_eos: About to create EOS fam for",
                spectral_parameters_at_i,
                mode="eos_segfault_debug", flush=True,
            )
            eos_fam_at_i = lalsimulation.CreateSimNeutronStarFamily(eos_at_i)
            debug_verbose(
                "is_valid_eos: Successfully created EOS fam for",
                spectral_parameters_at_i,
                mode="eos_segfault_debug", flush=True,
            )

            if not eos.is_causal_eos(eos_at_i, eos_fam_at_i):
                debug_verbose(
                    "Acausal sound speed",
                    eos.eos_max_sound_speed(eos_at_i, eos_fam_at_i),
                    "for params",
                    parameters_at_i,
                    mode="eos_reject", flush=True,
                )
                valid[i] = False
                continue

            eos_m_min, eos_m_max = eos.eos_mass_range(eos_fam_at_i)

            if eos_m_max < largest_ns_mass:
                debug_verbose(
                    "Mass range does not include largest known NS. "
                    "Maximum mass is {}Msun, but largest NS is {}Msun."
                    .format(eos_m_max, largest_ns_mass),
                    mode="eos_reject", flush=True,
                )
                valid[i] = False
                continue

            # Check that a series of mass ranges are included within the EOS's
            # allowed mass range.
            if require_mass_ranges is not None:
                for m_mins, m_maxs in require_mass_ranges:
                    if m_mins[i] >= eos_m_max:
                        debug_verbose(
                            "EOS mass range excludes too much of a "
                            "sub-population. "
                            "Maximum mass is {}Msun, but low end of "
                            "sub-population is {}Msun."
                            .format(eos_m_max, m_mins[i]),
                            mode="eos_reject", flush=True,
                        )
                        valid[i] = False
                        continue
                    if m_maxs[i] <= eos_m_min:
                        debug_verbose(
                            "EOS mass range excludes too much of a "
                            "sub-population. "
                            "Maximum mass is {}Msun, but low end of "
                            "sub-population is {}Msun."
                            .format(eos_m_min, m_maxs[i]),
                            mode="eos_reject", flush=True,
                        )
                        valid[i] = False
                        continue

        except RuntimeError as e:
            if str(e) != "Generic failure":
                raise
            else:
                debug_verbose(
                    "Generic failure for params", parameters_at_i,
                    mode="eos_reject", flush=True,
                )
                valid[i] = False

    return valid


class Prior(object):
    def __init__(
            self,
            prior_settings, population: eos.EOSPopulation,
            constants=None, duplicates=None,
            n_stdev_tolerance: float=1.0,
        ):
        self.prior_settings = prior_settings
        self.population = population

        self.n_stdev_tolerance = n_stdev_tolerance

        if constants is None:
            constants = {}
        if duplicates is None:
            duplicates = {}
        self.skip_params = set(list(constants.keys()) + list(duplicates.keys()))

        self._setup_param_names_record()

    def _setup_param_names_record(self):
        # Get all mass dist mean names
        m_mean_names = [
            param_name
            for param_name in self.population.param_names
            if param_name.startswith("m_mean")
        ]
        # Get the corresponding stdev names by replacing "m_mean" with "m_stdev"
        # in all of the mean names.
        m_stdev_names = [
            "m_stdev{}".format(m_mean_name[6:])
            for m_mean_name in m_mean_names
        ]
        # Pair up the names for future use in a list of pairs.
        self.mass_mean_stdev_names = list(zip(m_mean_names, m_stdev_names))

    def __call__(self, parameters):
        valid = self.population.params_valid(parameters)
        log_prior = numpy.zeros_like(valid, dtype=numpy.float64)

        for param_name in self.population.param_names:
            value = parameters[param_name]

            settings = self.prior_settings[param_name]
            dist = settings["dist"]
            params = settings["params"]

            if dist == "uniform":
                valid &= value >= params["min"]
                valid &= value <= params["max"]
            elif dist == "log-uniform":
                valid &= value >= params["min"]
                valid &= value <= params["max"]
                log_prior -= 0.5 * numpy.log(value)
            elif dist == "gaussian":
                if "min" in params:
                    valid &= value >= params["min"]
                if "max" in params:
                    valid &= valud <= params["max"]
                log_prior -= 0.5 * ((value - params["mu"])/params["sigma"])**2
            else:
                raise ValueError("Unknown prior distribution: {}".format(dist))

        # Setup mass ranges that must be allowed by the EOS.
        require_mass_ranges = []
        for m_mean_name, m_stdev_name in self.mass_mean_stdev_names:
            m_mean, m_stdev = parameters[m_mean_name], parameters[m_stdev_name]
            n_times_sigma = self.n_stdev_tolerance * m_stdev
            require_mass_ranges.append(
                (m_mean-n_times_sigma, m_mean+n_times_sigma)
            )

        valid &= is_valid_eos(
            parameters, self.prior_settings,
            eos_coordinates=self.population.eos_coordinates,
            require_mass_ranges=require_mass_ranges,
        )

        return numpy.where(valid, log_prior, numpy.NINF)


def sample_bounding_hypercube(n_samples, bounds, random_state):
    return tuple(
        random_state.uniform(lo, hi, n_samples)
        for i, (lo, hi) in enumerate(bounds)
    )


def init_gammas_from_prior(n_walkers, random_state, prior_settings):
    bounds = [
        (
            settings[gamma_name]["params"]["min"],
            settings[gamma_name]["params"]["max"],
        )
        for gamma_name in ["gamma1", "gamma2", "gamma3", "gamma4"]
    ]

    return sample_bounding_hypercube(n_walkers, bounds, random_state)


def init_gammas_from_pca(
        n_walkers, random_state, pca_fname, bounding_box_buffer=0.0,
    ):
    with h5py.File(pca_fname, "r") as pca_file:
        basis_vectors = pca_file["basis_vectors"][()]
        pca_mean = pca_file["pca_mean"][()]
        mean = pca_file["mean"][()]
        scaling = pca_file["scaling"][()]
        rotated_bounds = pca_file["rotated_bounds"][()]

    # Rescale bounds according to the specified buffer
    rotated_bounds_scale = (
        bounding_box_buffer * numpy.diff(rotated_bounds)[...,0]
    )
    rotated_bounds[...,0] -= rotated_bounds_scale
    rotated_bounds[...,1] += rotated_bounds_scale

    samples_rotated = sample_bounding_hypercube(
        n_walkers, rotated_bounds, random_state,
    )
    samples = basis_vectors.T.dot(samples_rotated).T
    samples *= scaling
    samples += pca_mean + mean

    return tuple(samples.T)


def init_gammas_from_named(n_walkers, random_state, fixed_eos=None):
    examples = eos.spectral_eos_parameter_examples
    # Downselect to only the fixed EoS if provided.
    if fixed_eos:
        examples = {fixed_eos: examples[fixed_eos]}

    names = list(examples.keys())

    gamma1_init = numpy.empty(n_walkers, dtype=numpy.float64)
    gamma2_init = numpy.empty(n_walkers, dtype=numpy.float64)
    gamma3_init = numpy.empty(n_walkers, dtype=numpy.float64)
    gamma4_init = numpy.empty(n_walkers, dtype=numpy.float64)

    for i in range(n_walkers):
        random_name = random_state.choice(names)
        random_gammas = examples[random_name]

        gamma1_init[i] = random_gammas["gamma1"]
        gamma2_init[i] = random_gammas["gamma2"]
        gamma3_init[i] = random_gammas["gamma3"]
        gamma4_init[i] = random_gammas["gamma4"]

    gamma1_init += random_state.uniform(-1e-6, 1e-6, n_walkers)
    gamma2_init += random_state.uniform(-1e-6, 1e-6, n_walkers)
    gamma3_init += random_state.uniform(-1e-6, 1e-6, n_walkers)
    gamma4_init += random_state.uniform(-1e-6, 1e-6, n_walkers)

    return gamma1_init, gamma2_init, gamma3_init, gamma4_init


def init_gammas_from_samples(n_walkers, random_state, init_eos_samples):
    samples = numpy.genfromtxt(init_eos_samples, names=True)

    n_samples_available = len(samples)

    if n_samples_available < n_walkers:
        raise ValueError(
            "Requested more walkers than available samples."
        )

    i_choice = random_state.choice(
        n_samples_available,
        size=n_walkers, replace=False,
    )

    return (
        samples["gamma0"][i_choice],
        samples["gamma1"][i_choice],
        samples["gamma2"][i_choice],
        samples["gamma3"][i_choice],
    )


def init_gammas(
        n_walkers, random_state, prior_settings,
        eos_method="named", fixed_eos=None,
        init_eos_samples=None,
        pca_fname=None, pca_buffer=None,
        require_mass_ranges=None,
    ):
    if eos_method == "prior":
        draw_candidate_gammas = init_gammas_from_prior
        args, kwargs = (prior_settings,), {}
    elif eos_method == "pca":
        draw_candidate_gammas = init_gammas_from_pca
        args, kwargs = (pca_fname,), {"bounding_box_buffer" : pca_buffer}
    elif eos_method == "named":
        draw_candidate_gammas = init_gammas_from_named
        args, kwargs = (), {"fixed_eos" : fixed_eos}
    elif eos_method == "reuse_samples":
        draw_candidate_gammas = init_gammas_from_samples
        args, kwargs = (init_eos_samples,), {}
    else:
        raise ValueError("Unknown eos_method: {}".format(eos_method))

    (
        gamma1_init, gamma2_init, gamma3_init, gamma4_init,
    ) = draw_candidate_gammas(n_walkers, random_state, *args, **kwargs)

    # If we initialized from existing valid samples, then we can skip
    # resampling.
    if eos_method == "reuse_samples":
        return gamma1_init, gamma2_init, gamma3_init, gamma4_init

    # Validate all of the samples are physical, and iteratively replace any that
    # are not.
    n = 0
    while True:
        debug_verbose(
            "Checking validity for the {n}th time".format(n=n),
            mode="eos_reject", flush="True",
        )
        n += 1
        i_invalid = ~is_valid_eos(
            {
                "gamma1" : gamma1_init,
                "gamma2" : gamma2_init,
                "gamma3" : gamma3_init,
                "gamma4" : gamma4_init,
            },
            prior_settings,
            eos_coordinates="spectral",
            require_mass_ranges=require_mass_ranges,
        )

        n_rejected = numpy.count_nonzero(i_invalid)
        debug_verbose(
            n_rejected, "rejected",
            mode="eos_reject", flush=True,
        )

        if n_rejected == 0:
            break

        gamma1_new, gamma2_new, gamma3_new, gamma4_new = (
            draw_candidate_gammas(n_rejected, random_state, *args, **kwargs)
        )

        gamma1_init[i_invalid] = gamma1_new
        gamma2_init[i_invalid] = gamma2_new
        gamma3_init[i_invalid] = gamma3_new
        gamma4_init[i_invalid] = gamma4_new

    debug_verbose(
        "returning after", n, "tries",
        mode="eos_reject", flush=True,
    )
    return gamma1_init, gamma2_init, gamma3_init, gamma4_init


def init_spins(
        n_walkers, random_state,
        prior_settings,
    ):
    alpha_init = random_state.uniform(0.0, 10.0, n_walkers)
    beta_init = random_state.uniform(0.0, 10.0, n_walkers)

    mean_init, variance_init = beta.alpha_beta_to_mean_variance(
        alpha_init, beta_init,
        loc=-0.05, scale=0.10,
    )
    return mean_init, variance_init


def _init_from_prior_single_component(
        n_walkers, random_state, prior_settings,
        skip_params,
        with_spin, component_number,
    ):
    if component_number is None:
        suffix = ""
    else:
        suffix = "_{}".format(component_number)

    base_param_names = ["rate"+suffix, "m_mean"+suffix, "m_stdev"+suffix]
    spin_mean_name = "spin_mean"+suffix
    spin_variance_name = "spin_variance"+suffix

    params_init = []

    # Initalize parameters that are always present (zero spin)
    for param_name in base_param_names:
        if param_name not in skip_params:
            settings = prior_settings[param_name]
            dist = settings["dist"]
            params = settings["params"]
            if dist == "uniform":
                params_init.append(
                    random_state.uniform(
                        params["min"], params["max"],
                        n_walkers,
                    )
                )
            elif dist == "log-uniform":
                params_init.append(
                    numpy.exp(
                        random_state.uniform(
                            numpy.log(params["min"]), numpy.log(params["max"]),
                            n_walkers,
                        )
                    )
                )
            elif dist == "gaussian":
                mu, sigma = params["mu"], params["sigma"]
                lower = params.get("min", numpy.NINF)
                upper = params.get("max", numpy.inf)
                a = (lower - mu) / sigma
                b = (upper - mu) / sigma
                dist = scipy.stats.truncnorm(a, b, loc=mu, scale=sigma)
                params_init.append(dist.rvs(n_walkers))
            else:
                raise ValueError("Unsupported distribution: {}".format(dist))

    # Initialize spin parameters if given.
    skip_spins = (
        (not with_spin) or
        (
            (spin_mean_name in skip_params) and
            (spin_variance_name in skip_params)
        )
    )
    if not skip_spins:
        spin_mean_init, spin_variance_init = init_spins(
            n_walkers, random_state,
        )
        if spin_mean_name not in skip_params:
            params_init.append(spin_mean_init)
        if spin_variance_name not in skip_params:
            params_init.append(spin_variance_init)

    return params_init

def init_from_prior(
        n_walkers, random_state, prior_settings, param_names, prior,
        constants=None, duplicates=None,
#        with_spin=False, n_components=None,
        eos_method="named", fixed_eos=None, eos_coordinates="spectral",
        init_eos_samples=None,
        pca_fname=None, pca_buffer=None,
        pop_inf: typing.Optional[posterior.PopulationInference]=None,
    ):
    if pop_inf is None:
        def validate_prob(parameters):
            # Return the log(prior)
            return prior(parameters)
    else:
        def validate_prob(parameters):
            log_post_and_prior = pop_inf.log_posterior_from_params(parameters)
            # Return the log(posterior)
            return log_post_and_prior[...,0]


    if constants is None:
        constants = {}
    if duplicates is None:
        duplicates = {}

    variable_names = [
        param_name for param_name in param_names
        if (param_name not in constants) and (param_name not in duplicates)
    ]
    skip_params = set(list(constants.keys()) + list(duplicates.keys()))
#    n_variables = len(param_names) - len(skip_params)
    n_variables = len(variable_names)

    # if n_components is None:
    #     mass_mean_stdev_names = [("m_mean", "m_stdev")]
    # else:
    #     mass_mean_stdev_names = [
    #         ("m_mean_{}".format(i), "m_stdev_{}".format(i))
    #         for i in range(n_components)
    #     ]

    n_walkers_to_go = n_walkers

    params_init = [[] for _ in range(n_variables)]

    while n_walkers_to_go > 0:
        params_init_trial = _init_from_prior_trial(
            n_walkers_to_go, random_state, prior_settings, #param_names,
            variable_names,
#            skip_params,
#            with_spin=with_spin, n_components=n_components,
            eos_method=eos_method, fixed_eos=fixed_eos,
            eos_coordinates=eos_coordinates,
            init_eos_samples=init_eos_samples,
            pca_fname=pca_fname, pca_buffer=pca_buffer,
        )

        parameters_trial = posterior.get_params(
            numpy.column_stack(tuple(params_init_trial)),
            constants, duplicates,
            param_names,
        )

        i_valid = numpy.isfinite(validate_prob(parameters_trial))
        n_valid = numpy.count_nonzero(i_valid)

        n_walkers_to_go -= n_valid

        for i in range(n_variables):
            params_init[i].append(params_init_trial[i][i_valid])

    return numpy.column_stack(tuple(
        numpy.concatenate(tuple(param_list))
        for param_list in params_init
    ))


def _init_from_prior_trial(
        n_walkers, random_state, prior_settings, param_names,
        skip_params, with_spin=False, n_components=None,
        eos_method="named", fixed_eos=None, eos_coordinates="spectral",
        init_eos_samples=None,
        pca_fname=None, pca_buffer=None,
    ):
    params_init_list = []

    if n_components is None:
        params_init_list += _init_from_prior_single_component(
            n_walkers, random_state, prior_settings,
            skip_params,
            with_spin, None,
        )
    else:
        for component_number in range(n_components):
            params_init_list += _init_from_prior_single_component(
                n_walkers, random_state, prior_settings,
                skip_params,
                with_spin, component_number,
            )

    gammas_init = init_gammas(
        n_walkers, random_state, prior_settings,
        eos_method=eos_method, fixed_eos=fixed_eos,
        init_eos_samples=init_eos_samples,
        pca_fname=pca_fname, pca_buffer=pca_buffer,
    )
    params_init_list += list(
        eos.eos_coords_from_spectral(
            gammas_init,
            eos_coordinates=eos_coordinates,
        )
    )

    return params_init_list


def _init_from_prior_trial(
        n_walkers, random_state, prior_settings, variable_names,
        eos_method="named", fixed_eos=None, eos_coordinates="spectral",
        init_eos_samples=None,
        pca_fname=None, pca_buffer=None,
    ):
    params_init_list = []

    # Initalize parameters that are always present (zero spin)
    for param_name in variable_names:
        if param_name not in {"gamma1", "gamma2", "gamma3", "gamma4"}:
            settings = prior_settings[param_name]
            dist = settings["dist"]
            params = settings["params"]
            if dist == "uniform":
                params_init_list.append(
                    random_state.uniform(
                        params["min"], params["max"],
                        n_walkers,
                    )
                )
            elif dist == "log-uniform":
                params_init_list.append(
                    numpy.exp(
                        random_state.uniform(
                            numpy.log(params["min"]), numpy.log(params["max"]),
                            n_walkers,
                        )
                    )
                )
            elif dist == "gaussian":
                mu, sigma = params["mu"], params["sigma"]
                lower = params.get("min", numpy.NINF)
                upper = params.get("max", numpy.inf)
                a = (lower - mu) / sigma
                b = (upper - mu) / sigma
                dist = scipy.stats.truncnorm(a, b, loc=mu, scale=sigma)
                params_init_list.append(dist.rvs(n_walkers))
            else:
                raise ValueError("Unsupported distribution: {}".format(dist))

    gammas_init = init_gammas(
        n_walkers, random_state, prior_settings,
        eos_method=eos_method, fixed_eos=fixed_eos,
        init_eos_samples=init_eos_samples,
        pca_fname=pca_fname, pca_buffer=pca_buffer,
    )
    params_init_list += list(
        eos.eos_coords_from_spectral(
            gammas_init,
            eos_coordinates=eos_coordinates,
        )
    )

    return params_init_list
