def populate_subparser(subparsers):
    subparser = subparsers.add_parser("test_pop_sampling")

    subparser.add_argument(
        "constants",
        help="A constants.json file specifying every parameter.",
    )
    subparser.add_argument(
        "output_plot",
        help="File to save plot to.",
    )

    subparser.add_argument(
        "--n-powerlaw-parts",
        type=int, default=2,
        help="Number of parts in broken powerlaw (default 2).",
    )

    subparser.add_argument(
        "--n-samples",
        type=int, default=1000,
        help="Number of random samples to draw from model.",
    )
    subparser.add_argument(
        "--seed",
        type=int,
        help="Seed for the random-number-generator.",
    )

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    from .population import get_param_names, get_population, coord_system
    from ....astro_models.coordinates import (
        m1_source_coord, q_coord,
    )

    import warnings

    import numpy
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt
    import corner

    import numbers

    import json

    # Initialize RNG
    random_state = numpy.random.RandomState(cli_args.seed)

    # Get param names
    param_names = get_param_names(cli_args.n_powerlaw_parts)

    # Load in constants, and validate they are properly structured.
    with open(cli_args.constants, "r") as constants_file:
        constants = json.load(constants_file)
    assert isinstance(constants, dict)
    for k, v in constants.items():
        assert isinstance(k, str)
        assert isinstance(v, numbers.Number)
    assert set(constants.keys()) == set(param_names)

    # Convert values in `constants` to numpy arrays.
    ## HACK: shouldn't need to wrap `v` in a list.  Limitation of broken
    ## powerlaw
    constants = {k : numpy.asarray([v]) for k, v in constants.items()}

    # Construct the population object.
    population = get_population(cli_args.n_powerlaw_parts)

    # Draw random samples.
    samples = population.rvs(
        cli_args.n_samples, constants,
        random_state=random_state,
    )
    ## HACK: Undoing extra nesting from hack above with `[v]`
    samples = [s[0] for s in samples]

    n_params = len(samples)

    # Put up a warning if there are any NaN's.
    n_nans = numpy.count_nonzero(numpy.isnan(samples), axis=1)
    if numpy.any(n_nans != 0):
        warnings.warn(
            "Found NaN's: {}"
            .format(", ".join([
                "{} = {}".format(coord.name, n)
                for coord, n in zip(coord_system, n_nans)
            ]))
        )

    fig, axes = plt.subplots(
        ncols=n_params, figsize=(3*n_params, 4),
        squeeze=False,
    )

    log_scale_coords = {m1_source_coord}

    for i, coord in enumerate(coord_system):
        ax = axes[0,i]

        if coord in log_scale_coords:
            ax.set_yscale("log")

        ax.hist(
            samples[i],
            density=True, histtype="step", bins="auto",
            color="C0",
        )
        axes[0,i].set_xlabel(coord.name)

    fig.savefig(cli_args.output_plot)
