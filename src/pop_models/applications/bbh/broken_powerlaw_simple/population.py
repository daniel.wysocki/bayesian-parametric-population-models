import types
import numpy

from ....coordinate import CoordinateSystem
from ....population import SeparableCoordinatePopulation
from ....astro_models.building_blocks import (
    generic, broken_powerlaw,
)
from ....astro_models.coordinates import (
    m1_source_coord, m2_source_coord, q_coord,
    transformations,
)


coord_system = CoordinateSystem(
    m1_source_coord, m2_source_coord, q_coord,
)

def get_population(n_parts: int, xpy: types.ModuleType=numpy):
    population_m1 = broken_powerlaw.BrokenPowerlawPopulation(
        m1_source_coord, n_parts,
        norm_name="rate", index_name="alpha", break_point_name="mass_break",
        negative_index=True,
        transformations=transformations,
        xpy=xpy,
    )
    # TODO: Need to actually make this p(m_2) to get Jacobians right, everything
    # will be off by an extra multiplicative factor of m_1 as-is.
    population_q = generic.GenericPowerlawPopulation(
        q_coord,
        norm_name="rate", index_name="beta",
        lower_name="q_min", upper_name="q_max",
        transformations=transformations,
        xpy=xpy,
    )

    population_mass = SeparableCoordinatePopulation(
        population_m1, population_q,
        transformations=transformations,
    )

    # TODO: add spin

    return population_mass


def get_param_names(n_parts: int):
    # Initialize
    param_names = []
    # Add rate terms
    param_names += ["rate"]
    # Add m_1 broken powerlaw terms
    param_names += ["alpha"+str(i) for i in range(n_parts)]
    param_names += ["mass_break"+str(i) for i in range(n_parts+1)]
    # Add mass ratio parts
    param_names += ["beta", "q_min", "q_max"]

    return param_names
