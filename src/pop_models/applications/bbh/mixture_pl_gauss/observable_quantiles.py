def populate_subparser(subparsers):
    from ....astro_models.coordinates import coordinates

    subparser = subparsers.add_parser("observable_quantiles")

    subparser.add_argument("cleaned_posterior_samples_hdf5")
    subparser.add_argument("output_file")
    subparser.add_argument("output_plot")

    subparser.add_argument(
        "--n-samples-per-realization",
        type=int, default=1000,
        help="Number of observable samples to make for each population "
             "realization.",
    )

#    subparser.add_argument("--overlay-synthetic-truth", action="store_true")

    subparser.add_argument(
        "--observable",
        default="m1_source", choices=coordinates.keys(),
        help="Observable to include quantiles for.",
    )
    subparser.add_argument(
        "--quantiles",
        type=float, nargs="+", default=[0.95],
        help="Quantiles to include.",
    )

    subparser.add_argument(
        "--isolate-subpops", nargs="+",
        help="Only plot specific subpopulations.  "
             "List their suffixes, e.g., 'pl0 g0 pl1'.",
    )

    subparser.add_argument("--seed", type=int)

    subparser.add_argument(
        "--force",
        action="store_true",
        help="Overwrite existing HDF5 output files.",
    )

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    from .population import get_population

    from ....posterior import H5CleanedPosteriorSamples
    from ....coordinate import CoordinateSystem
    from ....quantiles import ObservableQuantiles
    from ....astro_models.coordinates import coordinates

    import h5py
    import numpy
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt
    import corner

    random_state = numpy.random.RandomState(cli_args.seed)

    hdf5_mode = "w" if cli_args.force else "w-"

    coord_system = CoordinateSystem(coordinates[cli_args.observable])

    # Load in the posterior samples
    post_samples = H5CleanedPosteriorSamples(
        cli_args.cleaned_posterior_samples_hdf5
    )
    with post_samples:
        # Determine how many powerlaws and gaussians are in the mixture.
        n_powerlaws = post_samples.metadata["n_powerlaws"]
        n_gaussians = post_samples.metadata["n_gaussians"]
        M_max = post_samples.metadata["M_max"]
        # Get population object.
        population = get_population(n_powerlaws, n_gaussians, M_max).to_coords(
            coord_system
        )
        # Isolate sub-populations if requested.
        if cli_args.isolate_subpops is not None:
            # Add in the underscore prefixes for convenience.
            suffixes = ["_"+s for s in cli_args.isolate_subpops]
            # Replace population with sub-populations.
            population = population.get_subpops(suffixes)

        # Create object for computing quantiles.
        obs_quantiles = ObservableQuantiles(population)

        # Draw all posterior samples.
        parameter_samples = post_samples.get_params(...)

        # Compute quantiles.
        quantiles = {}
        for q in cli_args.quantiles:
            quantiles[q] = obs_quantiles.quantile(
                parameter_samples, q,
                n_samples=cli_args.n_samples_per_realization,
                random_state=random_state,
            )

        # Save to file.
        with h5py.File(cli_args.output_file, hdf5_mode) as out_file:
            for q, values in quantiles.items():
                out_file.create_dataset(str(q), data=values)

        data = numpy.column_stack(tuple(
            quantiles[q] for q in sorted(quantiles.keys())
        ))
        labels = [
            "{} ({})".format(cli_args.observable, q)
            for q in sorted(quantiles.keys())
        ]
        fig = corner.corner(
            data,
            labels=labels, color="black",
            levels=[0.5, 0.9],
            plot_density=False,
            plot_datapoints=False,
            no_fill_contours=True,
            hist_kwargs={"density" : True},
            contour_kwargs={
                "linestyles" : ["dashed", "solid"],
                "color" : "black",
            },
        )
        fig.savefig(cli_args.output_plot)
