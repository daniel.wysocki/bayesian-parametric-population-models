def populate_subparser(subparsers):
    subparser = subparsers.add_parser("plot_spin_distribution")

    subparser.add_argument("cleaned_posterior_samples_hdf5")
    subparser.add_argument("output_plot_intensity_chi")
    subparser.add_argument("output_plot_pdf_chi")
    subparser.add_argument("output_plot_intensity_costilt")
    subparser.add_argument("output_plot_pdf_costilt")

    subparser.add_argument(
        "--output-files",
        metavar=(
            "FILE_INTENSITY_CHI1", "FILE_PDF_CHI1",
            "FILE_INTENSITY_CHI2", "FILE_PDF_CHI2",
            "FILE_INTENSITY_COSTILT1", "FILE_PDF_COSTILT1",
            "FILE_INTENSITY_COSTILT2", "FILE_PDF_COSTILT2",
        ),
        nargs=8,
    )

    subparser.add_argument(
        "--isolate-subpops", nargs="+",
        help="Only plot specific subpopulations.  "
             "List their suffixes, e.g., 'pl0 g0 pl1'.",
    )

    subparser.add_argument("--n-plot-points", type=int, default=50)

    # subparser.add_argument(
    #     "--combine-components",
    #     action="store_true",
    #     help="Combine spin1 and spin2 into unlabeled spin.",
    # )

    subparser.add_argument(
        "--overlay-truth",
        help="JSON file specifying parameters for a population to overlay.",
    )

    subparser.add_argument(
        "--n-oom",
        type=int,
        help="Limit plots' y-axes to the specified number of OoM from the "
             "peak.",
    )

    subparser.add_argument("--seed", type=int)

    subparser.add_argument(
        "--force",
        action="store_true",
        help="Overwrite existing HDF5 output files.",
    )

    subparser.add_argument(
        "--debug",
        nargs="*",
        help="Output debugging messages.  Use with no arguments to display all "
             "debugging information, or specify the debugging mode(s).",
    )

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    from .population import get_population

    from pop_models.posterior import H5CleanedPosteriorSamples
    from pop_models.coordinate import CoordinateSystem
    from pop_models.credible_regions import CredibleRegions1D
    from pop_models.astro_models.coordinates import (
        chi1_coord, chi2_coord, costilt1_coord, costilt2_coord,
    )
    from pop_models.utils.plotting import limit_ax_oom
    from pop_models.utils import debug_verbose

    import numpy
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt
    import scipy.stats

    # Set debugging mode
    if cli_args.debug is not None:
        if len(cli_args.debug) == 0:
            debug_verbose.full_output_on()
        else:
            debug_verbose.enable_modes(*cli_args.debug)

    random_state = numpy.random.RandomState(cli_args.seed)

    # Determine whether HDF5 outputs are being made, and pull out filenames
    hdf5_out = cli_args.output_files is not None
    if hdf5_out:
        (
            output_file_intensity_chi1, output_file_pdf_chi1,
            output_file_intensity_chi2, output_file_pdf_chi2,
            output_file_intensity_costilt1, output_file_pdf_costilt1,
            output_file_intensity_costilt2, output_file_pdf_costilt2,
        ) = cli_args.output_files

        hdf5_mode = "w" if cli_args.force else "w-"

    # Load in true parameters if provided
    if cli_args.overlay_truth:
        import json
        with open(cli_args.overlay_truth, "r") as truths_file:
            parameters_true = json.load(truths_file)
        # Convert scalars into single-element numpy arrays
        parameters_true = {
            name : numpy.asarray(value)
            for name, value in parameters_true.items()
        }

    # Load in the posterior samples
    post_samples = H5CleanedPosteriorSamples(
        cli_args.cleaned_posterior_samples_hdf5
    )
    with post_samples:
        # Get appropriate population object.
        metadata = post_samples.metadata
        n_powerlaws = metadata["n_powerlaws"]
        n_gaussians = metadata["n_gaussians"]
        M_max = metadata["M_max"]
        same_gauss_mass_cutoffs = metadata.get("same_gauss_mass_cutoffs", True)
        population = get_population(
            n_powerlaws, n_gaussians, M_max,
            same_gauss_mass_cutoffs=same_gauss_mass_cutoffs,
        )
        # Isolate sub-populations if requested.
        if cli_args.isolate_subpops is not None:
            # Add in the underscore prefixes for convenience.
            suffixes = ["_"+s for s in cli_args.isolate_subpops]
            # Replace population with sub-populations.
            population = population.get_subpops(suffixes)

        # Split into p(m1) and p(q)
        population_chi1 = population.to_coords(
            CoordinateSystem(chi1_coord)
        )
        population_chi2 = population.to_coords(
            CoordinateSystem(chi2_coord)
        )
        population_costilt1 = population.to_coords(
            CoordinateSystem(costilt1_coord)
        )
        population_costilt2 = population.to_coords(
            CoordinateSystem(costilt2_coord)
        )

        # Draw all posterior samples.
        parameter_samples = post_samples.get_params(...)

        # Create wrapper functions for the PDF's and intensity functions, which
        # currently need to be built up with KDE's from samples, as there aren't
        # explicit mass marginals available right now.
        def chi1_pdf(chi1, parameters):
            return population_chi1.pdf(
                (chi1,), parameters,
            )
        def chi2_pdf(chi2, parameters):
            return population_chi2.pdf(
                (chi2,), parameters,
            )
        def costilt1_pdf(costilt1, parameters):
            return population_costilt1.pdf(
                (costilt1,), parameters,
            )
        def costilt2_pdf(costilt2, parameters):
            return population_costilt2.pdf(
                (costilt2,), parameters,
            )

        # Set up x-axis grids.
        chi_grid = numpy.linspace(0.0, 1.0, cli_args.n_plot_points)
        costilt_grid = numpy.linspace(-1.0, 1.0, cli_args.n_plot_points)

        ## Plot chi1 and chi2
        chi1_pdf_ci = CredibleRegions1D.from_samples(
            chi1_pdf, chi_grid, parameter_samples,
        )
        chi2_pdf_ci = CredibleRegions1D.from_samples(
            chi2_pdf, chi_grid, parameter_samples,
        )

        fig, ax = plt.subplots(figsize=[6,4], constrained_layout=True)

        chi1_pdf_ci.plot(
            ax,
            include_median=True, include_mean=True,
            equal_prob_bounds=[0.5, 0.9],
            cache=hdf5_out, color="C0", label=r"$\chi_1$",
        )
        # Overlay truths if provided
        if cli_args.overlay_truth:
            ax.plot(
                chi_grid, chi1_pdf(chi_grid, parameters_true),
                color="#000000",
            )
        # Save to file if provided
        if hdf5_out:
            chi1_pdf_ci.save(output_file_pdf_chi1, mode=hdf5_mode)

        chi2_pdf_ci.plot(
            ax,
            include_median=True, include_mean=True,
            equal_prob_bounds=[0.5, 0.9],
            cache=hdf5_out, color="C3", label=r"$\chi_2$",
        )
        # Overlay truths if provided
        if cli_args.overlay_truth:
            ax.plot(
                chi_grid, chi2_pdf(chi_grid, parameters_true),
                color="#AAAAAA",
            )
        # Save to file if provided
        if hdf5_out:
            chi2_pdf_ci.save(output_file_pdf_chi2, mode=hdf5_mode)

        ## TODO: implement synthetic truth overlay

        ax.set_xlabel(r"$\chi$")
        ax.set_ylabel(r"$p(\chi)$")

        ax.legend()

        fig.savefig(cli_args.output_plot_pdf_chi)
        # Free up memory
        plt.close(fig)
        del chi1_pdf_ci, chi2_pdf_ci

        ## Plot costilt1 and costilt2
        costilt1_pdf_ci = CredibleRegions1D.from_samples(
            costilt1_pdf, costilt_grid, parameter_samples,
        )
        costilt2_pdf_ci = CredibleRegions1D.from_samples(
            costilt2_pdf, costilt_grid, parameter_samples,
        )

        fig, ax = plt.subplots(figsize=[6,4], constrained_layout=True)

        costilt1_pdf_ci.plot(
            ax,
            include_median=True, include_mean=True,
            equal_prob_bounds=[0.5, 0.9],
            cache=hdf5_out, color="C0", label=r"$\cos\theta_1$",
        )
        # Overlay truths if provided
        if cli_args.overlay_truth:
            ax.plot(
                costilt_grid, costilt1_pdf(costilt_grid, parameters_true),
                color="#000000",
            )
        # Save to file if provided
        if hdf5_out:
            costilt1_pdf_ci.save(output_file_pdf_costilt1, mode=hdf5_mode)

        costilt2_pdf_ci.plot(
            ax,
            include_median=True, include_mean=True,
            equal_prob_bounds=[0.5, 0.9],
            cache=hdf5_out, color="C3", label=r"$\cos\theta_2$",
        )
        # Overlay truths if provided
        if cli_args.overlay_truth:
            ax.plot(
                costilt_grid, costilt2_pdf(costilt_grid, parameters_true),
                color="#AAAAAA",
            )
        # Save to file if provided
        if hdf5_out:
            costilt2_pdf_ci.save(output_file_pdf_costilt2, mode=hdf5_mode)

        ## TODO: implement synthetic truth overlay

        ax.set_xlabel(r"$\cos\theta$")
        ax.set_ylabel(r"$p(\cos\theta)$")

        ax.legend()

        fig.savefig(cli_args.output_plot_pdf_costilt)
        # Free up memory
        plt.close(fig)
        del costilt1_pdf_ci, costilt2_pdf_ci
