import typing
import types
import numpy
from pop_models.utils import debug_verbose
from pop_models.prior import KDEPriorHelper
from pop_models.coordinate import CoordinateSystem
from pop_models.stats import truncnorm_pdf
from pop_models.astro_models.building_blocks.beta import (
    BetaPopulation, mean_variance_to_alpha_beta,
)
from pop_models.astro_models.building_blocks.gaussian_mass import (
    truncnorm_rvs,
)
from pop_models.astro_models.coordinates import chi1_coord, chi2_coord

__all__ = [
    "Prior",
]

class Prior(object):
    def __init__(
            self,
            random_state: numpy.random.RandomState,
            population,
            param_names: typing.Tuple[str],
            settings,
            M_max: float, n_powerlaws: int, n_gaussians: int,
            constants=None, duplicates=None,
            xpy: types.ModuleType=numpy,
        ):
        self.random_state = random_state
        self.settings = settings

        self.population = population
        self.M_max = M_max
        self.n_powerlaws = n_powerlaws
        self.n_gaussians = n_gaussians
        # For convenience
        self.mixture_suffixes = (
            tuple("_pl{}".format(i) for i in range(self.n_powerlaws)) +
            tuple("_g{}".format(i)  for i in range(self.n_gaussians))
        )

        if constants is None:
            constants = {}
        if duplicates is None:
            duplicates = {}

        self.constants = constants
        self.duplicates = duplicates
        self.skip_params = set(list(constants.keys()) + list(duplicates.keys()))

        self.param_names = param_names
        self.variable_names = tuple(
            param_name for param_name in self.param_names
            if (param_name not in self.constants)
            and (param_name not in self.duplicates)
        )
        self.n_variables = len(self.variable_names)

        self.xpy = xpy

        self._setup_spin_info()
        self._setup_kde_prior_helpers()


    def _setup_spin_info(self):
        """
        Pre-computes data for initializing spin magnitude distributions.

        Specifically, this maps each pair of spin magnitude parameter names
        (mean and variance for a given subpopulation and binary component) to
        their respective BetaPopulation object.
        """
        # Initialize output.
        self.spin_info = {} # typing.Dict[typing.Tuple[typing.Tuple[str,str],BetaPopulation]]

        # Iterate over each subpopulation suffix.
        for suffix in self.mixture_suffixes:
            # Get the subpopulation.
            sub_pop = self.population.sub_populations[
                self.population.suffixes.index(suffix)
            ]
            # Iterate over the chi1 and chi2 components of the subpopulation.
            for comp, coord in [("1", chi1_coord), ("2", chi2_coord)]:
                # Get the name of the parameters.
                E_chi_name = (
                    "E_chi{comp}{suffix}".format(comp=comp, suffix=suffix)
                )
                Var_chi_name = (
                    "Var_chi{comp}{suffix}".format(comp=comp, suffix=suffix)
                )

                # Get the separable part of the subpopulation corresponding to
                # the given spin component.
                spin_pop = typing.cast(
                    BetaPopulation,
                    sub_pop.to_coords(CoordinateSystem(coord)),
                )

                debug_verbose(
                    "Storing spin parameters and population",
                    E_chi_name, Var_chi_name, spin_pop,
                    mode="prior",
                )

                # Append the info into self.spin_info for use later.
                self.spin_info[(E_chi_name, Var_chi_name)] = spin_pop


    def _setup_kde_prior_helpers(self) -> None:
        """
        Sets up the helper objects for evaluating KDE priors.
        """
        self._kde_prior_helpers = {}
        # Process each variable
        for param in self.variable_names:
            settings = self.settings[param]

            # Skip non-KDE priors
            if settings["dist"] != "kde":
                continue

            self._kde_prior_helpers[param] = KDEPriorHelper(settings["params"])


    def log_prior(self, parameters):
        """
        Evaluates the log(prior) for the given population parameters.
        """
        shape = next(iter(parameters.values())).shape
        log_prior = self.xpy.zeros(shape, dtype=numpy.float64)
        valid = self.population.params_valid(parameters)
        # Initialize array for storing contributions to ``valid``
        valid_tmp = self.xpy.empty_like(valid)

        # Handle priors for all parameters aside from rate.
        for param, values in parameters.items():
            # Skip if constant or duplicate.
            if param in self.skip_params:
                continue

            settings = self.settings[param]
            dist = settings["dist"]
            params = settings["params"]

            if dist == "uniform":
                is_large_enough = self.xpy.greater_equal(
                    values, params["min"],
                    out=valid_tmp,
                )
                debug_verbose(
                    "Parameter", param, "large enough?", is_large_enough,
                    mode="prior",
                )
                valid &= is_large_enough
                is_small_enough = self.xpy.less_equal(
                    values, params["max"],
                    out=valid_tmp,
                )
                debug_verbose(
                    "Parameter", param, "small enough?", is_small_enough,
                    mode="prior", flush=True,
                )
                valid &= is_small_enough
            elif dist == "log-uniform":
                is_large_enough = self.xpy.greater_equal(
                    values, params["min"],
                    out=valid_tmp,
                )
                debug_verbose(
                    "Parameter", param, "large enough?", is_large_enough,
                    mode="prior",
                )
                valid &= is_large_enough
                is_small_enough = self.xpy.less_equal(
                    values, params["max"],
                    out=valid_tmp,
                )
                debug_verbose(
                    "Parameter", param, "small enough?", is_small_enough,
                    mode="prior", flush=True,
                )
                valid &= is_small_enough
                log_prior -= self.xpy.log(values)
            elif dist == "gaussian":
                x_min = params.get("min", self.xpy.NINF)
                x_max = params.get("max", self.xpy.inf)
                mu = params["mu"]
                sigma = params["sigma"]

                log_prior += self.xpy.log(
                    truncnorm_pdf(values, mu, sigma, x_min, x_max)
                )
            elif dist == "kde":
                kde_helper = self._kde_prior_helpers[param]
                log_prior += kde_helper.log_pdf(values)
            # Special case for rates with Jeffries prior
            elif dist == "jeffries" and param.startswith("rate"):
                log_prior -= 0.5 * self.xpy.log(values)
            else:
                raise NotImplementedError()

        # Return the accumulated log(prior), or -inf where there's no support.
        return self.xpy.where(valid, log_prior, self.xpy.NINF)

    def init_walkers(self, n_walkers, pop_inf=None):
        """
        Draw samples from the population prior distribution to initialize the
        walkers.
        """
        from pop_models import posterior

        if pop_inf is None:
            def validate_prob(parameters):
                # Return the log(prior)
                return self.log_prior(parameters)
        else:
            def validate_prob(parameters):
                log_post_and_prior = (
                    pop_inf.log_posterior_from_params(parameters)
                )
                # Return the log(posterior)
                return log_post_and_prior[...,0]

        def sample_prior_single(N, param, prior_settings):
            """
            Samples from a prior with given settings.
            """
            # Determines the type of distribution
            dist = prior_settings["dist"]
            # Determines the parameters of the distribution (e.g., {min,max})
            params = prior_settings["params"]

            # Sample from uniform distribution.
            if dist == "uniform":
                return self.random_state.uniform(
                    params["min"], params["max"], N,
                )
            elif dist == "log-uniform":
                return self.xpy.exp(
                    self.random_state.uniform(
                        self.xpy.log(params["min"]),
                        self.xpy.log(params["max"]),
                        N,
                    )
                )
            elif dist == "gaussian":
                x_min = params.get("min", self.xpy.NINF)
                x_max = params.get("max", self.xpy.inf)
                mu = params["mu"]
                sigma = params["sigma"]

                return truncnorm_rvs(
                    N,
                    mu, sigma, x_min, x_max,
                    random_state=random_state,
                )
            elif dist == "kde":
                kde_helper = self._kde_prior_helpers[param]
                return kde_helper.rvs(N)
            # Type of distribution is unknown.
            else:
                raise NotImplementedError(
                    "No implementation for prior '{}'".format(dist)
                )

        def sample_prior(N):
            """
            Iterate over all free parameters and draw ``N`` samples from their
            priors. Then combine into an array, where each column holds the
            values from one parameter.
            """
            # Separately initialize spin magnitude parameters.
            special_param_samples = {}
            for (E_chi_name, Var_chi_name), pop in self.spin_info.items():
                E_chi_samples, Var_chi_samples = pop.random_mean_variance(
                    N, random_state=self.random_state,
                )
                special_param_samples[E_chi_name] = E_chi_samples
                special_param_samples[Var_chi_name] = Var_chi_samples

            return tuple(
                (
                    special_param_samples[param]
                    if param in special_param_samples
                    else sample_prior_single(N, param, self.settings[param])
                )
                for param in self.param_names
                # Skip over non-free params.
                if param not in self.skip_params
            )

        # Initialize index
        i_start = 0
        n_walkers_to_go = n_walkers

        # Tuple of arrays of variables to initialize with.
        variables_init = tuple(
            numpy.empty(n_walkers) for _ in range(self.n_variables)
        )

        # Keep drawing new candidate initialization states from the prior,
        # rejecting those which don't meet our requirements, until all walkers
        # have been initialized.
        while n_walkers_to_go > 0:
            debug_verbose(
                "Initializing {}/{} walkers".format(n_walkers_to_go, n_walkers),
                mode="mcmc_init", flush=True,
            )

            # Draw `n_walkers` new candidate variables, regardless of how many
            # we still need.
            variables_init_trial = sample_prior(n_walkers)
            debug_verbose(
                "Trial variables:", variables_init_trial,
                mode="mcmc_init", flush=True,
            )

            # Convert variables to full set of parameters, including constants
            # and duplicates, and check which have support, and how many new
            # samples we will keep.
            parameters_trial = posterior.get_params(
                numpy.column_stack(variables_init_trial),
                self.constants, self.duplicates,
                self.param_names,
            )
            debug_verbose(
                "Validating parameters",
                mode="mcmc_init", flush=True,
            )
            i_valid = numpy.isfinite(validate_prob(parameters_trial))
            n_valid = numpy.count_nonzero(i_valid)
            debug_verbose(
                n_valid, "parameters are valid",
                mode="mcmc_init", flush=True,
            )

            # If all candidate variables are invalid, skip to next iteration.
            if n_valid == 0:
                continue

            # Count how many new samples will be kept.  Don't to keep more than
            # we need in the end.
            n_keep = min(n_valid, n_walkers_to_go)
            debug_verbose(
                n_keep, "parameters to be kept",
                mode="mcmc_init", flush=True,
            )

            # For each variable, we append the new samples.
            for i in range(self.n_variables):
                variables_init[i][i_start:i_start+n_keep] = (
                    variables_init_trial[i][i_valid][:n_keep]
                )

            # Update bookkeeping
            i_start += n_keep
            n_walkers_to_go -= n_keep

        # Return the initializing variables as a column array.
        return numpy.column_stack(variables_init)
