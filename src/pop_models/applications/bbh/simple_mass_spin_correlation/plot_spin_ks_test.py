def populate_subparser(subparsers):
    subparser = subparsers.add_parser("plot_spin_ks_test")

    subparser.add_argument("cleaned_posterior_samples_hdf5")
    subparser.add_argument("output_plot")

    subparser.add_argument("--n-plot-points", type=int, default=50)
    subparser.add_argument("--n-samples", type=int, default=1000)
    subparser.add_argument(
        "--reference-masses",
        type=float, default=None, nargs="+",
        help="Reference mass(es) to compare against in KS test.  Default is to "
             "use both mmin and mmax.",
    )

#    subparser.add_argument("--overlay-synthetic-truth", action="store_true")

    subparser.add_argument("--seed", type=int)

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    import numpy
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt

    from .population import get_population

    from ....posterior import H5CleanedPosteriorSamples
    from ....credible_regions import CredibleRegions1D
    from ....astro_models.building_blocks.beta_conditional import (
        LinearTanhBetaConditionalPopulation,
    )
    from ....astro_models.coordinates import m1_source_coord, chi1_coord

    # Load in the colors to use.
    colors = ["C{}".format(i) for i in range(10)]

    # Construct the spin distribution, with no base m1 population, as we'll be
    # directly accessing the conditional pdf/rvs functions.
    pop_chi1_given_m1 = LinearTanhBetaConditionalPopulation(
        None,
        chi1_coord, m1_source_coord,
        loc=0.0, scale=1.0,
        rate_name="rate",
        a_name="a1", b_name="b1", c_name="c1", y_prime_name="m1_ref",
        variance_name="Var_chi1",
        xpy=numpy,
    )

    random_state = numpy.random.RandomState(cli_args.seed)


    def ks_test(m1, parameters, *, m1_ref):
        # Determine needed shapes.
        param_shape = next(iter(parameters.values())).shape
        sample_shape = param_shape + (cli_args.n_samples,)
        ks_shape = param_shape + (len(m1),)

        # Broadcast m1_ref to have `cli_args.n_samples` repetitions, so spin
        # samples are drawn that many times.
        m1_ref = numpy.broadcast_to(m1_ref, sample_shape)

        # Draw reference samples, and sort them for convenience.
        chi1_ref, = pop_chi1_given_m1.cond_rvs(
            (m1_ref,), parameters,
            random_state=random_state,
        )
        chi1_ref.sort(axis=-1)
        # Compute the empirical CDF at each of the sample points.
        edf_ref = numpy.linspace(0.0, 1.0, cli_args.n_samples)

        # Initialize output array.
        ks = numpy.empty(ks_shape, dtype=numpy.float64)

        # Iterate over each test sample, compute the KS test, and store it.
        for i, m1_test in enumerate(m1):
            # Broadcast m1_test like we did with m1_ref.
            m1_test = numpy.broadcast_to(m1_test, sample_shape)
            # Draw test samples, and sort.
            chi1_test, = pop_chi1_given_m1.cond_rvs(
                (m1_test,), parameters,
                random_state=random_state,
            )
            chi1_test.sort(axis=-1)
            # Compute the empirical CDF at each of the reference samples.
            edf_test = numpy.empty_like(m1_test)
            for j in range(cli_args.n_samples):
                edf_test[...,j] = (
                    numpy.count_nonzero(
                        chi1_test <= chi1_ref[...,j,None],
                        axis=-1,
                    ) / cli_args.n_samples
                )

            # Find the maximum absolute difference (the KS test) and store it.
            ks[...,i] = numpy.max(numpy.abs(edf_ref - edf_test), axis=-1)

        return ks

    # Load in the posterior samples
    post_samples = H5CleanedPosteriorSamples(
        cli_args.cleaned_posterior_samples_hdf5
    )
    with post_samples:
        # Draw all posterior samples.
        parameter_samples = post_samples.get_params(...)

        # Determine m_min and m_max
        m_min_name, m_max_name = "m_min", "m_max"
        if m_min_name in post_samples.constants:
            # Use the constant value.
            m_min = post_samples.constants[m_min_name]
        else:
            # Use the lowest value in the posterior sample set.
            m_min = parameter_samples[m_min_name].min()
        if m_max_name in post_samples.constants:
            # Use the constant value.
            m_max = post_samples.constants[m_max_name]
        else:
            # Use the highest value in the posterior sample set.
            m_max = parameter_samples[m_max_name].max()

        # Set up grid.
        m1_grid = numpy.logspace(
            numpy.log10(m_min), numpy.log10(m_max),
            cli_args.n_plot_points,
        )

        # Determine reference masses, defaulting to mmin, mmax if none given.
        if cli_args.reference_masses is None:
            reference_masses = [m_min, m_max]
        else:
            reference_masses = cli_args.reference_masses

        # Initialize the plot.
        fig, ax = plt.subplots(figsize=[6,4])

        # For each reference mass, compute and overplot CI's.
        for m1_ref, color in zip(reference_masses, colors):
            ks_test_ci = CredibleRegions1D.from_samples(
                ks_test, m1_grid, parameter_samples,
                kwargs={"m1_ref": m1_ref},
            )

            ks_test_ci.plot(
                ax,
                quantiles=[0.1, 0.5, 0.9],
                color=color, quantile_style="solid", quantile_labels=True,
                x_scale="log", y_scale="log",
                label=r"$m_{{1,\mathrm{{ref}}}} = {} M_\odot$".format(m1_ref),
            )

            ## TODO: implement synthetic truth overlay

        ax.set_xlabel(r"$m_{1,\mathrm{source}} / M_\odot$")
        ax.set_ylabel(r"KS Test")

        ax.legend(loc="best")

        fig.tight_layout()
        fig.savefig(cli_args.output_plot)
