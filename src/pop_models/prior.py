import typing
import numpy as np
import h5py
import scipy.interpolate
import scipy.stats
from pop_models.types import Numeric, Parameters


class ParametersLogPrior(object):
    def __call__(self, parameters: Parameters) -> Numeric:
        raise NotImplementedError()



class KDEPriorHelperDataLoader(object):
    """
    Used by ``KDEPriorHelper`` to load data of a particular format.
    """
    @classmethod
    def is_supported(cls, fname) -> bool:
        raise NotImplementedError

    @classmethod
    def load(cls, fname: str, **kwargs) -> np.array:
        raise NotImplementedError


SamplesAndOptionalWeightsType = (
    typing.Tuple[np.array, typing.Optional[np.array]]
)

class KDEPriorHelperTextDataLoader(KDEPriorHelperDataLoader):
    """
    Used by ``KDEPriorHelper`` to load text files.
    """
    _supported_formats = (".dat",)
    @classmethod
    def is_supported(cls, fname) -> bool:
        return any(fname.endswith(fmt) for fmt in cls._supported_formats)

    @classmethod
    def load(cls, fname: str, **kwargs) -> SamplesAndOptionalWeightsType:
        raise NotImplementedError("TODO")


class KDEPriorHelperH5DataLoader(KDEPriorHelperDataLoader):
    """
    Used by ``KDEPriorHelper`` to load text files.
    """
    _supported_formats = (".h5", ".hdf5", ".hdf")
    @classmethod
    def is_supported(cls, fname) -> bool:
        return any(fname.endswith(fmt) for fmt in cls._supported_formats)

    @classmethod
    def load(cls, fname: str, **kwargs) -> SamplesAndOptionalWeightsType:
        if "path" not in kwargs:
            raise ValueError("Missing argument 'path' for HDF5 file.")

        path = kwargs["path"]
        field = kwargs.get("field", None)
        flatten = kwargs.get("flatten", False)

        if field is not None:
            if not isinstance(field, str):
                raise TypeError("'field' must be a string")

        if not isinstance(flatten, bool):
            raise TypeError("'flatten' must be a boolean")

        with h5py.File(fname, "r") as h5_file:
            dset = h5_file[path]

            if not isinstance(dset, h5py.Dataset):
                raise ValueError("'path' did not point to a Dataset")

            # Load the appropriate field from the dataset, or load everything if
            # none is given.
            if field is None:
                samples = dset[()]
            else:
                samples = dset[field]

            # Flatten the samples if requested.
            if flatten:
                samples = samples.flatten()

        # TODO: add support for non-None weights
        return samples, None


kde_prior_helper_data_loaders = (
    KDEPriorHelperTextDataLoader,
    KDEPriorHelperH5DataLoader,
)


class KDEPriorHelper(object):
    """
    A class for storing necessary information for a KDE Prior.  Is not itself a
    prior object, though.
    """
    def __init__(self, prior_settings: typing.Dict[str, typing.Any]) -> None:
        fname = prior_settings["fname"]

        self._interpolate = prior_settings.get("interpolate", False)

        if not isinstance(self.is_interpolated, bool):
            raise TypeError("'interpolate' must be a boolean")

        if (not self.is_interpolated) and "interp_options" in prior_settings:
            raise ValueError(
                "Specified interpolation options depsite interpolation being "
                "disabled."
            )

        interp_options = prior_settings.get("interp_options", {})
        fit_options = prior_settings.get("fit_options", {})
        file_options = prior_settings.get("file_options", {})

        for data_loader in kde_prior_helper_data_loaders:
            if data_loader.is_supported(fname):
                break
        else:
            raise ValueError("Sample file has unsupported format.")

        samples, weights = data_loader.load(fname, **file_options)

        self._fit_kde(samples, weights, **fit_options)

        if self.is_interpolated:
            self._interpolate_kde(**interp_options)

    @property
    def is_interpolated(self) -> bool:
        return self._interpolate

    def _fit_kde(self, samples, weights, **kwargs) -> None:
        bw_method = kwargs.get("bw_method")
        self._kde = scipy.stats.gaussian_kde(
            samples, weights=weights,
            bw_method=bw_method,
        )

    def _interpolate_kde(self, **kwargs) -> None:
        # Determine whether we interpolate f(x) or log(f(x)).  Default is log.
        _supported_func_scales = {"linear", "log"}
        self._func_scale = kwargs.get("func_scale", "log")
        if self._func_scale not in _supported_func_scales:
            raise ValueError(
                "Function scale must be one of: {}"
                .format(_supported_func_scales)
            )

        # Set up the limits of the grid.  Use the smallest and largest samples
        # if a user-specified value is not given.
        if "grid_min" in kwargs:
            x_min = kwargs["grid_min"]
        else:
            x_min = self._kde.dataset.min()
        if "grid_max" in kwargs:
            x_max = kwargs["grid_max"]
        else:
            x_max = self._kde.dataset.max()

        # Determine the number of grid points.
        n_grid_points = kwargs.get("n_grid_points", 100)

        # Create the grid.
        x_grid = np.linspace(x_min, x_max, n_grid_points)

        # Evaluate the KDE on the grid.
        f_of_x_grid = self._kde([x_grid])

        # Transform the evaluated KDE to the appropriate space.
        if self._func_scale == "linear":
            y_of_x_grid = f_of_x_grid
            fill_value = 0.0
        elif self._func_scale == "log":
            y_of_x_grid = np.log(f_of_x_grid)
            fill_value = np.NINF
        else:
            raise RuntimeError

        # Create the interpolator.
        self._interpolator = scipy.interpolate.RegularGridInterpolator(
            (x_grid,), y_of_x_grid,
            bounds_error=False, fill_value=fill_value,
        )

    def log_pdf(self, x: np.array) -> np.array:
        if self.is_interpolated:
            y = self._interpolator(x)

            if self._func_scale == "linear":
                log_f_of_x = np.log(y)
            elif self._func_scale == "log":
                log_f_of_x = y
            else:
                raise RuntimeError
        else:
            log_f_of_x = self._kde.logpdf(x)

        return log_f_of_x

    def rvs(
            self,
            n_samples: int,
            random_state: typing.Optional[np.random.RandomState]=None,
        ) -> np.array:
        return self._kde.resample(n_samples, seed=random_state)[0]
