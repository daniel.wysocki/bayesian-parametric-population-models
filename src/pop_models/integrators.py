__all__ = [
    "MCIntegrator",
    "AdaptiveMCIntegrator",
]

import numbers
import typing

import numpy

from .types import Numeric, Observables


class MCIntegrator(object):
    def __init__(
            self,
            random_state: numpy.random.RandomState=numpy.random.RandomState(),
        ):
        self.random_state = random_state

    def __call__(
            self,
            sampler: typing.Callable[...,Observables],
            integrand: typing.Callable[...,Numeric],
            sampler_args: typing.Optional[list]=None,
            sampler_kwargs: typing.Optional[dict]=None,
            integrand_args: typing.Optional[list]=None,
            integrand_kwargs: typing.Optional[dict]=None,
        ) -> Numeric:
        pass


class AdaptiveMCIntegrator(MCIntegrator):
    def __init__(
            self,
            random_state: numpy.random.RandomState=numpy.random.RandomState(),
            iter_max: int=2**16, iter_start: int=2**10,
            err_abs: float=1e-8, err_rel: float=1e-2,
        ):
        super().__init__(random_state=random_state)

        self.iter_max = iter_max
        self.iter_start = iter_start
        self.err_abs = err_abs
        self.err_rel = err_rel

        self._init_convergence()

    def _init_convergence(self):
        if self.err_abs is None and self.err_rel is None:
            def converged(
                    err_abs_current: float, err_rel_current: float,
                ) -> bool:
                return False
        elif self.err_abs is None:
            def converged(
                    err_abs_current: float, err_rel_current: float,
                ) -> bool:
                return err_rel_current < self.err_rel
        elif self.err_rel is None:
            def converged(
                    err_abs_current: float, err_rel_current: float,
                ) -> bool:
                return err_abs_current < self.err_abs
        else:
            def converged(
                    err_abs_current: float, err_rel_current: float,
                ) -> bool:
                return (
                    (err_rel_current < self.err_rel) and
                    (err_abs_current < self.err_abs)
                )

        self._converged = converged

    def __call__(
            self,
            sampler: typing.Callable[...,Observables],
            integrand: typing.Callable[...,Numeric],
            sampler_args=None, sampler_kwargs=None,
            integrand_args=None, integrand_kwargs=None,
        ) -> Numeric:
        # If some of the optional args/kwargs arguments weren't provided,
        # create empty lists/dicts that can be used in place of provided values.
        if sampler_args is None:
            sampler_args = []
        if sampler_kwargs is None:
            sampler_kwargs = {}
        if integrand_args is None:
            integrand_args = []
        if integrand_kwargs is None:
            integrand_kwargs = {}

        # Less descriptive shorthand for arguments
        p = sampler
        f = integrand

        # Initialize samples and integral accumulators.
        samples = 0
        new_samples = self.iter_start
        F = 0.0
        F2 = 0.0

        # Iteratively improve MC integral, until either the convergence
        # criteria set by ``err_abs`` and ``err_rel`` are reached, or the
        # maximum number of iterations is reached.
        while samples < self.iter_max:
            # Draw samples from the probability distribution function.
            X = p(
                new_samples, *sampler_args,
                random_state=self.random_state, **sampler_kwargs
            )

            # Evaluate the function at these samples.
            # Accumulate the sum of the result in F and the squared result in
            # F2, which will be used to calculate the integral and its
            # uncertainty.
            value = f(X, *integrand_args, **integrand_kwargs)
            F += numpy.sum(value)
            F2 += numpy.sum(value*value)

            # Update the number of samples appropriately.
            # If more samples are needed to converge, the number will be
            # doubled.
            samples += new_samples
            new_samples = samples

            # Estimate the integral as the sample average of f(X) over all
            # iterations.
            I_current = F / samples
            # Estimate the absolute error (standard error) and the relative
            # error (standard error divided by the integral).
            if samples < 2:
                # Integral error is the integral itself.
                err_abs_current = F
            else:
                # Use standard error expression.
                err_abs_current = numpy.sqrt(
                  (F2 - F*F / samples) / (samples*(samples-1.0))
                )
            if I_current == 0.0:
                # Handle special case of 0/0: relative error is 100%
                err_rel_current = 1.0
            else:
                # Use standard relative error expression.
                err_rel_current = err_abs_current / I_current

            # Test for convergence, and end the routine if it has been reached.
            if self._converged(err_abs_current, err_rel_current):
                break

        return I_current
