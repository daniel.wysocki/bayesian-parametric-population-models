import pop_models

# Once this module has been imported, the backend should be locked.
pop_models.lock_backend()

# Determine which JIT decorator to use.
backend = pop_models.get_backend()
if backend == "numpy":
    # Make a no-op decorator.
    def jit(*args, **kwargs):
        def decorator(func):
            return func
        return decorator
elif backend == "numba":
    try:
        from numba import jit
    except Exception as e:
        raise ImportError(
            "Cannot use the numba backend, as numba failed to import with "
            "error: {}"
            .format(str(e))
        )
else:
    raise RuntimeError(
        "Reached inaccessible code, backend should have been set to a known "
        "value."
    )
