import typing
import warnings

import numpy

from .types import Parameters
from .utils import quantile


CredibleRegionsBasicResult = typing.Dict[
    str, # label
    typing.Union[
        float, # median
        typing.Dict[float, float], # quantiles
        typing.Dict[float, typing.Tuple[float,float]], # equal prob bounds
    ]
]

def cr(quantiles, samples, weights=None):
    import numpy
    import scipy.interpolate

    samples = numpy.asarray(samples)

    assert samples.ndim == 1

    i_sort = numpy.argsort(samples)

    samples = samples[i_sort]

    if weights is None:
        n_samples = len(samples)
        weights = numpy.ones(n_samples, dtype=numpy.float64) / n_samples
    else:
        weights = weights[i_sort]

    cumweights = numpy.cumsum(weights)
    cumweights /= cumweights[-1]

    interp_value = scipy.interpolate.interp1d(
        cumweights, samples,
        kind="linear",
    )

    return [interp_value(q) for q in quantiles]


class CredibleRegions1D(object):
    def __init__(
            self,
            indep_variable: numpy.ndarray,
            parameters: typing.Optional[Parameters]=None,
            curves: typing.Optional[numpy.ndarray]=None,
            weights: typing.Optional[numpy.ndarray]=None,
            precomputed_crs: typing.Optional[dict]=None,
        ):
        self.indep_variable = indep_variable
        self.parameters = parameters
        self.curves = curves
        self.weights = weights
        self.precomputed_crs = (
            precomputed_crs if precomputed_crs is not None
            else {
                "quantiles" : {},
                "equal_prob_bounds" : {},
            }
        )

    def save(
            self,
            filename: str, mode: str,
            include_parameters: bool=True,
            include_curves: bool=True,
            include_crs: bool=True,
        ):
        import h5py

        if not (include_parameters or include_curves or include_crs):
            raise ValueError(
                "Attempting to save CredibleRegions1D but with no data "
                "included."
            )

        with h5py.File(filename, mode) as h5_file:
            h5_file.create_dataset("indep_variable", data=self.indep_variable)

            if include_parameters and (self.weights is not None):
                h5_file.create_dataset("weights", data=self.weights)

            if include_parameters and (self.parameters is not None):
                params_group = h5_file.create_group("parameters")
                for name, value in self.parameters.items():
                    params_group.create_dataset(name, data=value)

            if include_curves and (self.curves is not None):
                h5_file.create_dataset("curves", data=self.curves)

            if include_crs:
                crs_group = h5_file.create_group("crs")

                if "median" in self.precomputed_crs:
                    median = self.precomputed_crs["median"]
                    crs_group.create_dataset("median", data=median)

                if "mean" in self.precomputed_crs:
                    mean = self.precomputed_crs["mean"]
                    crs_group.create_dataset("mean", data=mean)

                if len(self.precomputed_crs.get("quantiles", {})) != 0:
                    quantiles_group = crs_group.create_group("quantiles")

                    quantiles = self.precomputed_crs["quantiles"]
                    for q, values in quantiles.items():
                        quantiles_group.create_dataset(str(q), data=values)

                if len(self.precomputed_crs.get("equal_prob_bounds", {})) != 0:
                    prob_group = crs_group.create_group("equal_prob_bounds")

                    bounds = self.precomputed_crs["equal_prob_bounds"]
                    for p, (xlo, xhi) in bounds.items():
                        values = numpy.row_stack((xlo, xhi))
                        prob_group.create_dataset(str(p), data=values)


    @staticmethod
    def load(filename):
        import h5py

        with h5py.File(filename, "r") as h5_file:
            indep_variable = h5_file["indep_variable"][()]

            if "weights" in h5_file:
                weights = h5_file["weights"][()]
            else:
                weights = None

            if "parameters" in h5_file:
                parameters = {
                    name : values[()]
                    for name, values in h5_file["parameters"].items()
                }
            else:
                parameters = None

            if "curves" in h5_file:
                curves = h5_file["curves"][()]
            else:
                curves = None

            if "crs" in h5_file:
                crs_group = h5_file["crs"]
                precomputed_crs = {}

                if "median" in crs_group:
                    precomputed_crs["median"] = crs_group["median"][()]

                if "mean" in crs_group:
                    precomputed_crs["mean"] = crs_group["mean"][()]

                if "quantiles" in crs_group:
                    quantiles_group = crs_group["quantiles"]

                    precomputed_crs["quantiles"] = {
                        float(q) : values[()]
                        for q, values in quantiles_group.items()
                    }

                if "equal_prob_bounds" in crs_group:
                    prob_group = crs_group["equal_prob_bounds"]

                    precomputed_crs["equal_prob_bounds"] = {
                        float(p) : tuple(values[()])
                        for p, values in prob_group.items()
                    }

        return CredibleRegions1D(
            indep_variable,
            parameters=parameters,
            curves=curves, weights=weights,
            precomputed_crs=precomputed_crs,
        )

    @staticmethod
    def from_samples(
            func: typing.Callable[[numpy.ndarray, Parameters], numpy.ndarray],
            indep_variable: numpy.ndarray,
            parameters: Parameters,
            weights: typing.Optional[numpy.ndarray]=None,
            args: typing.Optional[typing.Sequence[typing.Any]]=None,
            kwargs: typing.Optional[typing.Mapping[str, typing.Any]]=None,
        ):
        if args is None:
            args = []
        if kwargs is None:
            kwargs = {}

        curves = func(indep_variable, parameters, *args, **kwargs)
        return CredibleRegions1D(
            indep_variable,
            parameters=parameters,
            curves=curves,
            weights=weights,
        )


    @staticmethod
    def from_grid(
            func: typing.Callable[[numpy.ndarray, Parameters], numpy.ndarray],
            indep_variable: numpy.ndarray,
            parameters: Parameters,
            log_prob: numpy.ndarray,
            args: typing.Optional[typing.Sequence[typing.Any]]=None,
            kwargs: typing.Optional[typing.Mapping[str, typing.Any]]=None,
        ):
        if args is None:
            args = []
        if kwargs is None:
            kwargs = {}

        curves = func(indep_variable, parameters, *args, **kwargs)
        return CredibleRegions1D(
            indep_variable,
            parameters=parameters,
            curves=curves,
            weights=numpy.exp(log_prob-numpy.max(log_prob)),
        )


    def median(self, cache: bool=False):
        if "median" in self.precomputed_crs:
            return self.precomputed_crs["median"]
        else:
            med = self.quantile(0.50, cache=False)

            if cache:
                self.precomputed_crs["median"] = med

            return med


    def mean(self, cache: bool=False):
        if "mean" in self.precomputed_crs:
            return self.precomputed_crs["mean"]
        else:
            mean = numpy.average(self.curves, weights=self.weights, axis=0)

            if cache:
                self.precomputed_crs["mean"] = mean

            return mean


    def quantile(self, q: float, cache: bool=False):
        if q in self.precomputed_crs["quantiles"]:
            return self.precomputed_crs["quantiles"][q]
        else:
            x_q = quantile(
                self.curves, q,
                weights=self.weights,
                axis=0,
            )

            if cache:
                self.precomputed_crs["quantiles"][q] = x_q

            return x_q


    def equal_prob_bounds(self, p: float, cache: bool=False):
        if p in self.precomputed_crs["equal_prob_bounds"]:
            return self.precomputed_crs["equal_prob_bounds"][p]
        else:
            q_lo, q_hi = 0.5 * (1.0 - p), 0.5 * (1.0 + p)

            x_lo = self.quantile(q_lo, cache=False)
            x_hi = self.quantile(q_hi, cache=False)

            bounds = x_lo, x_hi

            if cache:
                self.precomputed_crs["equal_prob_bounds"][p] = bounds

            return bounds

    def plot(
            self,
            ax,
            include_median: bool=False,
            include_mean: bool=False,
            quantiles: typing.Optional[typing.List[float]]=None,
            equal_prob_bounds: typing.Optional[typing.List[float]]=None,
            color="black", bounds_alpha=0.2,
            median_style="solid", mean_style="dashed", quantile_style="dotted",
            median_lw=2, mean_lw=2, quantile_lw=2,
            orientation="horizontal",
            quantile_labels: bool=False,
            x_scale: str="linear", y_scale: str="linear",
            label: typing.Optional[str]=None,
            cache: bool=False,
        ):
        if orientation == "horizontal":
            x = self.indep_variable
        elif orientation == "vertical":
            y = self.indep_variable
        else:
            raise ValueError(
                "Cannot plot orientation '{}'. "
                "Must be 'horizontal' or 'vertical'."
                .format(orientation)
            )

        ax.set_xscale(x_scale)
        ax.set_yscale(y_scale)

        if (x_scale == "linear") and (y_scale == "linear"):
            plot = ax.plot
        elif (x_scale == "linear") and (y_scale == "log"):
            plot = ax.semilogy
        elif (x_scale == "log") and (y_scale == "linear"):
            plot = ax.semilogx
        elif (x_scale == "log") and (y_scale == "log"):
            plot = ax.loglog
        else:
            raise ValueError("Unknown either x_scale or y_scale")

        if include_mean:
            mean = self.mean(cache=cache)
            if orientation == "horizontal":
                plot_x = x
                plot_y = mean
            else:
                plot_x = mean
                plot_y = y

            plot(
                plot_x, plot_y,
                color=color, linewidth=mean_lw, linestyle=mean_style,
                label=label,
            )
            label = None

        if include_median:
            median = self.median(cache=cache)
            if orientation == "horizontal":
                plot_x = x
                plot_y = median
            else:
                plot_x = median
                plot_y = y

            plot(
                plot_x, plot_y,
                color=color, linewidth=median_lw, linestyle=median_style,
                label=label,
            )
            label = None

        if quantiles is not None:
            lines = []

            for q in quantiles:
                quantile = self.quantile(q, cache=cache)
                if orientation == "horizontal":
                    plot_x = x
                    plot_y = quantile
                else:
                    plot_x = quantile
                    plot_y = y

                line = plot(
                    plot_x, plot_y,
                    color=color,
                    linewidth=quantile_lw, linestyle=quantile_style,
                    label=label,
                )[0]
                label = None

                if quantile_labels:
                    from labellines import labelLine
                    # Label quantile as a percentage, trimming off trailing
                    # zeros.
                    quantile_label = "{:g}%".format(100*q)
                    # Determine where to put the label along the line.  As a
                    # rule of thumb, we put it at 80% of the way along the
                    # x-axis to the highest point.
                    argmin, argmax = plot_x.argmin(), plot_x.argmax()
                    ptp = numpy.ptp(plot_x)
                    if y[argmin] > y[argmax]:
                        if x_scale == "linear":
                            label_loc = 0.2*ptp + plot_x[argmin]
                        else:
                            label_loc = ptp**0.2 + plot_x[argmin]
                    else:
                        if x_scale == "linear":
                            label_loc = 0.8*ptp + plot_x[argmin]
                        else:
                            label_loc = ptp**0.8 + plot_x[argmin]

                    labelLine(
                        line, label_loc,
                        label=quantile_label,
                        zorder=2.5, align=False,
                    )

        if equal_prob_bounds is not None:
            for p in equal_prob_bounds:
                lo, hi = self.equal_prob_bounds(p, cache=cache)

                if orientation == "horizontal":
                    print(lo, hi)
                    ax.fill_between(
                        x, lo, hi,
                        color=color, alpha=bounds_alpha,
                        label=label,
                    )
                else:
                    ax.fill_betweenx(
                        y, lo, hi,
                        color=color, alpha=bounds_alpha,
                        label=label,
                    )
                label = None

        return ax

    @classmethod
    def overlay_plot(cls, cli_args):
        import matplotlib
        matplotlib.use(cli_args.mpl_backend)
        import matplotlib.pyplot as plt

        from .utils.plotting import limit_ax_oom

        fig, ax = plt.subplots()

        for fname, color, label in cli_args.crs:
            cr = cls.load(fname)

            include_median = {
                "on" : True, "off" : False,
                "auto" : cr.median is not None,
            }[cli_args.median]
            include_mean = {
                "on" : True, "off" : False,
                "auto" : cr.mean is not None,
            }[cli_args.mean]

            quantiles = (
                cr.precomputed_crs.get("quantiles")
                if cli_args.quantiles is None
                else cli_args.quantiles
            )
            equal_prob_bounds = (
                cr.precomputed_crs.get("equal_prob_bounds")
                if cli_args.equal_prob_bounds is None
                else cli_args.equal_prob_bounds
            )

            cr.plot(
                ax,
                include_median=include_median,
                include_mean=include_mean,
                quantiles=quantiles,
                equal_prob_bounds=equal_prob_bounds,
                x_scale=cli_args.x_scale, y_scale=cli_args.y_scale,
                bounds_alpha=cli_args.bounds_alpha,
                color=color, label=label,
            )

        if cli_args.n_oom is not None:
            limit_ax_oom(ax, cli_args.n_oom, "y")

        x_min, x_max = ax.get_xlim()
        if cli_args.fix_x_min:
            x_min = cli_args.fix_x_min
        if cli_args.fix_x_max:
            x_max = cli_args.fix_x_max

        y_min, y_max = ax.get_ylim()
        if cli_args.fix_y_min:
            y_min = cli_args.fix_y_min
        if cli_args.fix_y_max:
            y_max = cli_args.fix_y_max

        ax.set_xlim([x_min, x_max])

        ax.set_xlabel(cli_args.x_label)
        ax.set_ylabel(cli_args.y_label)

        ax.legend(loc="best")

        fig.savefig(cli_args.output)

    @staticmethod
    def make_parser():
        import argparse

        parser = argparse.ArgumentParser()

        subparsers = parser.add_subparsers(dest="command")

        ## Overlay Plot ##
        subparser_overlay = subparsers.add_parser("overlay")

        subparser_overlay.add_argument(
            "output",
            help="File to output plot to.",
        )

        subparser_overlay.add_argument(
            "--crs",
            nargs=3, metavar=("FILE", "COLOR", "LABEL"),
            action="append", required=True,
            help="Overlay another set of CR's.  Must specify a color and "
                 "label.",
        )

        subparser_overlay.add_argument(
            "--median",
            choices=["on", "off", "auto"], default="auto",
            help="Determine whether to include medians.",
        )
        subparser_overlay.add_argument(
            "--mean",
            choices=["on", "off", "auto"], default="auto",
            help="Determine whether to include means.",
        )
        subparser_overlay.add_argument(
            "--quantiles",
            type=float, nargs="*",
            help="Specify quantiles to include -- must be attainable from the "
                 "credible interval files included.  If not specified, will "
                 "use whatever each file provides.",
        )
        subparser_overlay.add_argument(
            "--equal-prob-bounds",
            type=float, nargs="*",
            help="Specify equal probability bounds to include -- must be "
                 "attainable from the credible interval files included.  If "
                 "not specified, will use whatever each file provides.",
        )

        subparser_overlay.add_argument(
            "--x-scale",
            choices=["linear", "log"],
            default="linear",
            help="Set scale for x-axis.",
        )
        subparser_overlay.add_argument(
            "--y-scale",
            choices=["linear", "log"],
            default="linear",
            help="Set scale for y-axis.",
        )

        subparser_overlay.add_argument(
            "--x-label",
            default="x",
            help="Set label for x-axis.  May use LaTeX code.",
        )
        subparser_overlay.add_argument(
            "--y-label",
            default="y",
            help="Set label for y-axis.  May use LaTeX code.",
        )

        subparser_overlay.add_argument(
            "--n-oom",
            type=int,
            help="Limit plots' y-axes to the specified number of OoM from the "
                 "peak.",
        )

        subparser_overlay.add_argument(
            "--fix-x-min",
            type=float,
            help="Fix the minimum x value.",
        )
        subparser_overlay.add_argument(
            "--fix-x-max",
            type=float,
            help="Fix the maximum x value.",
        )

        subparser_overlay.add_argument(
            "--fix-y-min",
            type=float,
            help="Fix the minimum y value.",
        )
        subparser_overlay.add_argument(
            "--fix-y-max",
            type=float,
            help="Fix the maximum y value.",
        )

        subparser_overlay.add_argument(
            "--bounds-alpha",
            type=float, default=0.2,
            help="Alpha value for filled regions.",
        )

        subparser_overlay.add_argument(
            "--fig-size", metavar=("WIDTH", "HEIGHT"),
            type=float, nargs=2, default=(4.0, 4.0),
            help="Width and height of figure in inches.  Default is 4 by 4",
        )

        subparser_overlay.add_argument(
            "--mpl-backend",
            default="Agg",
            help="Backend to use for matplotlib.",
        )

        return parser

    @classmethod
    def cli(cls, raw_args=None):
        if raw_args is None:
            import sys
            raw_args = sys.argv[1:]

        cli_parser = cls.make_parser()
        cli_args = cli_parser.parse_args(raw_args)

        if cli_args.command == "overlay":
            cls.overlay_plot(cli_args)

        else:
            raise KeyError(
                "Unknown command '{c}' provided.".format(c=cli_args.command)
            )


class CredibleRegionsBasic(object):
    def __init__(
            self,
            include_median: bool=False,
            quantiles: typing.Optional[typing.Sequence[float]]=None,
            equal_prob_bounds: typing.Optional[typing.Sequence[float]]=None,
            interpolation: str="linear",
        ) -> None:
        no_op = (
            (not include_median) and
            (quantiles is None) and
            (equal_prob_bounds is None)
        )
        if no_op:
            warnings.warn(
                "No credible regions will be computed, as all options were "
                "left blank.  You probably didn't mean to do this."
            )

        self.include_median = include_median
        self.quantiles = quantiles
        self.equal_prob_bounds = equal_prob_bounds

        self.interpolation = interpolation

    def compute(
            self,
            samples: numpy.ndarray,
            weights: typing.Optional[numpy.ndarray]=None,
        ) -> CredibleRegionsBasicResult:
        result = {}

        if self.include_median:
            result["median"] = quantile(
                samples, 0.5, weights=weights,
                interpolation=self.interpolation,
            )

        if self.quantiles is not None:
            xs = quantile(
                samples, self.quantiles, weights=weights,
                interpolation=self.interpolation,
            )

            result["quantiles"] = {q: x for q, x in zip(self.quantiles, xs)}

        if self.equal_prob_bounds is not None:
            result["equal_prob_bounds"] = {}

            for p in self.equal_prob_bounds:
                q_lo, q_hi = 0.5*(1.0-p), 0.5*(1.0+p)
                x_lo, x_hi = quantile(
                    samples, (q_lo, q_hi), weights=weights,
                    interpolation=self.interpolation,
                )
                result["equal_prob_bounds"][p] = (x_lo, x_hi)

        return result

    @staticmethod
    def format_result(result: CredibleRegionsBasicResult, format: str="json"):
        ## TODO: Add ASCII option
        supported_formats = ["json"]
        if format not in supported_formats:
            raise KeyError(
                "Unsupported format '{}', must be one of: {}"
                .format(format, ", ".join(supported_formats))
            )

        if format == "json":
            import json
            print(json.dumps(result, indent=True))
        else:
            raise RuntimeError
