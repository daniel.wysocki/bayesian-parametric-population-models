from __future__ import division, print_function

labels = {
    "rate": r"\mathcal{R}",
    "log_rate": r"\log\mathcal{R}",
    "alpha": r"\alpha",
    "m_min": r"m_{\mathrm{min}}",
    "m_max": r"m_{\mathrm{max}}",
    "M_max": r"M_{\mathrm{max}}",
    "log_alpha_chi1": r"\log\alpha_{\chi_1}",
    "log_beta_chi1": r"\log\beta_{\chi_1}",
    "log_alpha_chi2": r"\log\alpha_{\chi_2}",
    "log_beta_chi2": r"\log\beta_{\chi_2}",
    "log_sigma_chi1": r"\log\sigma_{\chi_1}",
    "log_sigma_chi2": r"\log\sigma_{\chi_2}",
    "alpha_chi1": r"\alpha_{\chi_1}",
    "beta_chi1": r"\beta_{\chi_1}",
    "alpha_chi2": r"\alpha_{\chi_2}",
    "beta_chi2": r"\beta_{\chi_2}",
    "sigma_chi1": r"\sigma_{\chi_1}",
    "sigma_chi2": r"\sigma_{\chi_2}",
}

units = {
    "rate": r"\mathrm{Gpc}^{-3} \, \mathrm{yr}^{-1}",
    "m_min": r"\mathrm{M}_{\odot}",
    "m_max": r"\mathrm{M}_{\odot}",
    "M_max": r"\mathrm{M}_{\odot}",
}
