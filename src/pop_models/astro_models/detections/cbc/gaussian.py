import typing
import numpy

from ...coordinates import coordinates, transformations
from ....coordinate import CoordinateSystem, CoordinateTransforms
from ....detection import GaussianDetection

__all__ = [
    "CBCGaussianDetection",
]

class CBCGaussianDetection(GaussianDetection):
    def __init__(
            self,
            coord_system: CoordinateSystem,
            mean: numpy.ndarray,
            covariance: typing.Optional[numpy.ndarray]=None,
            inv_covariance: typing.Optional[numpy.ndarray]=None,
            transformations: CoordinateTransforms=transformations,
            truth: typing.Optional[numpy.ndarray]=None,
        ) -> None:
        super().__init__(
            coord_system,
            mean, covariance=covariance, inv_covariance=inv_covariance,
            transformations=transformations,
            truth=truth,
        )

        # Store the original covariance and inverse covariances given, so we can
        # serialize to a file, using exactly what was initially given.
        self.base_covariance = covariance
        self.base_inv_covariance = inv_covariance

    def save(self, filename):
        """
        Save this CBCGaussianDetection to a JSON file.
        """
        import json

        data = {}

        data["names"] = [coord.name for coord in self.coord_system]
        data["mean"] = [item for item in self.mean]

        if self.truth is not None:
            data["truth"] = [item for item in self.truth]

        if self.base_covariance is not None:
            data["covar"] = [
                [item for item in row]
                for row in self.base_covariance
            ]
        if self.base_inv_covariance is not None:
            data["inv_covar"] = [
                [item for item in row]
                for row in self.base_inv_covariance
            ]

        with open(filename, "w") as f:
            json.dump(data, f)


    @classmethod
    def load(cls, filename, dtype=numpy.float64):
        """
        Load a CBCGaussianDetection from a JSON file.  File must have 'names'
        and 'mean' field, as well as either of 'covar' and 'inv_covar'.  Can
        also have a 'truth' field when using synthetic data.

        Example file::
            {
                "names" : ["m1_source", "m2_source"],
                "mean" : [40.0, 35.0],
                "covar" : [
                    [8.0, 0.1],
                    [0.1, 6.0],
                ]
            }
        """
        import json
        import textwrap

        # Load raw data from JSON file.
        with open(filename, "r") as f:
            data = json.load(f)

        # Check that there's a names field.
        has_names = "names" in data
        # Check that there's a truth field.
        has_truth = "truth" in data
        # Check that there's a mean field.
        has_mean = "mean" in data
        # Check if there's a covariance or inverse covariance field.
        has_covar = "covar" in data
        has_inv_covar = "inv_covar" in data
        has_any_covar = has_covar or has_inv_covar

        # Raise an exception if any required fields are missing.
        # If missing all fields, error message should be:
        # "Missing fields: 'names', 'mean', either of 'covar' or 'inv_covar'"
        # Build that string up, only including the parts that are actually
        # missing.
        if not all([has_names, has_mean, has_any_covar]):
            missing_fields = []
            if not has_names:
                missing_fields.append("'names'")
            if not has_mean:
                missing_fields.append("'mean'")
            if not has_any_covar:
                missing_fields.append("either of 'covar' and 'inv_covar'")

            raise ValueError(
                "Missing fields: {}".format(", ".join(missing_fields))
            )

        # Validate that each of the 'names', 'mean', 'covar', and 'inv_covar'
        # fields (when present) can all be converted to proper arrays of
        # compatible dimensions.  'names' and 'mean' should have shape ``(n,)``,
        # and the other two should have shapes ``(n, n)``.
        def is_list_of_dtype(obj, dtype=dtype) -> bool:
            # Exit if not a list.
            if not isinstance(obj, list):
                return False
            # Exit if any entries cannot be converted to 'dtype', or if that
            # conversion does not produce a scalar.
            for item in obj:
                try:
                    if numpy.ndim(dtype(item)) != 0:
                        return False
                except ValueError:
                    return False
            # Nothing failed, so it passes.
            return True
        def is_list_matrix(obj) -> bool:
            # Exit if not a list.
            if not isinstance(obj, list):
                return False
            # Exit if any row is not a list of dtype.
            for row in obj:
                if not is_list_of_dtype(row):
                    return False
            # Nothing failed, so it passes.
            return True

        # Names should be a list of strings.
        names_valid = is_list_of_dtype(data["names"], dtype=str)
        # Truth should be a list of dtype, or None.
        truth_valid = True
        if has_truth:
            truth_valid = is_list_of_dtype(data["truth"])
        # Mean should be a list of dtype.
        mean_valid = is_list_of_dtype(data["mean"])
        # Covar and inverse covar should be list of lists of dtype, or None.
        covar_valid = True
        if has_covar:
            covar_valid = isinstance(data["covar"], list)
            for row in data["covar"]:
                covar_valid &= is_list_of_dtype(row)
        inv_covar_valid = True
        if has_inv_covar:
            inv_covar_valid = isinstance(data["inv_covar"], list)
            for row in data["inv_covar"]:
                inv_covar_valid &= is_list_of_dtype(row)

        # If any had the wrong type, raise an exception.  If all have the wrong
        # types, the message will be:
        # Malformed fields, expected:
        #   'names' : list of 'str'
        #   'truth' : list of 'dtype'
        #   'mean' : list of 'dtype'
        #   'covar' : list of lists of 'dtype'
        #   'inv_covar' : list of lists of 'dtype'
        # Any that were not malformed will be removed from the error message.
        if not all([names_valid, mean_valid, covar_valid, inv_covar_valid]):
            malformed_fields = []
            if not names_valid:
                malformed_fields.append("'names' : list of 'str'")
            if not truth_valid:
                malformed_fields.append("'truth' : list of '{}'".format(dtype))
            if not mean_valid:
                malformed_fields.append("'mean' : list of '{}'".format(dtype))
            if not covar_valid:
                malformed_fields.append(
                    "'covar' : list of lists of '{}'".format(dtype)
                )
            if not inv_covar_valid:
                malformed_fields.append(
                    "'inv_covar' : list of lists of '{}'".format(dtype)
                )

            msg = "Malformed fields, expected:\n{}".format(
                "\n".join(malformed_fields)
            )

        # Check the dimensions all match 'names'.
        n_dim = len(data["names"])

        if has_truth:
            truth = numpy.asarray(data["truth"], dtype=dtype)
            truth_valid = truth.shape == (n_dim,)
        else:
            truth = None
            truth_valid = True

        mean = numpy.asarray(data["mean"], dtype=dtype)
        mean_valid = mean.shape == (n_dim,)

        if has_covar:
            covar = numpy.asarray(data["covar"], dtype=dtype)
            covar_valid = covar.shape == (n_dim, n_dim)
        else:
            covar = None
            covar_valid = True

        if has_inv_covar:
            inv_covar = numpy.asarray(data["inv_covar"], dtype=dtype)
            inv_covar_valid = inv_covar.shape == (n_dim, n_dim)
        else:
            inv_covar = None
            inv_covar_valid = True

        # If any had the wrong shape, raise an exception.  If any have the wrong
        # shape, the message will be:
        # Array mismatch.
        # 'names' has 'n_dim' entries, some other fields incompatible:
        #     'truth': Expected '(n_dim,)', got 'truth.shape'
        #     'mean': Expected '(n_dim,)', got 'mean.shape'
        #     'covar': Expected '(n_dim, n_dim)', got 'covar.shape'
        #     'inv_covar': Expected '(n_dim, n_dim)', got 'inv_covar.shape'
        #
        # Any of 'truth', 'covar', and 'inv_covar' will be replaced with
        # 'field': Not given
        # if not provided.
        if not all([mean_valid, covar_valid, inv_covar_valid]):
            if has_truth:
                truth_msg = (
                    "Expected '({n_dim},)', got '{actual}'"
                    .format(n_dim=n_dim, actual=truth.shape)
                )
            else:
                truth_msg = "'covar': Not given"

            mean_msg = (
                "Expected '({n_dim},)', got '{actual}'"
                .format(n_dim=n_dim, actual=mean.shape)
            )

            if has_covar:
                covar_msg = (
                    "Expected '({n_dim}, {n_dim})', got '{actual}'"
                    .format(n_dim=n_dim, actual=covar.shape)
                )
            else:
                covar_msg = "'covar': Not given"
            if has_inv_covar:
                inv_covar_msg = (
                    "Expected '({n_dim}, {n_dim})', got '{actual}'"
                    .format(n_dim=n_dim, actual=inv_covar.shape)
                )
            else:
                inv_covar_msg = "'inv_covar': Not given"

            raise ValueError(textwrap.dedent("""\
                Array mismatch.
                'names' has '{n_dim}' entries, some other fields incompatible:
                    'truth': {truth_msg}
                    'mean': {mean_msg}
                    'covar': {covar_msg}
                    'inv_covar': {inv_covar_msg}\
                """.format(
                    n_dim=n_dim,
                    truth_msg=truth_msg,
                    mean_msg=mean_msg,
                    covar_msg=covar_msg, inv_covar_msg=inv_covar_msg,
                )
            ))

        # Validate and then construct coordinate system
        unknown_coords = [
            name for name in data["names"] if name not in coordinates
        ]
        if len(unknown_coords) != 0:
            raise KeyError(
                "Unknown coordinates: {}".format(", ".join(unknown_coords))
            )

        coord_system = CoordinateSystem(*(
            coordinates[name] for name in data["names"]
        ))

        # Return appropriate CBCGaussianDetection object.
        return cls(
            coord_system,
            mean, covariance=covar, inv_covariance=inv_covar,
            truth=truth,
        )
