import typing

import numpy
import astropy.cosmology
from astropy.cosmology import Planck15
import astropy.units as units

from pop_models.coordinate import CoordinateSystem, CoordinateTransforms
from pop_models.population import Population
from pop_models.types import Parameters, Observables, Numeric, WhereType
from pop_models.astro_models.coordinates import z_coord, transformations

z_coord_system = CoordinateSystem(z_coord)

class CosmoPopulation(Population):
    """
    Base class for populations with a cosmology.
    """
    def __init__(
            self,
            coord_system: CoordinateSystem,
            param_names: typing.Tuple[str],
            cosmology: astropy.cosmology.core.Cosmology=Planck15,
            z_max: float=numpy.inf,
            transformations: CoordinateTransforms=transformations,
        ) -> None:
        super().__init__(
            coord_system, param_names,
            transformations=transformations,
        )

        self._cosmology = cosmology
        self._z_max = z_max
        self._Vc_max = cosmology.comoving_volume(z_max).to(units.Mpc**3).value

    @property
    def cosmology(self) -> astropy.cosmology.core.Cosmology:
        """
        Cosmology used in this population.
        """
        return self._cosmology

    @property
    def z_max(self) -> float:
        """
        Maximum redshift supported by this population.
        """
        return self._z_max

    @property
    def Vc_max(self) -> float:
        """
        Comoving volume out to this population's maximum redshift.
        """
        return self._Vc_max


class UniformComovingVolumePopulation(CosmoPopulation):
    def __init__(
            self,
            z_max: float,
            cosmology: astropy.cosmology.core.Cosmology=Planck15,
            transformations: CoordinateTransforms=transformations,
        ) -> None:
        param_names = ()

        super().__init__(
            z_coord_system, param_names,
            cosmology=cosmology,
            z_max=z_max,
            transformations=transformations,
        )

    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: typing.Optional[numpy.random.RandomState]=None,
        ) -> Observables:
        if random_state is None:
            random_state = numpy.random.RandomState()

        Vc_samples = (
            random_state.uniform(0.0, self.Vc_max, n_samples) *
            units.Mpc**3
        )
        # TODO: make more efficient by pre-tabulating in `__init__` and
        # interpolating.
        z_samples = numpy.asarray([
            astropy.cosmology.z_at_value(self.cosmology.comoving_volume, Vc)
            for Vc in Vc_samples
        ])

        return z_samples,

    def pdf(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
        ) -> Numeric:
        """
        TODO: Not quite the pdf, but it's what is needed in the current usage
        within :mod:`pop_models.astro_models.gw_ifo_vt`.  Need to think about
        what is correct.
        """
        z, = observables
        return self.cosmology.differential_comoving_volume(z)

class MadauDickinsonPopulation(CosmoPopulation):
    def __init__(
            self,
            z_max: float,
            cosmology: astropy.cosmology.core.Cosmology=Planck15,
            transformations: CoordinateTransforms=transformations,
        ) -> None:
        param_names = ()

        super().__init__(
            z_coord_system, param_names,
            cosmology=cosmology,
            z_max=z_max,
            transformations=transformations,
        )

    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: typing.Optional[numpy.random.RandomState]=None,
        ) -> Observables:
        if random_state is None:
            random_state = numpy.random.RandomState()

        raise NotImplementedError("TODO: implement MD via inverse CDF sampling")

    def pdf(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
        ) -> Numeric:
        """
        TODO: Not quite the pdf, but it's what is needed in the current usage
        within :mod:`pop_models.astro_models.gw_ifo_vt`.  Need to think about
        what is correct.
        """
        z, = observables
        zp1 = 1.0 + z

        dVdz = self.cosmology.differential_comoving_volume(z)
        sfr = zp1**2.7 / (1 + (zp1/2.9)**5.6)

        return dVdz * sfr
