import typing

import numpy

from ...coordinate import CoordinateSystem, CoordinateTransforms
from ...population import Population
from ...types import WhereType, Observables, Parameters, ParametersSingle

from ..coordinates import transformations

class StellarEvolutionPopulation(Population):
    """
    Takes an initial population, and evolves it forward.
    """
    def __init__(
            self,
            initial_population: Population,
            coord_system_init: CoordinateSystem,
            coord_system_final: CoordinateSystem,
            param_names_evo: typing.Tuple[str,...],
            transformations: CoordinateTransforms=transformations,
        ) -> None:
        param_names = initial_population.param_names + param_names_evo
        super().__init__(
            coord_system_final, param_names,
            transformations=transformations,
        )
        self.initial_population = (
            initial_population.to_coords(coord_system_init)
        )

    def initial_to_final_single(
            self,
            observables_init: Observables,
            parameters: ParametersSingle,
        ) -> Observables:
        raise NotImplementedError

    def initial_to_final(
            self,
            observables_init: Observables,
            parameters: Parameters,
        ) -> Observables:
        obs_shape = numpy.shape(observables_init[0])
        params_shape = numpy.shape(next(iter(parameters.values())))

        where_arr = numpy.broadcast_to(where, params_shape)

        observables_final = tuple(
            numpy.full(
                obs_shape,
                fill_value=coord.fill_value, dtype=numpy.float64,
            )
            for coord in self.coord_system
        )

        for idx in numpy.ndindex(*params_shape):
            if not where_arr[idx]:
                continue

            observables_at_idx = tuple(obs[idx] for obs in observables_init)
            parameters_at_idx = {
                name : values[idx]
                for name, values in parameters.items()
            }

            observables_final_single = self.initial_to_final_single(
                observables_at_idx, parameters_at_idx,
            )

            iterables = zip(observables_final, observables_final_single)
            for obs_out, obs in iterables:
                obs_out[idx] = obs

        return observables_final

    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: typing.Optional[numpy.random.RandomState]=None,
            **kwargs
        ) -> Observables:
        observables_init = self.initial_population.rvs(
            n_samples, parameters,
            where=where, random_state=random_state, **kwargs
        )
        return self.initial_to_final(
            observables_init, parameters,
        )
