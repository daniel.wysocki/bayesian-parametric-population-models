import typing

import numpy

from ...coordinate import CoordinateSystem, CoordinateTransforms
from ...population import Population
from ...types import WhereType, Numeric, Observables, Parameters

from ..coordinates import m_source_coord, transformations


coord_system = CoordinateSystem(m_source_coord)

class InitialMassFunction(Population):
    def __init__(
            self,
            param_names: typing.Tuple[str,...],
            transformations: CoordinateTransforms=transformations,
        ) -> None:
        super().__init__(
            coord_system, param_names,
            transformations=transformations,
        )

class SalpeterIMF(InitialMassFunction):
    def __init__(
            self,
            transformations: CoordinateTransforms=transformations,
            m_min_name: str="m_min_IMF",
        ) -> None:
        param_names = (m_min_name,)
        super().__init__(param_names, transformations=transformations)
        self.m_min_name = m_min_name

    def pdf(
            self,
            observables: Observables, parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        m_source, = observables

        return (
            1.35 *
            numpy.multiply.outer(
                parameters[self.m_min_name]**1.35,
                m_source**-2.35,
            )
        )

    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: typing.Optional[numpy.random.RandomState]=None,
            **kwargs
        ) -> Observables:
        if random_state is None:
            random_state = numpy.random.RandomState()

        # 1/-1.35
        exponent_inv = -0.7407407407407407

        params_shape = numpy.shape(next(iter(parameters.values())))
        out_shape = params_shape + (n_samples,)

        # Draw uniform samples on [0,1) so we can inverse CDF sample.
        U = random_state.uniform(size=out_shape)

        # Pull lower mass cutoff from parameters.
        m_min = parameters[self.m_min_name]

        # Plug into inverse CDF
        m_source = (1-U)**exponent_inv * m_min[...,None]

        return m_source,
