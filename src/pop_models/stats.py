r"""
This module defines generally useful statistical functions.
"""
import numpy
import scipy.special
from pop_models.optimization import jit


@jit(parallel=True, forceobj=True)
def beta_lnpdf(x, alpha, beta, out=None, where=True):
    r"""
    Computes the natural log of the probability density of a beta distribution

    ..math::
       \ln\mathrm{Beta}(x;\alpha,\beta) =
       (1-\alpha)\ln(x) + (1-\beta)\ln(1-x) - \ln(\mathrm{B}(\alpha,\beta))

    at the points ``x``, given parameters ``alpha`` and ``beta``.

    :param array_like x: shape ``S``
        Points to evaluate the probability density at.

    :param array_like alpha: shape ``T`` or compatible
        :math:`\alpha` parameters to use in defining the distribution.  Will be
        broadcast such that ``alpha`` and ``beta`` have the same shape.

    :param array_like beta: shape ``T`` or compatible
        :math:`\beta` parameters to use in defining the distribution.  Will be
        broadcast such that ``alpha`` and ``beta`` have the same shape.

    :param out: (optional)
        Array to write the result to.  Must have shape ``T + S`` if provided.

    :type out: ndarray or None

    :param array_like where: (optional)
        Values of ``True`` indicate to calculate the probability density at that
        position, values of ``False`` indicate to leave the value in the output
        alone.  Must have shape compatible to ``T + S``.

    :return: array_like, shape ``T + S``
        Value of the probability density evaluated at each point.
    """
    x = numpy.asarray(x)
    alpha, beta = numpy.broadcast_arrays(alpha, beta)

    S = numpy.shape(x)
    T = numpy.shape(alpha)
    TS = T + S

    where_arr = numpy.broadcast_to(numpy.asarray(where).T, reversed(TS)).T

    out_type = x.dtype

    if out is None:
        out = numpy.empty(TS, dtype=out_type)
    elif out.shape != TS:
        raise ValueError(
            "Operands could not be broadcast together with shapes {} {} {}"
            .format(S, T, out.shape)
        )

    # Initialize temporary arrays with shapes ``T`` and ``TS`` for holding
    # intermediate results in an efficient manner.
    tmp_T = numpy.empty(T, dtype=out_type)
    tmp_TS = numpy.empty(TS, dtype=out_type)

    # Compute ``alpha-1`` and store in ``tmp_T``, and then compute
    # ``(alpha-1)*ln(x)`` and store in ``out``.  ``tmp_T`` is free to be
    # overwritten after this.
    alpha_minus_one = numpy.subtract(alpha, 1.0, out=tmp_T)
    alpha_minus_one_log_x = scipy.special.xlogy.outer(
        alpha_minus_one, x,
        out=out, where=where_arr,
    )

    # Compute ``beta-1`` and store in ``tmp_T``, and then compute
    # ``(beta-1)*ln(1-x)`` and store in ``tmp_TS``.  ``tmp_T`` is free to be
    # overwritten after this.
    beta_minus_one = numpy.subtract(beta, 1.0, out=tmp_T)
    beta_minus_one_log_one_minus_x = scipy.special.xlog1py.outer(
        beta_minus_one, -x,
        out=tmp_TS, where=where_arr,
    )

    # Compute ``(alpha-1)*ln(x) + (beta-1)*ln(1-x)`` and store in ``out``.
    # ``tmp_TS`` is no longer needed after this, and it is marked for garbage
    # collection.
    numerator = numpy.add(
        alpha_minus_one_log_x, beta_minus_one_log_one_minus_x,
        out=out, where=where_arr,
    )
#    del tmp_TS

    # Compute ``ln(B(alpha, beta))``, and store in ``tmp_T``.
    denominator = scipy.special.betaln(alpha, beta, out=tmp_T)

    # Compute the final result, storing it in ``out``.  The arrays are being
    # transposed because numpy broadcasting works from the last dimension
    # backwards.  Our arrays have shapes ``T+S`` and ``T``, which are not
    # compatible, so by transposing everything we change our arrays to have
    # shapes S[::-1]+T[::-1] and T[::-1], which are compatible.  The transpose
    # of the output array ensures the original ordering is kept.
    numpy.subtract(
        numerator.T, denominator.T,
        out=out.T, where=where_arr.T,
    )

    # Zero-out unsupported indices
    i_unsupported = (x <= 0.0) | (x >= 1.0)
    out[...,i_unsupported] = numpy.NINF

    # Return the final result.
    return out


def beta_pdf(x, alpha, beta, out=None, where=True):
    r"""
    Computes the probability density of a beta distribution

    ..math::
       \mathrm{Beta}(x;\alpha,\beta) =
       \frac{x^{1-\alpha} (1-x)^{1-\beta}}{\mathrm{B}(\alpha,\beta)}

    at the points ``x``, given parameters ``alpha`` and ``beta``.

    :param array_like x: shape ``S``
        Points to evaluate the probability density at.

    :param array_like alpha: shape ``T`` or compatible
        :math:`\alpha` parameters to use in defining the distribution.  Will be
        broadcast such that ``alpha`` and ``beta`` have the same shape.

    :param array_like beta: shape ``T`` or compatible
        :math:`\beta` parameters to use in defining the distribution.  Will be
        broadcast such that ``alpha`` and ``beta`` have the same shape.

    :param out: (optional)
        Array to write the result to.  Must have shape ``T + S`` if provided.

    :type out: ndarray or None

    :param array_like where: (optional)
        Values of ``True`` indicate to calculate the probability density at that
        position, values of ``False`` indicate to leave the value in the output
        alone.  Must have shape compatible to ``T + S``.

    :return: array_like, shape ``T + S``
        Value of the probability density evaluated at each point.
    """
    lnpdf = beta_lnpdf(x, alpha, beta, out=out, where=where)
    where_arr = numpy.broadcast_to(numpy.asarray(where).T, lnpdf.T.shape).T
    return numpy.exp(lnpdf, out=out, where=where_arr)



def beta_lnpdf_noouter(x, alpha, beta, out=None, where=True):
    r"""
    Computes the natural log of the probability density of a beta distribution

    ..math::
       \ln\mathrm{Beta}(x;\alpha,\beta) =
       (1-\alpha)\ln(x) + (1-\beta)\ln(1-x) - \ln(\mathrm{B}(\alpha,\beta))

    at the points ``x``, given parameters ``alpha`` and ``beta``.

    :param array_like x: shape ``S``
        Points to evaluate the probability density at.

    :param array_like alpha: shape ``T`` or compatible
        :math:`\alpha` parameters to use in defining the distribution.  Will be
        broadcast such that ``alpha`` and ``beta`` have the same shape.

    :param array_like beta: shape ``T`` or compatible
        :math:`\beta` parameters to use in defining the distribution.  Will be
        broadcast such that ``alpha`` and ``beta`` have the same shape.

    :param out: (optional)
        Array to write the result to.  Must have shape ``T + S`` if provided.

    :type out: ndarray or None

    :param array_like where: (optional)
        Values of ``True`` indicate to calculate the probability density at that
        position, values of ``False`` indicate to leave the value in the output
        alone.  Must have shape compatible to ``T + S``.

    :return: array_like, shape ``T + S``
        Value of the probability density evaluated at each point.
    """
    import numpy
    import scipy.special

    x = numpy.asarray(x)
    alpha = numpy.asarray(alpha)
    beta = numpy.asarray(beta)
    where = numpy.asarray(where)

    x, alpha, beta = numpy.broadcast_arrays(x, alpha, beta)

    S = alpha.shape
    where = numpy.broadcast_to(where.T, reversed(S)).T

    out_type = x.dtype

    if out is None:
        out = numpy.empty(S, dtype=out_type)
    elif out.shape != S:
        raise ValueError(
            "Operands could not be broadcast together with shapes {} {}"
            .format(S, out.shape)
        )

    # Initialize temporary array with shape ``S``.
    tmp = numpy.empty(S, dtype=out_type)

    # Compute ``alpha-1`` and store in ``tmp``, and then compute
    # ``(alpha-1)*ln(x)`` and store in ``out``.  ``tmp`` is free to be
    # overwritten after this.
    alpha_minus_one = numpy.subtract(alpha, 1.0, out=tmp)
    alpha_minus_one_log_x = scipy.special.xlogy(
        alpha_minus_one, x,
        out=out, where=where,
    )

    # Compute ``beta-1`` and store in ``tmp``, and then compute
    # ``(beta-1)*ln(1-x)`` and store in ``tmp``.
    beta_minus_one = numpy.subtract(beta, 1.0, out=tmp)
    beta_minus_one_log_one_minus_x = scipy.special.xlog1py(
        beta_minus_one, -x,
        out=tmp, where=where,
    )

    # Compute ``(alpha-1)*ln(x) + (beta-1)*ln(1-x)`` and store in ``out``.
    numerator = numpy.add(
        alpha_minus_one_log_x, beta_minus_one_log_one_minus_x,
        out=out, where=where,
    )

    # Compute ``ln(B(alpha, beta))``, and store in ``tmp``.
    denominator = scipy.special.betaln(alpha, beta, out=tmp)

    # Compute the final result, storing it in ``out``.
    numpy.subtract(
        numerator, denominator,
        out=out, where=where,
    )

    # Return the final result.
    return out


def beta_pdf_noouter(x, alpha, beta, out=None, where=True):
    r"""
    Computes the probability density of a beta distribution

    ..math::
       \mathrm{Beta}(x;\alpha,\beta) =
       \frac{x^{1-\alpha} (1-x)^{1-\beta}}{\mathrm{B}(\alpha,\beta)}

    at the points ``x``, given parameters ``alpha`` and ``beta``.

    :param array_like x: shape ``S``
        Points to evaluate the probability density at.

    :param array_like alpha: shape ``T`` or compatible
        :math:`\alpha` parameters to use in defining the distribution.  Will be
        broadcast such that ``alpha`` and ``beta`` have the same shape.

    :param array_like beta: shape ``T`` or compatible
        :math:`\beta` parameters to use in defining the distribution.  Will be
        broadcast such that ``alpha`` and ``beta`` have the same shape.

    :param out: (optional)
        Array to write the result to.  Must have shape ``T + S`` if provided.

    :type out: ndarray or None

    :param array_like where: (optional)
        Values of ``True`` indicate to calculate the probability density at that
        position, values of ``False`` indicate to leave the value in the output
        alone.  Must have shape compatible to ``T + S``.

    :return: array_like, shape ``T + S``
        Value of the probability density evaluated at each point.
    """
    import numpy

    lnpdf = beta_lnpdf_noouter(x, alpha, beta, out=out, where=where)

    where = numpy.asarray(where)
    where = numpy.broadcast_to(where.T, reversed(lnpdf.shape)).T

    return numpy.exp(lnpdf, out=out, where=where)




def _beta_pdf_scipy(x, alpha, beta):
    import numpy
    import scipy.stats

    x = numpy.asarray(x)
    alpha, beta = numpy.broadcast_arrays(alpha, beta)

    TS = numpy.shape(alpha) + numpy.shape(x)

    out = numpy.empty(TS, dtype=x.dtype)

    for i, _ in numpy.ndenumerate(alpha):
        out[i] = scipy.stats.beta(alpha[i], beta[i]).pdf(x)

    return out


def _test_beta_pdf(x, alpha, beta, rtol=1e-7, atol=1e-3, verbose=True):
    import numpy
    import numpy.testing
    import scipy.stats

    our_result = beta_pdf(x, alpha, beta)

    scipy_result = _beta_pdf_scipy(x, alpha, beta)

    numpy.testing.assert_allclose(
        actual=our_result, desired=scipy_result,
        rtol=rtol, atol=atol,
        verbose=verbose,
    )


def truncnorm_pdf(x, mu, sigma, a, b, out=None, where=True):
    r"""
    Computes the probability density of a truncated normal distribution

    ..math::
       \mathcal{N}_T(x;\mu,\sigma,a,b) =
       \begin{cases}
         \mathcal{N}(x;\mu,\sigma) / \Delta, & \text{if } x \in [a, b], \\
         0, & \text{otherwise}
       \end{cases}

    where

    ..math::
       \Delta =
       \begin{cases}
         \Phi(-a) - \Phi(-b), & \text{if } a > 0 \\
         \Phi(b) - \Phi(a), & \text{otherwise}
       \end{cases}

    and where

    ..math::
       \Phi(x) = \frac{1}{\sqrt{2\pi}} \int_{-\infty}^x e^{-x^2 / 2} \mathrm{d}x

    at the points ``x``, given parameters ``mu``, ``sigma``, ``a``, and ``b``.
    ``mu`` and ``sigma`` correspond to the mean and standard deviation of the
    corresponding non-truncated normal distribution (by truncating the mean and
    variance change), and ``a`` and ``b`` correspond to the truncation bounds,
    which may be unsorted.

    :param array_like x: shape ``S``
        Points to evaluate the probability density at.

    :param array_like mu: shape ``T`` or compatible
        :math:`\mu` parameters to use in defining the distribution.  Will be
        broadcast such that ``mu``, ``sigma``, ``a``, and ``b`` have the same
        shape.

    :param array_like sigma: shape ``T`` or compatible
        :math:`\sigma` parameters to use in defining the distribution.  Will be
        broadcast such that ``mu``, ``sigma``, ``a``, and ``b`` have the same
        shape.

    :param array_like a: shape ``T`` or compatible
        :math:`a` parameters to use in defining the distribution.  Will be
        broadcast such that ``mu``, ``sigma``, ``a``, and ``b`` have the same
        shape.

    :param array_like b: shape ``T`` or compatible
        :math:`b` parameters to use in defining the distribution.  Will be
        broadcast such that ``mu``, ``sigma``, ``a``, and ``b`` have the same
        shape.

    :param out: (optional)
        Array to write the result to.  Must have shape ``T + S`` if provided.

    :type out: ndarray or None

    :param array_like where: (optional)
        Values of ``True`` indicate to calculate the probability density at that
        position, values of ``False`` indicate to leave the value in the output
        alone.  Must have shape compatible to ``T + S``.

    :return: array_like, shape ``T + S``
        Value of the probability density evaluated at each point.
    """
    import numpy
    import scipy.special


    mu, sigma, a, b = numpy.broadcast_arrays(mu, sigma, a, b)

    x = numpy.asarray(x)
    mu = numpy.asarray(mu)
    sigma = numpy.asarray(sigma)
    a = numpy.asarray(a)
    b = numpy.asarray(b)
    where = numpy.asarray(where)

    S = numpy.shape(x)
    T = numpy.shape(mu)
    TS = T + S

    out_type = x.dtype

    if out is None:
        out = numpy.empty(TS, dtype=out_type)
    elif out.shape != TS:
        raise ValueError(
            "Operands could not be broadcast together with shapes {} {} {}"
            .format(S, T, out.shape)
        )

    # Initialize temporary arrays with shape ``T`` for holding intermediate
    # results in an efficient manner.  We also keep a transposed view of ``out``
    # called ``out_T``, which is used when we need to perform broadcasting
    # operations across the ``T`` dimension instead of ``S``, as well as a
    # similarly transposed view of ``where``.
    tmp_T1 = numpy.empty(T, dtype=out_type)
    tmp_T2 = numpy.empty(T, dtype=out_type)
    tmp_T3 = numpy.empty(T, dtype=out_type)
    tmp_T4 = numpy.empty(T, dtype=out_type)
    out_T = out.T
    where_T = where.T

    tmp_i_T = numpy.empty(T, dtype=bool)


    # Ensure b > a.
    # Use the temporary arrays ``tmp_T3`` and ``tmp_T4`` to hold the values of
    # ``a`` and ``b``, but ensuring ``b >= a``.  Will use these temporary arrays
    # in place of the original ``a`` and ``b``, and will only free them at the
    # end of this function call.
    i_swap = numpy.greater(a, b, out=tmp_i_T)
    tmp_T3[i_swap] = b[i_swap]
    tmp_T4[i_swap] = a[i_swap]

    i_noswap = numpy.logical_not(i_swap, out=tmp_i_T)
    tmp_T3[i_noswap] = a[i_noswap]
    tmp_T4[i_noswap] = b[i_noswap]

    a, b = tmp_T3, tmp_T4

    # Compute Delta.
    # We compute the ``a`` terms in ``tmp_T1``, and the ``b`` terms in
    # ``tmp_T2``.  Finally we take their difference to get ``delta``, which we
    # keep in ``tmp_T1``.
    a_minus_mu = numpy.subtract(a, mu, out=tmp_T1)
    a_minus_mu_by_sigma = numpy.divide(a_minus_mu, sigma, out=tmp_T1)
    i_pos = a_minus_mu_by_sigma > 0
    numpy.negative(a_minus_mu_by_sigma, out=tmp_T1, where=i_pos)
    cdf_a = scipy.special.ndtr(a_minus_mu_by_sigma, out=tmp_T1)

    b_minus_mu = numpy.subtract(b, mu, out=tmp_T2)
    b_minus_mu_by_sigma = numpy.divide(b_minus_mu, sigma, out=tmp_T2)
    numpy.negative(b_minus_mu_by_sigma, out=tmp_T2, where=i_pos)
    cdf_b = scipy.special.ndtr(b_minus_mu_by_sigma, out=tmp_T2)

    plus_minus_delta = numpy.subtract(cdf_b, cdf_a, out=tmp_T1)
    delta = numpy.absolute(plus_minus_delta, out=tmp_T1)

    ## REMOVE
    # Compute sqrt(2*pi)*delta*sigma, which we will wtore in ``tmp_T1``, and
    # divide the whole result by at the end.
#    delta_times_sigma = numpy.multiply(delta, sigma, out=tmp_T1)
#    sqrt2pi_times_delta_times_sigma = numpy.multiply(
#        numpy.sqrt(2.0*numpy.pi), delta_times_sigma,
#        out=tmp_T1,
#    )
    ## /REMOVE

    # Now compute the exponential term.
    # We compute ``sigma**2`` and store it in ``tmp_T2`` until no longer needed,
    # at which point it gets marked for garbage collection.  We then compute the
    # rest of the exponential term, storing each step in ``out_T``, as all but
    # the ``x - mu`` operation needs to be done across the ``T`` dimension.  The
    # Final exponential term will be stored in ``out_T``.

    x_minus_mu = numpy.subtract.outer(x, mu, out=out_T, where=where_T)
    x_minus_mu_by_sigma = numpy.divide(
        x_minus_mu, sigma, out=out_T, where=where_T,
    )
    x_minus_mu_by_sigma_squared = numpy.square(
        x_minus_mu_by_sigma,
        out=out_T, where=where_T,
    )

    neg_half_x_minus_mu_by_sigma_squared = numpy.multiply(
        -0.5, x_minus_mu_by_sigma_squared,
        out=out_T, where=where_T,
    )

    exponential_term = numpy.exp(
        neg_half_x_minus_mu_by_sigma_squared,
        out=out_T, where=where_T,
    )

    # Compute the final PDF, pending zeroing out terms outside the domain,
    # and store it in ``out_T``.  No longer need ``tmp_T1`` after this.
    pdf = numpy.divide(
        exponential_term, numpy.sqrt(2.0*numpy.pi),
        out=out_T, where=where_T,
    )
    pdf = numpy.divide(
        pdf, delta,
        out=out_T, where=where_T,
    )
    pdf = numpy.divide(
        pdf, sigma,
        out=out_T, where=where_T,
    )

    # Zero out terms where ``x`` isn't between ``a`` and ``b``.
    i_bad = numpy.asarray(numpy.greater.outer(x, b, where=where_T))
    pdf[i_bad] = 0.0
    del tmp_T4, b

    i_bad = numpy.less.outer(x, a, out=i_bad, where=where_T)
    pdf[i_bad] = 0.0

    # Return the final PDF, in shape ``T + S``.
    return out


def _truncnorm_pdf_scipy(x, mu, sigma, a, b):
    import numpy
    import scipy.stats

    mu, sigma, a, b = numpy.broadcast_arrays(mu, sigma, a, b)

    x = numpy.asarray(x)
    mu = numpy.asarray(mu)
    sigma = numpy.asarray(sigma)
    a = numpy.asarray(a)
    b = numpy.asarray(b)

    TS = numpy.shape(mu) + numpy.shape(x)

    out = numpy.empty(TS, dtype=x.dtype)

    for i, _ in numpy.ndenumerate(mu):
        out[i] = scipy.stats.truncnorm(
            (a[i]-mu[i])/sigma[i], (b[i]-mu[i])/sigma[i],
            loc=mu[i], scale=sigma[i],
        ).pdf(x)

    return out

def _test_truncnorm_pdf(x, mu, sigma, a, b, rtol=1e-7, atol=1e-3, verbose=True):
    import numpy
    import numpy.testing
    import scipy.stats

    our_result = truncnorm_pdf(x, mu, sigma, a, b)
    scipy_result = _truncnorm_pdf_scipy(x, mu, sigma, a, b)

    numpy.testing.assert_allclose(
        actual=our_result, desired=scipy_result,
        rtol=rtol, atol=atol,
        verbose=verbose,
    )
