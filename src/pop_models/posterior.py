__all__ = [
    "log_posterior",
    "AuxInfoType", "AuxFnType",
    "PopulationInference",
    "PopulationInferenceSampler",
    "PopulationInferenceEmceeSampler",
]

import types
import typing
from pop_models.utils import debug_verbose

import numpy

from pop_models.detection import DetectionLikelihood
from pop_models.poisson_mean import PoissonMean
from pop_models.population import Population
from pop_models.types import Numeric, Parameters

from pop_models.credible_regions import (
    CredibleRegionsBasic, CredibleRegionsBasicResult,
)

AuxInfoType = typing.Dict[str, typing.Any]
AuxFnType = typing.Callable[...,AuxInfoType]

def log_posterior(
        parameters: Parameters,
        log_prior_fn: typing.Callable[...,numpy.ndarray],
        expval_fn: typing.Callable[...,numpy.ndarray],
        detection_likelihood_fns:
          typing.Sequence[typing.Callable[...,numpy.ndarray]],
        before_prior_aux_fn: typing.Optional[AuxFnType],
        after_prior_aux_fn: typing.Optional[AuxFnType],
        args: list, kwargs: dict,
    ):
    import numpy

    debug_verbose(
        "Computing ln(post)+C for parameters:", parameters,
        mode="log_post", flush=True,
    )

    # Compute auxiliary information, if requested.
    if before_prior_aux_fn is not None:
        debug_verbose(
            "Computing aux info before the prior",
            mode="log_post", flush=True,
        )
        aux_info = before_prior_aux_fn(parameters, *args, **kwargs)
    else:
        aux_info = {}

    # Compute the log of the prior.
    # We will also start incrementally computing the log of the posterior with
    # this term, so we initialize `log_post` to a copy of `log_prior`.
    debug_verbose(
        "Computing ln(prior)+C",
        mode="log_post", flush=True,
    )
    log_prior = log_prior_fn(parameters, *args, **kwargs, **aux_info)
    debug_verbose(
        "ln(prior)+C is:", log_prior,
        mode="log_post", flush=True,
    )
    log_post = log_prior.copy()

    prior_support = numpy.isfinite(log_prior)
    debug_verbose(
        "Prior support at:", prior_support,
        mode="log_post", flush=True,
    )

    if after_prior_aux_fn is not None:
        debug_verbose(
            "Computing aux info after the prior",
            mode="log_post", flush=True,
        )
        aux_info = after_prior_aux_fn(
            parameters, prior_support, aux_info, *args, **kwargs, **aux_info,
        )

    # Add the contribution from each detection to the posterior.
    for det_like_fn in detection_likelihood_fns:
        debug_verbose(
            "Computing contribution from event:", det_like_fn,
            mode="log_post", flush=True,
        )
        # Compute the log of the contribution to the likelihood from this
        # detection, and add the result in-place.
        det_log_contribution = numpy.log(
            det_like_fn(parameters, where=prior_support, **kwargs, **aux_info),
            where=prior_support,
        )
        debug_verbose(
            "Event contribution is:", det_log_contribution,
            mode="log_post", flush=True,
        )
        numpy.add(
            log_post, det_log_contribution,
            where=prior_support, out=log_post,
        )
        debug_verbose(
            "ln(post)+C currently adds up to:", log_post,
            mode="log_post", flush=True,
        )
        del det_log_contribution

    # Compute the contribution from the Poisson average, -mu, and add it to the
    # result in-place.
    debug_verbose(
        "Computing contribution from Poisson mean",
        mode="log_post", flush=True,
    )
    expval = expval_fn(
        parameters,
        where=prior_support, **kwargs, **aux_info,
    )
    debug_verbose(
        "Poisson mean is:", expval,
        mode="log_post", flush=True,
    )
    numpy.subtract(
        log_post, expval,
        where=prior_support, out=log_post,
    )

    debug_verbose(
        "Final ln(post)+C is:", log_post,
        mode="log_post", flush=True,
    )

    # Return log(post) + const = log(prior) + log(event contribution) - mu
    return log_post, log_prior


def get_params(
        variables: numpy.ndarray,
        constants: typing.Dict[str,float],
        duplicates: typing.Dict[str,str],
        names: typing.List[str],
    ) -> Parameters:
    import numpy
    import six

    full_shape = variables.shape

    shape = full_shape[:-1]
    n_variables = full_shape[-1]

    n_constants = len(constants)
    n_duplicates = len(duplicates)
    n_params = len(names)

    if n_variables + n_constants + n_duplicates != n_params:
        raise ValueError(
            "Incorrect number of variables and constants. "
            "Expected {expected}, but got {actual}."
            .format(
                expected=n_params,
                actual=n_variables+n_constants+n_duplicates,
            )
        )

    params = {}

    i = 0

    for name in names:
        # Use the constant value.
        if name in constants:
            param = numpy.broadcast_to(constants[name], shape)
        # Leave this for now, will fill duplicates at the end.
        elif name in duplicates:
            continue
        # Use the variable value.
        else:
            param = variables[...,i]
            i += 1

        # Store the chosen parameter.
        params[name] = param

    # Copy the duplicates into their respective indices.
    for target, source in six.iteritems(duplicates):
        params[target] = numpy.broadcast_to(params[source], shape)

    return params


class Posterior(object):
    r"""
    Abstract representation of the posterior distribution for a population
    inference problem.  Implementing sub-classes provide their own approach for
    storage (e.g., grid-based, random sampling-based), as well as a method for
    producing corner plots, :py:meth:`corner`.
    """
    def __init__(
            self,
            param_names: typing.List[str],
            metadata: typing.Optional[typing.Dict[str,typing.Any]]=None,
            constants: typing.Optional[typing.Dict[str,float]]=None,
            duplicates: typing.Optional[typing.Dict[str,str]]=None,
            dtype=numpy.float64,
        ) -> None:
        self.dtype = dtype

        self.param_names = param_names

        self.metadata = (
            metadata if metadata is not None else {}
        )
        self.constants = (
            constants if constants is not None else {}
        ) # type: typing.Dict[str,float]
        self.duplicates = (
            duplicates if duplicates is not None else {}
        ) # type: typing.Dict[str,str]

        self.n_dim = (
            len(self.param_names) - len(self.constants) - len(self.duplicates)
        )

        self.variable_names = [
            name
            for name in self.param_names
            if (name not in self.constants) and (name not in self.duplicates)
        ] # type: typing.List[str]

    def plot_corner(
            self,
            scale: float=3.0,
            color="black",
            levels: typing.Union[int,typing.Sequence[float]]=10,
            truths: typing.Optional[typing.Dict[str,float]]=None,
            truth_color="C3",
            rescale_parameters: typing.Optional[typing.Mapping[str, str]]=None,
        ):
        r"""
        Produce a 'corner plot' of the posteriors, as in Dan Foreman-Mackey's
        ``corner.py``.
        """
        raise NotImplementedError()


class PosteriorGrid(Posterior):
    r"""
    Concrete representation of a population inference posterior distribution on
    a rectangular grid of parameters.  Provides utilities for normalizing the
    distribution on the grid, and finding arbitrary marginal distributions.
    """
    def __init__(
            self,
            variable_grids: typing.Dict[str,numpy.ndarray],
            param_names: typing.List[str],
            metadata: typing.Optional[typing.Dict[str,typing.Any]]=None,
            constants: typing.Optional[typing.Dict[str,float]]=None,
            duplicates: typing.Optional[typing.Dict[str,str]]=None,
            variable_dxs: typing.Optional[typing.Dict[str,float]]=None,
            dtype=numpy.float64,
        ):
        super().__init__(
            param_names,
            metadata=metadata, constants=constants, duplicates=duplicates,
            dtype=dtype
        )

        # Ensure ``variable_grids`` provides one grid for each variable.
        # Also do the same for ``variable_dxs`` if provided.
        if set(self.variable_names) != set(variable_grids.keys()):
            raise KeyError(
                "Unexpected variable names in 'variable_grids'. "
                "Expected {} but got {}."
                .format(set(self.variable_names), set(variable_grids.keys()))
            )
        if variable_dxs is not None:
            if set(self.variable_names) != set(variable_dxs.keys()):
                raise KeyError(
                    "Unexpected variable names in 'variable_dxs'. "
                    "Expected {} but got {}."
                    .format(
                        set(self.variable_names), set(variable_dxs.keys())
                    )
                )

        # Ensure arrays in ``variable_grids`` are 1D
        for grid in variable_grids.values():
            if grid.ndim != 1:
                raise ValueError(
                    "'variable_grids' must all be 1D arrays."
                )

        # Store grids and dx's.
        self.variable_grids = variable_grids
        self.variable_dxs = variable_dxs

        # Store shape of full grid.
        self.grid_shape = tuple(
            variable_grids[name].size for name in self.variable_names
        )

        # Initially grid won't be stored.
        self.grid_stored = False

    def store_log_prob_grids(
            self,
            posterior_log_prob_grid: numpy.ndarray,
            prior_log_prob_grid: numpy.ndarray,
        ) -> None:
        r"""
        One time use function for storing the posterior and prior on a grid.
        Will handle normalization internally.
        """
        if self.grid_stored:
            raise RuntimeError(
                "Log probability grids have already been stored."
            )

        # Ensure grids have the expected shape.
        if posterior_log_prob_grid.shape != self.grid_shape:
            raise ValueError(
                "Posterior grid's shape {} does not equal the expected {}."
                .format(posterior_log_prob_grid.shape, self.grid_shape)
            )
        if prior_log_prob_grid.shape != self.grid_shape:
            raise ValueError(
                "Prior grid's shape {} does not equal the expected {}."
                .format(prior_log_prob_grid.shape, self.grid_shape)
            )

        # Normalize the grids and convert to standard probabilities.
        self.posterior_prob_grid = self.normalized_prob_grid(
            posterior_log_prob_grid
        )
        self.prior_prob_grid = self.normalized_prob_grid(
            prior_log_prob_grid
        )

        # Make the grids read only
        self.posterior_prob_grid.setflags(write=False)
        self.prior_prob_grid.setflags(write=False)

        # Grids have now been stored.
        self.grid_stored = True

    def normalized_prob_grid(
            self, log_prob_grid: numpy.ndarray,
        ) -> numpy.ndarray:
        prob_grid = numpy.exp(log_prob_grid - numpy.max(log_prob_grid))

        marg_grid = prob_grid

        if self.variable_dxs is not None:
            for name in reversed(self.variable_names):
                dx = self.variable_dxs[name]
                marg_grid = numpy.trapz(marg_grid, dx=dx, axis=-1)
        else:
            for name in reversed(self.variable_names):
                x = self.variable_grids[name]
                marg_grid = numpy.trapz(marg_grid, x=x, axis=-1)

        # Final value of marginalized grid is just the overall normalization.
        norm = marg_grid

        # Divide out the normalization so `prob_grid` is normalized to unity.
        prob_grid /= norm

        return prob_grid

    def get_params(self):
        variable_meshgrids = numpy.meshgrid(
            *[self.variable_grids[name] for name in self.variable_names],
            indexing="ij",
        )

        shape = variable_meshgrids[0].shape

        parameters = {
            name : meshgrid
            for name, meshgrid in zip(self.variable_names, variable_meshgrids)
        }

        for name, const in self.constants.items():
            parameters[name] = numpy.broadcast_to(const, shape)

        for target, source in self.duplicates.items():
            parameters[target] = parameters[source]

        return parameters

    def posterior_prob(self):
        # Return a view of the posterior grid.
        return self.posterior_prob_grid[()]

    def prior_prob(self):
        # Return a view of the prior grid.
        return self.prior_prob_grid[()]

    def marginal_posterior_prob(self, variable_names: typing.List[str]):
        return self.marginal_prob(self.posterior_prob_grid, variable_names)

    def marginal_prior_prob(self, variable_names: typing.List[str]):
        return self.marginal_prob(self.prior_prob_grid, variable_names)

    def marginal_prob(
            self,
            prob_grid: numpy.ndarray,
            variable_names: typing.List[str],
        ):
        # Ensure variable names are valid.
        if not set(variable_names).issubset(self.variable_names):
            raise KeyError(
                "Variables {} not part of this grid."
                .format(set(self.variable_names) - set(variable_names))
            )

        # Marginalize over un-desired variables.
        axis = -1
        unsorted_variable_names = [] # type: typing.List[str]
        for name in reversed(self.variable_names):
            # Skip marginalizing for variables we want.
            if name in variable_names:
                # Make sure next marginalization knows to skip this spot.
                axis -= 1

                # Do bookkeeping on order of variables, as it may not match the
                # order in ``variable_names``.
                unsorted_variable_names.insert(0, name)

                continue

            # Marginalize using dx if possible
            if self.variable_dxs is not None:
                dx = self.variable_dxs[name]
                prob_grid = numpy.trapz(prob_grid, dx=dx, axis=axis)
            # Marginalize using x if we must
            else:
                x = self.variable_grids[name]
                prob_grid = numpy.trapz(prob_grid, x=x, axis=axis)

        # If ``prob_grid`` is sorted such that its axes are ordered the same
        # way as ``variable_names``, return.
        if variable_names == unsorted_variable_names:
            return prob_grid

        # Re-order axes of output prob_grid to match ``variable_names``.
        # ``i`` denotes the index a variable should have, and ``j`` denotes its
        # current index in the array we're sorting.
        for i, name in enumerate(variable_names):
            # Determine where variable ``name`` is currently located.
            j = unsorted_variable_names.index(name)
            # Place variable in correct position.
            prob_grid = numpy.swapaxes(prob_grid, i, j)
            # Update record of where variables are located.
            (
                unsorted_variable_names[i], unsorted_variable_names[j],
            ) = (
                unsorted_variable_names[j], unsorted_variable_names[i],
            )

        # Grid is now sorted properly, return.
        return prob_grid

    def plot_corner(
            self,
            scale: float=3.0,
            color="black",
            levels: typing.Union[int,typing.Sequence[float]]=10,
            truths: typing.Optional[typing.Dict[str,float]]=None,
            truth_color="C3",
            rescale_parameters: typing.Optional[typing.Mapping[str, str]]=None,
        ):
        import matplotlib.pyplot as plt

        if not isinstance(levels, int):
            raise NotImplementedError(
                "Currently only support 'levels' that are integers."
            )

        n_dim = len(self.variable_names)
        size = scale*n_dim

        fig, axes = plt.subplots(n_dim, n_dim, figsize=(size,size))

        for row in range(n_dim):
            ax = axes[row,row]

            # Plot diagonals
            row_name = self.variable_names[row]

            row_grid = self.variable_grids[row_name]
            row_marginal = self.marginal_posterior_prob([row_name])

            ax.plot(row_grid, row_marginal, color=color)

            # Overlay truth if known (for synthetic data runs)
            if truths is not None:
                ax.axvline(
                    truths[row_name],
                    linestyle="dashed", color=truth_color,
                )

            row_lim = [row_grid[0], row_grid[-1]]
            ax.set_xlim(row_lim)

            # Hide un-necessary axis ticks.
            ax.set_yticklabels([])
            if row < n_dim-1:
                ax.set_xticklabels([])

            for col in range(n_dim):
                ax = axes[row,col]

                # Diagonal is already handled.
                if row == col:
                    continue

                # Upper triangle should be blank.
                if col > row:
                    ax.set_axis_off()
                    continue

                # Hide un-necessary axis ticks.
                if col > 0:
                    ax.set_yticklabels([])
                if row < n_dim-1:
                    ax.set_xticklabels([])

                # Plot off-diagonals
                col_name = self.variable_names[col]

                col_grid = self.variable_grids[col_name]
                joint_marginal = self.marginal_posterior_prob(
                    [row_name, col_name]
                )

                ax.contourf(
                    col_grid, row_grid, joint_marginal,
                    levels,
                    cmap="viridis",
                )

                if truths is not None:
                    ax.axvline(
                        truths[col_name],
                        linestyle="dashed", color=truth_color,
                    )
                    ax.axhline(
                        truths[row_name],
                        linestyle="dashed", color=truth_color,
                    )

                col_lim = [col_grid[0], col_grid[-1]]
                ax.set_xlim(col_lim)
                ax.set_ylim(row_lim)

        # Add axis labels.
        for i, name in enumerate(self.variable_names):
            axes[i,0].set_ylabel(name)
            axes[n_dim-1,i].set_xlabel(name)

        return fig


def _rolling_average(x, n=1, axis=0):
    """
    Compute a 
    """
    if not isinstance(n, int):
        raise TypeError("n must be of type 'int', not {}".format(type(n)))

    if n < 1:
        raise ValueError("n must be at least 1, got {}".format(n))

    if not isinstance(axis, int):
        raise TypeError("axis must be of type 'int', not {}".format(type(axis)))

    # Length of the axis we're averaging over.
    L = x.shape[axis]

    if n > L:
        raise ValueError(
            "n was '{}', but expected value less than axis length '{}'"
            .format(n, L)
        )

    # Cumulative sum along the specified axis.
    s = numpy.cumsum(x, dtype=numpy.float64, axis=axis)

    # Padding for array indexing.
    idx_pad = tuple(slice(None) for _ in range(axis))

    # Cumulative sum but with a single zero padding the start of the
    # axis we're averaging over, and the last `n` elements removed.
    padding = (
        [(0, 0) for _ in range(axis)] +
        [(1, 0)] +
        [(0, 0) for _ in range(x.ndim - axis - 1)]
    )
    s0 = numpy.pad(s[idx_pad + (slice(L-n),)], padding)

    # Return the rolling average by taking the difference between
    # cumulative sums at the start and end of the range, and dividing
    # by the number of elements.
    return (s[idx_pad + (slice(n-1, None),)] - s0) / n

class PosteriorSamples(Posterior):
    r"""
    Abstract representation of a population inference posterior distribution
    measured by drawing posterior samples, typically by an MCMC or other similar
    method.

    On top of the corner plots which all :py:class:`Posterior` objects provide,
    this also provides a :py:meth:`plot_chains` method for plotting the samples.
    """
    def __init__(
            self,
            param_names: typing.List[str],
            metadata: typing.Optional[typing.Dict[str,typing.Any]]=None,
            constants: typing.Optional[typing.Dict[str,float]]=None,
            duplicates: typing.Optional[typing.Dict[str,str]]=None,
            state: typing.Optional[numpy.ndarray]=None,
            dtype=numpy.float64,
        ) -> None:
        super().__init__(
            param_names,
            metadata=metadata, constants=constants, duplicates=duplicates,
            dtype=dtype
        )

        self.state = state

        if self.state is None:
            self.n_walkers = None
        else:
            n_walkers, n_dim = self.state.shape

            assert n_dim == self.n_dim, \
                "State array's shape incompatible with number of parameters."

            self.n_walkers = n_walkers

    def __len__(self) -> int:
        raise NotImplementedError()

    def get_samples(self, index) -> numpy.ndarray:
        raise NotImplementedError()

    def get_posterior_log_prob(self, index) -> numpy.ndarray:
        raise NotImplementedError()

    def get_prior_log_prob(self, index) -> numpy.ndarray:
        raise NotImplementedError()

    def get_params(self, index) -> typing.Mapping[str, numpy.ndarray]:
        variables = self.get_samples(index)

        shape = variables.shape[:-1]

        parameters = {
            name : variables[...,i]
            for i, name in enumerate(self.variable_names)
        }

        for name, const in self.constants.items():
            parameters[name] = numpy.broadcast_to(const, shape)

        for target, source in self.duplicates.items():
            parameters[target] = parameters[source]

        return parameters

    def get_weights(self, index) -> numpy.ndarray:
        raise NotImplementedError()

    @property
    def is_weighted(self) -> bool:
        return False

    def append(
            self,
            sample: numpy.ndarray,
            posterior_log_prob: numpy.ndarray, prior_log_prob: numpy.ndarray,
        ) -> None:
        raise NotImplementedError()

    def plot_chains(
            self,
            fig=None,
            skip_samples: int=0,
            rolling_average: typing.Optional[int]=None,
            color="black", alpha: float=0.1,
            color_code_log_post: typing.Optional[str]=None,
            log_post_color="C0", log_prior_color="C1",
            **plot_kwargs
        ):
        import matplotlib as mpl
        import matplotlib.pyplot as plt

        n_rows = self.n_dim + 1

        # Create figure if not provided, otherwise extract axes.
        if fig is None:
            fig, axes = plt.subplots(
                nrows=n_rows, sharex=True,
                figsize=[4, 3*n_rows],
                constrained_layout=True,
            )
        else:
            axes = fig.axes

        from matplotlib.lines import Line2D

        # First axis is special, as it plots the log post and prior.
        # Remaining axes are for the parameter samples.
        ax_log_prob = axes[0]
        axes_params = axes[1:]

        n_samples = len(self)
        index = slice(-n_samples, None)

        # Extract the log post and prior.
        log_post = self.get_posterior_log_prob(index)
        log_prior = self.get_prior_log_prob(index)

        # If we are skipping samples in the plot, overwrite them with NaN's
        log_post[:skip_samples] = numpy.nan
        log_prior[:skip_samples] = numpy.nan

        # Shift log post and prior such that they peak at zero.
        log_post -= numpy.nanmax(log_post)
        log_prior -= numpy.nanmax(log_prior)

        # Create array of iteration indices
        iteration_indices = numpy.arange(len(log_post), dtype=float)

        # Set color scale if color-coding.
        if color_code_log_post is not None:
            log_post_masked = numpy.ma.masked_invalid(log_post)
            norm_color_code = plt.Normalize(
                log_post_masked.min(),
                log_post_masked.max(),
            )

        # Plot the log post and prior
        ax_log_prob.plot(log_prior, color=log_prior_color, alpha=alpha)
        if color_code_log_post is None:
            # Not color coding, so do a single color plot.
            ax_log_prob.plot(log_post, color=log_post_color, alpha=alpha)
        else:
            # Iterate over each walker.
            for log_post_i in log_post.T:
                print(list(zip(iteration_indices, log_post_i)))
                # Plot color coded line segements.
                lc = mpl.collections.LineCollection(
                    list(zip(iteration_indices, log_post_i)),
                    cmap=color_code_log_post,
                    norm=norm_color_code,
                )
                lc.set_array(log_post_i)
                ax_log_prob.add_collection(lc)
        # Plot the rolling average if requested.
        if rolling_average is not None:
            log_prior_avg = numpy.nanmean(
                _rolling_average(log_prior[skip_samples:], n=rolling_average),
                axis=1,
            )
            log_post_avg = numpy.nanmean(
                _rolling_average(log_post[skip_samples:], n=rolling_average),
                axis=1,
            )
            ax_log_prob.plot(
                iteration_indices[skip_samples+rolling_average-1:],
                log_prior_avg,
                color="white", linewidth=5,
            )
            ax_log_prob.plot(
                iteration_indices[skip_samples+rolling_average-1:],
                log_prior_avg,
                color=log_prior_color, linewidth=4,
            )
            ax_log_prob.plot(
                iteration_indices[skip_samples+rolling_average-1:],
                log_post_avg,
                color="white", linewidth=5,
            )
            ax_log_prob.plot(
                iteration_indices[skip_samples+rolling_average-1:],
                log_post_avg,
                color=log_post_color, linewidth=4,
            )

        params = self.get_params(index)

        for ax, var_name in zip(axes_params, self.variable_names):
            param_samples = params[var_name]
            # If we are skipping samples in the plot, overwrite them with NaN's
            param_samples[:skip_samples] = numpy.nan

            if color_code_log_post is None:
                ax.plot(param_samples, color=color, alpha=alpha)
            else:
                raise NotImplementedError

            # Plot the rolling average if requested.
            if rolling_average is not None:
                param_samples_avg = numpy.nanmean(
                    _rolling_average(
                        param_samples[skip_samples:], n=rolling_average,
                    ),
                    axis=1,
                )
                ax.plot(
                    iteration_indices[skip_samples+rolling_average-1:],
                    param_samples_avg,
                    color="white", linewidth=5,
                )
                ax.plot(
                    iteration_indices[skip_samples+rolling_average-1:],
                    param_samples_avg,
                    color=color, linewidth=4,
                )

            ax.set_ylabel(var_name)

        axes[-1].set_xlabel("sample #")

        legend_lines = [
            Line2D([0], [0], color="C0", lw=2),
            Line2D([0], [0], color="C1", lw=2),
        ]
        legend_labels = [
            "posterior",
            "prior",
        ]

        ax_log_prob.legend(legend_lines, legend_labels, loc="best")

        ax_log_prob.set_ylabel(r"$\log(\mathrm{prob}) + \mathrm{const}$")

        return fig, axes


    def plot_corner(
            self,
            scale: float=3.0,
            color="black",
            levels: typing.Union[int, typing.Sequence[float]]=(0.50, 0.90),
            truths: typing.Optional[typing.Dict[str,float]]=None,
            truth_color="C3",
            rescale_parameters: typing.Optional[typing.Mapping[str, str]]=None,
        ):
        import corner

        params = self.get_params(...)
        # Rescale params if needed, and update labels accordingly.
        if rescale_parameters is not None:
            # Work on a copy of ``truths`` if it was provided.
            if truths is not None:
                truths = truths.copy()

            for name, scaling in rescale_parameters.items():
                # Skip unknown parameters.
                if name not in params:
                    continue

                if scaling == "log":
                    params[name] = numpy.log10(params[name])
                    if truths is not None:
                        truths[name] = numpy.log10(truths[name])
                elif scaling == "linear":
                    pass
                else:
                    raise KeyError("Unknown scaling: {}".format(scaling))

            labels = []
            for name in self.variable_names:
                if name in rescale_parameters:
                    scaling = rescale_parameters[name]
                    if scaling == "log":
                        name = r"$\log_{{10}}$ {}".format(name)
                labels.append(name)
        else:
            labels = self.variable_names

        # If ``truths`` was provided, convert it from a dict to a list
        truths_list = None
        if truths is not None:
            truths_list = [truths[name] for name in self.variable_names]

        # Convert dict of parameter values to an array in the form needed by
        # corner.
        xs = numpy.column_stack(
            tuple(
                typing.cast(numpy.ndarray, params[name]).flatten()
                for name in self.variable_names
            )
        )

        return corner.corner(
            xs,
            labels=labels,
            color=color,
            truths=truths_list,
            truth_color=truth_color,
            levels=typing.cast(typing.Sequence[float], levels),
            plot_datapoints=False,
            plot_density=False,
            plot_contours=True,
            no_fill_contours=True,
        )

    def credible_regions(
            self,
            include_median: bool=False,
            quantiles: typing.Optional[typing.Sequence[float]]=None,
            equal_prob_bounds: typing.Optional[typing.Sequence[float]]=None,
            interpolation: str="linear",
        ) -> typing.Dict[str,CredibleRegionsBasicResult]:
        cr = CredibleRegionsBasic(
            include_median=include_median,
            quantiles=quantiles, equal_prob_bounds=equal_prob_bounds,
            interpolation=interpolation,
        )

        params = self.get_params(...)

        return {
            param_name : cr.compute(param_samples)
            for param_name, param_samples in params.items()
        }


class NumpyPosteriorSamples(PosteriorSamples):
    def __init__(
            self,
            n_samples: int,
            param_names: typing.List[str],
            constants: typing.Optional[typing.Dict[str,float]]=None,
            duplicates: typing.Optional[typing.Dict[str,str]]=None,
            state: typing.Optional[numpy.ndarray]=None,
            dtype=numpy.float64,
        ):
        super().__init__(
            param_names,
            constants=constants, duplicates=duplicates,
            state=state,
            dtype=dtype,
        )

        self.n_samples = n_samples

        if self.n_walkers is None:
            probs_shape = [self.n_samples]
        else:
            assert isinstance(self.n_walkers, int)
            probs_shape = [self.n_samples, self.n_walkers]

        samples_shape = probs_shape + [self.n_dim]

        self.samples = numpy.empty(samples_shape, dtype=self.dtype)
        self.posterior_log_probs = numpy.empty(probs_shape, dtype=self.dtype)
        self.prior_log_probs = numpy.empty(probs_shape, dtype=self.dtype)

        self.index = 0

    def __len__(self) -> int:
        return self.n_samples

    def get_samples(self, index) -> numpy.ndarray:
        return self.samples[index]

    def get_posterior_log_prob(self, index) -> numpy.ndarray:
        return self.posterior_log_probs[index]

    def get_prior_log_prob(self, index) -> numpy.ndarray:
        return self.prior_log_probs[index]


    def append(
            self,
            sample: numpy.ndarray,
            posterior_log_prob: numpy.ndarray, prior_log_prob: numpy.ndarray,
        ) -> None:
        if self.index < self.n_samples:
            self.samples[self.index] = sample
            self.posterior_log_probs[self.index] = posterior_log_prob
            self.prior_log_probs[self.index] = prior_log_prob

            self.index += 1
        else:
            raise IndexError("Tried to append to filled array")



class H5RawPosteriorSamples(PosteriorSamples):
    r"""
    Class for storing posterior samples directly from an ensemble of MCMC
    chains, in an extensible HDF5 format.  In the specified directory, it
    produces a file named ``master.hdf5``, containing links to sub-files named
    ``subfile-000.hdf5``, ``subfile-001.hdf5``, etc.  Starts with a single
    subfile, and adds more as they exhaust their space.

    Only use the constructor method after the file has been initialized.  To
    initialize, instead call the static method :py:meth:`create`.
    """
    def __init__(
            self,
            directory: str, mode: str,
            swmr_mode: bool=False,
        ):
        import h5py

        if mode not in {"r", "r+"}:
            raise ValueError(
                "Can only load H5RawPosteriorSamples file in readonly ('r') "
                "and read/write ('r+') modes.  Provided: {mode}"
                .format(mode=mode)
            )

        filename = H5RawPosteriorSamples._get_master_filename(directory)
        master_file = h5py.File(
            filename, mode,
            libver="latest", swmr=swmr_mode,
        )

        # Allow the file to be read by others while it's being written to.
        if (mode == "r+") and swmr_mode:
            master_file.swmr_mode = True

        param_names = [
            name.decode("utf-8")
            for name in master_file["param_names"][()]
        ]
        chunk_size = master_file.attrs["chunk_size"]
        zero_padding_length = master_file.attrs["zero_padding_length"]

        self.directory = directory
        self.master_file = master_file
        self.chunk_size = chunk_size
        self.zero_padding_length = zero_padding_length

        # Pull out the initial state, just to conform to the super-class
        # API.  We will later overwrite it with the latest state.
        first_chain = H5RawPosteriorSamples._get_chain_link(
            master_file["chain_links"], 0, zero_padding_length,
        )
        init_state = first_chain["samples"][0]
        dtype = init_state.dtype

        metadata = (
            master_file["metadata"].attrs if "metadata" in master_file
            else None
        )
        constants = dict(master_file["constants"].attrs)
        duplicates = dict(master_file["duplicates"].attrs)

        super().__init__(
            param_names,
            metadata=metadata,
            constants=constants, duplicates=duplicates,
            state=init_state,
            dtype=dtype,
        )

        # Overwrite the state, which should be set to the initial state by
        # the call to super().__init__
        self.state = self.get_samples(-1)

    @staticmethod
    def _get_master_filename(directory):
        import os.path
        return os.path.join(directory, "master.hdf5")

    @classmethod
    def create(
            cls,
            directory: str,
            param_names: typing.List[str],
            init_state: numpy.ndarray,
            constants: typing.Optional[typing.Dict[str,float]]=None,
            duplicates: typing.Optional[typing.Dict[str,str]]=None,
            dtype=numpy.float64,
            chunk_size: int=1024,
            zero_padding_length: int=3,
            swmr_mode: bool=False,
            force: bool=False,
        ) -> "H5RawPosteriorSamples":
        import h5py
        import os
        import glob
        import errno

        # Ensure output directory exists.
        try:
            os.makedirs(directory)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

        # Elevate `constants` and `duplicates` to dictionaries if they're `None`
        constants = constants if constants is not None else {}
        duplicates = duplicates if duplicates is not None else {}

        # Determine dimensionality properties.
        n_walkers, n_dim = init_state.shape

        # Create output file.
        mode = "w" if force else "w-"
        filename = cls._get_master_filename(directory)
        master_file = h5py.File(filename, mode, swmr=swmr_mode, libver="latest")
        with master_file:
            # Delete any existing subfiles in the directory.
            for f in glob.glob(os.path.join(directory, "subfile-*.hdf5")):
                os.remove(f)

            # Enable SWMR mode if requested
            if swmr_mode:
                master_file.swmr_mode = swmr_mode

            master_file.attrs["chunk_size"] = chunk_size
            master_file.attrs["zero_padding_length"] = zero_padding_length

            master_file.attrs["n_samples"] = 1

            master_file.create_dataset(
                "param_names",
                data=numpy.array(param_names, dtype="S"),
            )

            metadata_group = master_file.create_group("metadata")
            constants_group = master_file.create_group("constants")
            duplicates_group = master_file.create_group("duplicates")

            for name, const in constants.items():
                constants_group.attrs[name] = const

            for target, source in duplicates.items():
                duplicates_group.attrs[target] = source

            # Create group for ExternalLink's to sub-files
            chain_links = master_file.create_group("chain_links")
            chain_links.attrs["latest"] = 0

            # Initialize links to sub-files even though they don't exist yet
            # and some may never exist.
            for i in range(10**zero_padding_length):
                num_str = cls._get_chain_num_str(
                    i, zero_padding_length,
                )
                relname = "subfile-{}.hdf5".format(num_str)
                chain_links[num_str] = h5py.ExternalLink(relname, "/")

            # Create the first sub-file.
            cls._create_subfile(
                directory,
                typing.cast(int, n_walkers), typing.cast(int, n_dim),
                master_file, 0,
                chunk_size, zero_padding_length,
                dtype,
            )
            # Store the initial state as the first sample in the sub-file, and
            # manually increment the sample size.  Set the log-probabilities to
            # NaN since this object has no way of evaluating the probabilities,
            # and it is not likely to be used.
            sub_file = cls._get_chain_link(
                chain_links, 0, zero_padding_length,
            )
            sub_file["samples"][0] = init_state
            sub_file["posterior_log_probs"][0] = numpy.nan
            sub_file["prior_log_probs"][0] = numpy.nan
            sub_file.attrs["next"] += 1

            if swmr_mode:
                sub_file["samples"].flush()
                sub_file["posterior_log_probs"].flush()
                sub_file["prior_log_probs"].flush()

        return cls(directory, "r+")

    @classmethod
    def create_from_raw_samples(
            cls,
            directory: str,
            raw_samples_init: "H5RawPosteriorSamples",
            constants: typing.Optional[typing.Dict[str,float]]=None,
            dtype=numpy.float64,
            chunk_size: int=1024,
            zero_padding_length: int=3,
            swmr_mode: bool=False,
            force: bool=False,
        ) -> "H5RawPosteriorSamples":
        # Pull constants from initializing samples object if not provided.
        # If provided explicitly, must have the same keys as initializing
        # object, though values may be different.
        if constants is None:
            constants = raw_samples_init.constants
        else:
            # Validate.
            if constants.keys() != raw_samples_init.constants.keys():
                raise ValueError(
                    "If constants are overwridden, they must be for the same "
                    "set of parameters, though they may use different values."
                )
        # Pull duplicates from initializing samples object.  There is no option
        # to override currently.
        duplicates = raw_samples_init.duplicates

        # Pull parameter names from initializing samples object.
        param_names = raw_samples_init.param_names

        # Take the last set of samples from the initializing object as our
        # initial state.
        init_state = raw_samples_init.get_samples(-1)

        # Create a new H5RawPosteriorSamples object, initialized to the final
        # state of ``raw_samples_init``.
        return cls.create(
            directory,
            param_names,
            init_state,
            constants=constants,
            duplicates=duplicates,
            dtype=dtype,
            chunk_size=chunk_size,
            zero_padding_length=zero_padding_length,
            swmr_mode=swmr_mode,
            force=force,
        )

    @classmethod
    def create_from_samples(
            cls,
            directory: str,
            n_walkers: int,
            samples_init: PosteriorSamples,
            constants: typing.Optional[typing.Dict[str,float]]=None,
            dtype=numpy.float64,
            chunk_size: int=1024,
            zero_padding_length: int=3,
            swmr_mode: bool=False,
            force: bool=False,
            random_state: typing.Optional[numpy.random.RandomState]=None,
            pop_inf: typing.Optional["PopulationInference"]=None,
        ) -> "H5RawPosteriorSamples":
        # Create RNG object if not provided.
        if random_state is None:
            random_state = numpy.random.RandomState()

        # Pull constants from initializing samples object if not provided.
        # If provided explicitly, must have the same keys as initializing
        # object, though values may be different.
        if constants is None:
            constants = samples_init.constants
        else:
            # Validate.
            if constants.keys() != samples_init.constants.keys():
                raise ValueError(
                    "If constants are overwridden, they must be for the same "
                    "set of parameters, though they may use different values."
                )
        # Pull duplicates from initializing samples object.  There is no option
        # to override currently.
        duplicates = samples_init.duplicates

        # Pull parameter names from initializing samples object.
        param_names = samples_init.param_names

        # Initialize output.
        init_state = numpy.empty((n_walkers, samples_init.n_dim))

        # Take a random set of samples from the initializing object as our
        # initial state.
        if pop_inf is None:
            # Memory-efficient case when there is no sample rejection.
            candidates = len(samples_init)
        else:
            # Handle sample rejection, loading all parameters into memory.
            ## CANT DO THIS -- break into smaller chunks of memory
            ## Solution is to set a fixed chunk size, load in a batch of
            ## samples, and only store in the array those that are not rejected.
            ## Then take a random fraction of that chunk, such that taking that
            ## fraction from all chunks will add up to the number of samples
            ## needed.
            n_drawn = 0
#            log_post = numpy.empty(len(samples_init))
#            candidates = numpy.empty(len(samples_init), dtype=bool)
            chunk_size = 1024
            for i_start in range(0, len(samples_init), chunk_size):
                # Set up slice object for input samples.
                i_stop = min(i_start+chunk_size, len(samples_init))
                slc = slice(i_start, i_stop)

                # Pull out the parameters for this chunk, both as a samples
                # array and as a parameters map.
                parameters_chunk = samples_init.get_params(slc)
                samples_chunk = samples_init.get_samples(slc)

                # Compute the log posterior for the current dataset.
                log_post_and_prior_chunk = (
                    pop_inf.log_posterior_from_params(parameters_chunk)
                )
                log_post_chunk = log_post_and_prior_chunk[...,0]

                # Determine indices with posterior support.
                i_good = log_post_chunk > numpy.NINF

                # Determine how many samples to keep from this iteration.
                # Truncate if we've cumulatively exceeded the number of walkers.
                n_good = min(numpy.count_nonzero(i_good), n_walkers-n_drawn)

                # Store good samples in walker arrays.
                init_state[n_drawn:n_drawn+n_good] = (
                    samples_chunk[i_good][:n_good]
                )

#                log_post[slc] = log_post_and_prior_chunk[...,0]

                if n_drawn == n_walkers:
                    break

#            candidates = numpy.flatnonzero(log_post > numpy.NINF)

        # i_choice = random_state.choice(
        #     candidates, n_walkers,
        #     replace=False,
        # )
        # for i_walker, i_choice in enumerate(i_choice):
        #     init_state[i_walker] = samples_init.get_samples(i_choice)

        # Create a new H5RawPosteriorSamples object, initialized to the final
        # state of ``raw_samples_init``.
        return cls.create(
            directory,
            param_names,
            init_state,
            constants=constants,
            duplicates=duplicates,
            dtype=dtype,
            chunk_size=chunk_size,
            zero_padding_length=zero_padding_length,
            swmr_mode=swmr_mode,
            force=force,
        )

    def close(self) -> None:
        self.master_file.close()

    def append(
            self,
            sample: numpy.ndarray,
            posterior_log_prob: numpy.ndarray, prior_log_prob: numpy.ndarray,
        ) -> None:
        chain = self.get_chain_link(self._get_latest_chain())

        if chain.attrs["next"] == self.chunk_size:
            # create next sub-file
            self.create_subfile()
            chain = self.get_chain_link(self._get_latest_chain())

        index = chain.attrs["next"]

        chain["samples"][index] = sample
        chain["posterior_log_probs"][index] = posterior_log_prob
        chain["prior_log_probs"][index] = prior_log_prob

        if self.master_file.swmr_mode:
            chain["samples"].flush()
            chain["posterior_log_probs"].flush()
            chain["prior_log_probs"].flush()

        chain.attrs["next"] += 1
        self.master_file.attrs["n_samples"] += 1

        if self.master_file.swmr_mode:
            self.master_file.flush()


    def get_samples(self, index) -> numpy.ndarray:
        return self._get_field(index, "samples")

    def get_posterior_log_prob(self, index) -> numpy.ndarray:
        return self._get_field(index, "posterior_log_probs")

    def get_prior_log_prob(self, index) -> numpy.ndarray:
        return self._get_field(index, "prior_log_probs")

    def _get_field(self, index, field_name: str) -> numpy.ndarray:
        if isinstance(index, int):
            return self._get_field_int(index, field_name)
        elif isinstance(index, slice):
            return self._get_field_slice(index, field_name)
        elif index is ...:
            return self._get_field_all(field_name)
        else:
            raise TypeError(
                "Unable to handle indices of type {}"
                .format(type(index))
            )

    def _get_field_int(self, index: int, field_name: str) -> numpy.ndarray:
        n_samples = len(self)

        if index >= n_samples:
            raise IndexError(
                "Index {} out of range.  Only {} samples available."
                .format(index, len(self))
            )

        # If a negative index was used, we count from the end.  Convert to the
        # equivalent positive index.
        if index < 0:
            index = n_samples + index

        chain_number = index // self.chunk_size
        index_within_chain = index % self.chunk_size

        chain = self.get_chain_link(chain_number)
        return chain[field_name][index_within_chain]

    def _get_field_slice(self, index: slice, field_name: str) -> numpy.ndarray:
        n_samples_total = len(self)

        start = index.start if index.start is not None else 0
        stop = index.stop if index.stop is not None else n_samples_total
        step = index.step if index.step is not None else 1

        # No support right now for arbitrary step-sizes.
        if step != 1:
            raise NotImplementedError(
                "Slices with step-sizes other than 1 are not currently "
                "implemented."
            )

        # Convert `start` and `stop` to equivalent positive indices if they're
        # negative.
        if start < 0:
            start = n_samples_total + start
        if stop < 0:
            stop = n_samples_total + stop

        # If `stop` goes off the end of the file, truncate
        if stop > n_samples_total:
            stop = n_samples_total

        # Determine the shape of each chunk by querying the first one.
        chunk_shape = self.get_chain_link(0)[field_name].shape
        # Determine number of samples in output array
        ## TODO: support step-sizes other than 1
        n_samples = max(0, stop - start)

        # Complete output will have the same shape as each chunk, but the first
        # axis will instead be scaled to the number of samples.
        output_shape = list(chunk_shape)
        output_shape[0] = n_samples

        output = numpy.empty(output_shape, self.dtype)

        # If there are no samples to be grabbed, exit now.
        if n_samples == 0:
            return output

        # Will update with number of samples still needed.
        needed = n_samples

        # Determine index of the chain containing `start`, as well as the index
        # within that chain containing the sample.
        start_chain_number = start // self.chunk_size
        i_start = start % self.chunk_size
        chain = self.get_chain_link(start_chain_number)

        # Determine the number of samples that we will take from the first
        # chain.
        taken = min(needed, self.chunk_size - i_start)

        # Populate the first set of samples in the output array.
        output[:taken] = chain[field_name][i_start:i_start+taken]

        # Will update with the next index to populate
        next_output_index = taken

        # Update number of samples still needed
        needed -= taken

        # If no more samples are needed, return now.
        if needed == 0:
            return output

        # Iterate over the remaining chains
        n_chains = self._get_latest_chain() + 1
        for chain_number in range(start_chain_number+1, n_chains):
            chain = self.get_chain_link(chain_number)

            # Still need to iterate over more chains.  Grab all samples.
            if needed > self.chunk_size:
                output[next_output_index:next_output_index+self.chunk_size] = (
                    chain[field_name][()]
                )
                needed -= self.chunk_size
                next_output_index += self.chunk_size
            # This chain has all the remaining samples.  Grab them and return.
            else:
                output[next_output_index:] = chain[field_name][:needed]
                return output


    def _get_field_all(self, field_name: str) -> numpy.ndarray:
        # Need to know the number of chains that exist.
        n_chains = self._get_latest_chain() + 1

        # Determine the shape of each chunk by querying the first one.
        chunk_shape = self.get_chain_link(0)[field_name].shape
        # Complete output will have the same shape as each chunk, but the first
        # axis will instead be scaled to the number of samples.
        output_shape = list(chunk_shape)
        output_shape[0] = len(self)

        # Initialize output array.
        output = numpy.empty(output_shape, self.dtype)

        # Iterate over each of the chains, inserting all of their samples into
        # `output`.  The "next" attribute tells the index where a future sample
        # should be inserted, or likewise the number of samples currently there,
        # so we take all of the samples up through that number (exclusive).
        # `i` tells us how many samples have been extracted so far, so we don't
        # overwrite any samples in the output.
        i = 0
        for chain_number in range(n_chains):
            chain = self.get_chain_link(chain_number)
            n_within_chain = chain.attrs["next"]

            output[i:i+n_within_chain] = chain[field_name][:n_within_chain]

            i += n_within_chain

        return output


    def __len__(self) -> int:
        return self.master_file.attrs["n_samples"]

    def _get_latest_chain(self):
        return self.master_file["chain_links"].attrs["latest"]

    def _increment_latest_chain(self):
        self.master_file["chain_links"].attrs["latest"] += 1

    def create_subfile(self) -> None:
        self._increment_latest_chain()

        ## TODO: figure out why self.n_dim needs to be casted -- it shouldn't
        H5RawPosteriorSamples._create_subfile(
            self.directory,
            typing.cast(int, self.n_walkers), typing.cast(int, self.n_dim),
            self.master_file, self._get_latest_chain(),
            self.chunk_size, self.zero_padding_length,
            self.dtype,
        )

    @staticmethod
    def _create_subfile(
            directory: str,
            n_walkers: int, n_dim: int,
            master_file, chain_number: int,
            chunk_size: int, zero_padding_length: int,
            dtype,
        ) -> None:
        import h5py
        import os.path

        num_str = H5RawPosteriorSamples._get_chain_num_str(
            chain_number, zero_padding_length,
        )
        relname = "subfile-{}.hdf5".format(num_str)
        filename = os.path.join(directory, relname)

        with h5py.File(filename, "w-", libver="latest") as subfile:
            subfile.create_dataset(
                "samples", (chunk_size, n_walkers, n_dim),
                dtype=dtype,
            )
            subfile.create_dataset(
                "posterior_log_probs", (chunk_size, n_walkers),
                dtype=dtype,
            )
            subfile.create_dataset(
                "prior_log_probs", (chunk_size, n_walkers),
                dtype=dtype,
            )

            subfile.attrs["next"] = 0

    def get_chain_link(self, chain_number):
        return H5RawPosteriorSamples._get_chain_link(
            self.master_file["chain_links"], chain_number,
            self.zero_padding_length,
        )

    @staticmethod
    def _get_chain_link(chain_link_group, chain_number, zero_padding_length):
        num_str = H5RawPosteriorSamples._get_chain_num_str(
            chain_number, zero_padding_length,
        )
        return chain_link_group[num_str]

    @staticmethod
    def _get_chain_num_str(chain_number: int, zero_padding_length: int) -> str:
        return ("{:>0" + str(zero_padding_length) + "}").format(chain_number)

    def __enter__(self) -> "H5RawPosteriorSamples":
        return self

    def __exit__(self, exc_type, exc_value, traceback) -> bool:
        self.close()
        return False

    def _is_constituent_file(self, f):
        name = f.name

        prefix = "subfile-"
        suffix = ".hdf5"

        # Check if it is the master file.
        if f.name == "master.hdf5":
            return True

        # Check if it has the same number of characters as a subfile.
        elif len(f.name) == (len(prefix)+len(suffix)+self.zero_padding_length):
            # Check that the prefix matches.
            if f.name[:len(prefix)] != prefix:
                return False
            # Check that the suffix matches.
            if f.name[-len(suffix):] != suffix:
                return False

            try:
                # Parse a number from the subfile number.
                number = int(f.name[len(prefix):-len(suffix)])
                # Check that the subfile number should exist.
                if number > self._get_latest_chain():
                    return False
            # Was unable to parse a number from the subfile number, so not a
            # match
            except ValueError:
                return False

            # No checks failed, so it's a subfile.
            return True

        # No match, not a constituent file.
        else:
            return False

    @classmethod
    def recover_master_file(
            cls,
            directory: str, swmr_mode: bool=False,
            param_names: typing.Optional[typing.List[str]]=None,
            metadata: typing.Optional[typing.Dict[str,typing.Any]]=None,
            constants: typing.Optional[typing.Dict[str,float]]=None,
            duplicates: typing.Optional[typing.Dict[str,str]]=None,
        ) -> None:
        import os
        import glob
        import h5py

        # Can handle ``param_names`` being ``None`` if both ``constants`` and
        # ``duplicates`` are as well, but if they're provided, then so too must
        # param_names.
        if param_names is None:
            if (constants is not None) or (duplicates is not None):
                raise ValueError(
                    "'param_names' must be provided if 'constants' or "
                    "'duplicates' is provided.  Otherwise ambiguities arise."
                )

        # Set default empty dicts for metadata, constants, and duplicates if not
        # provided.
        if metadata is None:
            metadata = {} # type: typing.Dict[str,typing.Any]
        if constants is None:
            constants = {} # type: typing.Dict[str,float]
        if duplicates is None:
            duplicates = {} # type: typing.Dict[str,str]

        # Get names of any existing subfiles in the directory.
        subfile_names = sorted(
            glob.glob(os.path.join(directory, "subfile-*.hdf5"))
        )

        ## Extract formatting info from first subfile. ##
        first_subfile_name = subfile_names[0]
        # Get zero-padding-length from first subfile
        zero_padding_length = (
            len(os.path.basename(first_subfile_name)) - len("subfile-.hdf5")
        )
        # Get information present within first subfile itself.
        first_subfile = h5py.File(first_subfile_name, "r", libver="latest")
        with first_subfile:
            samples_shape = first_subfile["samples"].shape
            chunk_size, n_walkers, n_variables = samples_shape

        # If ``param_names`` is ``None``, set default values of ``variable0``,
        # ``variable1``, ...
        if param_names is None:
            param_names = ["variable{}".format(i) for i in range(n_variables)]
        # Otherwise, double-check that there are the right number of parameters
        else:
            expected_n_parameters = n_variables+len(constants)+len(duplicates)
            if expected_n_parameters != len(param_names):
                raise ValueError(
                    "Number of parameters doesn't match up with number of "
                    "variables, constants, and duplicates.  Expected {} "
                    "parameters, but ``param_names`` has {} entries."
                    .format(expected_n_parameters, len(param_names))
                )

        # Begin iterating over the subfiles, verifying that they are contiguous
        # and consistent.
        n_samples = 0 # type: int
        latest_chain = 0 # type: int
        must_be_last = False # type: bool
        for i, subfile_name in enumerate(subfile_names):
            # If the previous subfile should have been the last one, but wasn't,
            # fail.
            if must_be_last:
                raise ValueError(
                    "Subfile '{}' was not filled, so subfile '{}' should not "
                    "exist."
                    .format(subfile_names[i-1], subfile_name)
                )

            # Get name without extra formatting
            subfile_basename = os.path.basename(subfile_name)

            # Determine what this filename *should* be, for validation.
            num_str = cls._get_chain_num_str(
                i, zero_padding_length,
            )
            expected_basename = "subfile-{}.hdf5".format(num_str)

            # If name doesn't match what we expected, fail.
            if expected_basename != subfile_basename:
                raise ValueError(
                    "At least one subfile was misnamed or missing.  While "
                    "iterating through, '{}' was found where '{}' was expected."
                    .format(subfile_basename, expected_basename)
                )

            # Get information present within subfile itself.
            subfile = h5py.File(subfile_name, "r", libver="latest")
            with subfile:
                # Verify that formatting of samples are consistent.  Should have
                # (chunk_size, n_walkers, n_variables) which match all other
                # subfiles.
                if samples_shape != subfile["samples"].shape:
                    raise ValueError(
                        "Samples in subfile '{}' do not match shape from first "
                        "subfile.  Expected {}, but got {}."
                        .format(
                            subfile_basename,
                            samples_shape, subfile["samples"].shape,
                        )
                    )
                # Determine which sample we were up to.  If this doesn't point
                # to the last possible sample in the file, then there should not
                # be any files following this.
                subfile_next_sample = subfile.attrs["next"]
                n_samples += subfile_next_sample
                if subfile_next_sample < chunk_size:
                    must_be_last = True

                # Remember what the last chain used was.
                latest_chain = i

        # Now that we're confident the subfiles were properly formatted, and we
        # have all the information we can get from them, create the master file.
        filename = cls._get_master_filename(directory)
        mode = "w"
        master_file = h5py.File(
            filename, mode,
            libver="latest", swmr=swmr_mode,
        )

        with master_file:

            # Enable SWMR mode if requested
            if swmr_mode:
                master_file.swmr_mode = swmr_mode

            master_file.attrs["chunk_size"] = chunk_size
            master_file.attrs["zero_padding_length"] = zero_padding_length

            master_file.attrs["n_samples"] = n_samples

            master_file.create_dataset(
                "param_names",
                data=numpy.array(param_names, dtype="S"),
            )

            metadata_group = master_file.create_group("metadata")
            constants_group = master_file.create_group("constants")
            duplicates_group = master_file.create_group("duplicates")

            for name, value in metadata.items():
                metadata_group.attrs[name] = value

            for name, const in constants.items():
                constants_group.attrs[name] = const

            for target, source in duplicates.items():
                duplicates_group.attrs[target] = source

            # Create group for ExternalLink's to sub-files
            chain_links = master_file.create_group("chain_links")
            chain_links.attrs["latest"] = latest_chain

            # Initialize links to sub-files even though some don't exist yet
            # and some may never exist.
            for i in range(10**zero_padding_length):
                num_str = cls._get_chain_num_str(
                    i, zero_padding_length,
                )
                relname = "subfile-{}.hdf5".format(num_str)
                chain_links[num_str] = h5py.ExternalLink(relname, "/")

    def prune_chains(
            self,
            dirname: str,
            n_samples: typing.Optional[int]=None,
            ln_post_thresh: typing.Optional[float]=None,
            prune_stationary: bool=False,
        ) -> "H5RawPosteriorSamples":
        # Slice to use, either including all samples if no number is provided,
        # or taking the last ``n_samples`` if that number is provided.
        slc = ... if n_samples is None else slice(-n_samples, None)

        # Extract samples.
        samples = self.get_samples(slc)
        log_post = self.get_posterior_log_prob(slc)
        log_prior = self.get_prior_log_prob(slc)

        # Initialize bit mask.
        good_chains = numpy.ones(self.n_walkers, dtype=bool)

        # Handle delta log posterior criterion.
        if ln_post_thresh is not None:
            log_post_max = numpy.nanmax(log_post)
            good_chains &= numpy.any(
                (log_post_max - log_post) < ln_post_thresh,
                axis=0,
            )

        # Handle stationary chains.
        if prune_stationary:
            # For each walker and parameter, confirm that any samples have
            # changed.  Then if this was true for any parameter, accept that
            # walker.
            good_chains &= numpy.any(
                samples[numpy.newaxis,0] != samples,
                axis=(0,2),
            )

        # Reduce samples to good chains.
        samples = samples[:,good_chains]
        log_post = log_post[:,good_chains]
        log_prior = log_prior[:,good_chains]

        # Create new output file.
        output_chains = H5RawPosteriorSamples.create(
            dirname,
            self.param_names,
            samples[0],
            constants=self.constants,
            duplicates=self.duplicates,
            dtype=self.dtype,
            chunk_size=self.chunk_size,
            zero_padding_length=self.zero_padding_length,
            force=False,
        )

        # Save output data one sample at a time.
        for i in range(1, len(samples)):
            output_chains.append(samples[i], log_post[i], log_prior[i])

        return output_chains

    def summary(self) -> None:
        import os

        mid_file_prefix = "├──"
        end_file_prefix = "└──"

        files = sorted(
            filter(
                self._is_constituent_file,
                os.scandir(self.directory),
            ),
            key=lambda f: f.name
        )
        n_files = len(files)

        print("Directory:", os.path.join(self.directory, ""))
        print("Samples:", len(self))
        print("Walkers:", self.n_walkers)
        print("Chunk size:", self.chunk_size)
        print("Variables:", self.variable_names)
        print("Constants:", self.constants)
        print("Duplicates:", self.duplicates)

        print("Files:")
        for i, f in enumerate(files):
            is_first = i == 0
            is_last = (i+1) == n_files

            name = f.name
            size = f.stat().st_size
            file_prefix = end_file_prefix if is_last else mid_file_prefix

            if is_first:
                file_info = "size={size}B".format(size=size)
            else:
                subfile = self.get_chain_link(i-1)
                n = subfile.attrs["next"]
                file_info = "size={size}B, samples={n}".format(size=size, n=n)

            print(file_prefix, name, ":", file_info)


    @staticmethod
    def cli(raw_args=None):
        if raw_args is None:
            import sys
            raw_args = sys.argv[1:]

        import argparse

        parser = argparse.ArgumentParser()

        parser.add_argument(
            "directory",
            help="Directory containing posterior samples files.",
        )

        parser.add_argument(
            "--swmr-mode",
            action="store_true",
            help="Read file in single-writer multiple reader mode.",
        )

        subparsers = parser.add_subparsers(dest="command")

        ## Summary ##
        subparser_summary = subparsers.add_parser("summary")

        ## Plot chains ##
        subparser_plot_chains = subparsers.add_parser("plot_chains")

        subparser_plot_chains.add_argument(
            "plot_filename",
            help="Name of file to store plot in.",
        )
        subparser_plot_chains.add_argument(
            "--skip-samples",
            type=int, default=0,
            help="Number of samples to skip at the beginning.",
        )
        subparser_plot_chains.add_argument(
            "--rolling-average",
            type=int, metavar="N",
            help="Overlay a rolling average of each variable.  Must specify "
                 "the number of samples to average over.",
        )
        subparser_plot_chains.add_argument(
            "--color-code-log-post",
            help="Color-code chains based on their log(post) value.  Must "
                 "specify a matplotlib colormap name.",
        )
        subparser_plot_chains.add_argument(
            "--log-post-color",
            default="C0",
            help="Color to display log(post) as, if --color-code-log-post not "
                 "given.",
        )
        subparser_plot_chains.add_argument(
            "--log-prior-color",
            default="C1",
            help="Color to display log(prior) as.",
        )
        subparser_plot_chains.add_argument(
            "--mpl-backend",
            default="Agg",
            help="Backend to use for matplotlib.",
        )

        ## Prune chains ##
        subparser_prune_chains = subparsers.add_parser("prune_chains")

        subparser_prune_chains.add_argument(
            "output_dirname",
            help="Name of new raw chains directory to write to.",
        )
        subparser_prune_chains.add_argument(
            "--n-samples",
            type=int,
            help="Take a specific number of samples from the end of the "
                 "chains.",
        )
        subparser_prune_chains.add_argument(
            "--ln-post-threshold",
            type=float,
            help="Threshold from max ln(post) a chain must reach in order to "
                 "avoid pruning.  Chain is kept if threshold met at any sample "
                 "being considered.",
        )
        subparser_prune_chains.add_argument(
            "--prune-stationary",
            action="store_true",
            help="Remove chains which do not move for any of the samples being "
                 "considered.",
        )

        subparser_recover = subparsers.add_parser("recover")

        subparser_recover.add_argument(
            "reference_directory",
            nargs="?",
            help="A non-corrupted H5RawPosteriorSamples directory, from which "
                 "the master file's contents will be referenced.  If not "
                 "provided, then new master file will be missing metadata.",
        )

        cli_args = parser.parse_args(raw_args)


        if cli_args.command == "summary":
            f = H5RawPosteriorSamples(
                cli_args.directory, "r",
                swmr_mode=cli_args.swmr_mode,
            )
            with f as h5_samples:
                h5_samples.summary()

        elif cli_args.command == "plot_chains":
            import matplotlib
            f = H5RawPosteriorSamples(
                cli_args.directory, "r",
                swmr_mode=cli_args.swmr_mode,
            )
            with f as h5_samples:
                matplotlib.use(cli_args.mpl_backend)

                fig, axes = h5_samples.plot_chains(
                    skip_samples=cli_args.skip_samples,
                    rolling_average=cli_args.rolling_average,
                    color_code_log_post=cli_args.color_code_log_post,
                    log_post_color=cli_args.log_post_color,
                    log_prior_color=cli_args.log_prior_color,
                )
                fig.savefig(cli_args.plot_filename)

        elif cli_args.command == "prune_chains":
            f = H5RawPosteriorSamples(
                cli_args.directory, "r",
                swmr_mode=cli_args.swmr_mode,
            )
            with f as h5_samples:
                new_h5_samples = h5_samples.prune_chains(
                    cli_args.output_dirname,
                    n_samples=cli_args.n_samples,
                    ln_post_thresh=cli_args.ln_post_threshold,
                    prune_stationary=cli_args.prune_stationary,
                )
                new_h5_samples.close()

        elif cli_args.command == "recover":
            if cli_args.reference_directory is None:
                param_names = None
                constants = None
                duplicates = None
                metadata = None
            else:
                reference_file = H5RawPosteriorSamples(
                    cli_args.reference_directory, "r",
                    swmr_mode=cli_args.swmr_mode,
                )
                with reference_file:
                    param_names = reference_file.param_names
                    constants = reference_file.constants
                    duplicates = reference_file.duplicates
                    metadata = reference_file.metadata
                    if metadata is not None:
                        metadata = dict(metadata)

            restored_file = H5RawPosteriorSamples.recover_master_file(
                cli_args.directory, swmr_mode=cli_args.swmr_mode,
                param_names=param_names,
                constants=constants, duplicates=duplicates, metadata=metadata,
            )

        else:
            raise NotImplementedError(
                "Unknown command '{c}' provided.".format(c=cli_args.command)
            )


class H5CleanedPosteriorSamples(PosteriorSamples):
    r"""
    Class for storing post-processed posterior samples, in a single,
    non-extensible HDF5-based format.

    Only use the constructor method after the file has been initialized.  To
    initialize, instead call the static method :py:meth:`create`.
    """
    def __init__(self, filename: str) -> None:
        import h5py

        self.filename = filename
        self.h5_file = h5py.File(filename, "r", libver="latest")

        self._n_samples = self.h5_file["posterior_log_probs"].size
        self._is_weighted = "weights" in self.h5_file

        param_names = [
            name.decode("utf-8")
            for name in self.h5_file["param_names"][()]
        ]

        dtype = self.h5_file["samples"].dtype

        metadata = dict(self.h5_file["metadata"].attrs)
        constants = dict(self.h5_file["constants"].attrs)
        duplicates = dict(self.h5_file["duplicates"].attrs)

        super().__init__(
            param_names,
            metadata=metadata,
            constants=constants, duplicates=duplicates,
            dtype=dtype,
        )

    def close(self) -> None:
        self.h5_file.close()

    def __enter__(self) -> "H5CleanedPosteriorSamples":
        return self

    def __exit__(self, exc_type, exc_value, traceback) -> bool:
        self.close()
        return False

    def __len__(self) -> int:
        return self._n_samples

    def get_samples(self, index) -> numpy.ndarray:
        return self.h5_file["samples"][index]

    def get_posterior_log_prob(self, index) -> numpy.ndarray:
        return self.h5_file["posterior_log_probs"][index]

    def get_prior_log_prob(self, index) -> numpy.ndarray:
        return self.h5_file["prior_log_probs"][index]

    def get_weights(self, index) -> numpy.ndarray:
        if self.is_weighted:
            return self.h5_file["weights"][index]
        else:
            # Determine shape of output array.
            if index == ...:
                # If Ellipsis was provided, use full length of file.
                shape = len(self)
            elif isinstance(index, slice):
                # If a slice was provided, perform the arithmetic for the slice.
                # Credit to ShadowRanger on StackOverflow
                # <https://stackoverflow.com/a/36188683/4761692>
                start, stop, step = index.indices(len(self))
                shape = max(
                    0,
                    (stop - start + (step - (1 if step > 0 else -1))) // step
                )
            ## TODO: add more cases for alternative index types.
            else:
                # Fall back option: take another array that should have the same
                # shape, and compute its shape.  Suboptimal because of extra
                # memory usage.
                shape = self.get_posterior_log_prob(index).shape

            # Return ones broadcast to shape of needed output array.
            return numpy.broadcast_to(1.0, shape)

    @property
    def is_weighted(self) -> bool:
        return self._is_weighted

    def save_copy(
            self,
            filename: str,
            weights: typing.Optional[numpy.ndarray]=None,
            unique: bool=False,
            max_samples: typing.Optional[int]=None,
            random_state: typing.Optional[numpy.random.RandomState]=None,
            force: bool=False,
        ) -> "H5CleanedPosteriorSamples":
        """
        Saves a copy of this object to a new file, and returns the associated
        object.  Can optionally specify a maximum number of samples to save,
        in which case a random subset of samples are saved.
        """
        # Seed RNG.
        if random_state is None:
            random_state = numpy.random.RandomState()

        # Extract all data from self.
        samples_full = self.get_samples(...)
        posterior_log_probs_full = self.get_posterior_log_prob(...)
        prior_log_probs_full = self.get_posterior_log_prob(...)
        if weights is None:
            weights_full = self.get_weights(...)
        else:
            weights_full = weights

        # If 'unique' flag is on, remove duplicate samples which may arise from
        # stuck chains.
        if unique:
            _, unique_indices = numpy.unique(
                samples_full,
                return_index=True, axis=0,
            )
            samples_full = samples_full[unique_indices]
            posterior_log_probs_full = posterior_log_probs_full[unique_indices]
            prior_log_probs_full = prior_log_probs_full[unique_indices]
            weights_full = weights_full[unique_indices]

        # Count the number of samples.
        n_samples = len(samples_full)

        # If no maximum number of samples is specified, or the number of samples
        # does not exceed the max, just use all samples.
        if (max_samples is None) or (n_samples <= max_samples):
            samples = samples_full
            posterior_log_probs = posterior_log_probs_full
            prior_log_probs = prior_log_probs_full
            weights = weights_full
        # Otherwise, take a random subset of the samples to keep.
        else:
            i_keep = random_state.choice(
                n_samples, size=max_samples,
                replace=False,
            )
            samples = samples_full[i_keep]
            posterior_log_probs = posterior_log_probs_full[i_keep]
            prior_log_probs = prior_log_probs_full[i_keep]
            weights = weights_full[i_keep]

        # Return newly created object.
        return H5CleanedPosteriorSamples.from_data(
            filename,
            self.param_names,
            samples, posterior_log_probs, prior_log_probs,
            weights=weights,
            metadata=self.metadata,
            constants=self.constants, duplicates=self.duplicates,
            force=force,
        )

    @staticmethod
    def from_data(
            filename: str,
            param_names: typing.List[str],
            samples: numpy.ndarray,
            posterior_log_probs: numpy.ndarray,
            prior_log_probs: numpy.ndarray,
            weights: typing.Optional[numpy.ndarray]=None,
            constants: typing.Optional[typing.Dict[str,float]]=None,
            duplicates: typing.Optional[typing.Dict[str,str]]=None,
            metadata: typing.Optional[typing.Dict[str,typing.Any]]=None,
            force: bool=False,
        ) -> "H5CleanedPosteriorSamples":
        import h5py

        if metadata is None:
            metadata = {}
        if constants is None:
            constants = {}
        if duplicates is None:
            duplicates = {}

        mode = "w" if force else "w-"

        with h5py.File(filename, mode, libver="latest") as h5_file:

            h5_file.create_dataset(
                "param_names",
                data=numpy.array(param_names, dtype="S"),
            )

            metadata_group = h5_file.create_group("metadata")
            constants_group = h5_file.create_group("constants")
            duplicates_group = h5_file.create_group("duplicates")

            for k, v in metadata.items():
                metadata_group.attrs[k] = v

            for name, const in constants.items():
                constants_group.attrs[name] = const

            for target, source in duplicates.items():
                duplicates_group.attrs[target] = source

            h5_file.create_dataset(
                "samples", data=samples,
            )
            h5_file.create_dataset(
                "posterior_log_probs", data=posterior_log_probs,
            )
            h5_file.create_dataset(
                "prior_log_probs", data=prior_log_probs,
            )
            if weights is not None:
                h5_file.create_dataset(
                    "weights", data=weights,
                )


        return H5CleanedPosteriorSamples(filename)

    @staticmethod
    def from_raw_manual(
            filename: str,
            source_samples: H5RawPosteriorSamples,
            burnin: int=0,
            thinning: int=1,
            force: bool=False,
        ) -> "H5CleanedPosteriorSamples":
        r"""
        Extract samples from an :py:class:`H5RawPosteriorSamples` object,
        discarding the first ``burnin`` samples, and then taking one sample
        every ``thinning`` iterations.  Returns an
        :py:class:`H5CleanedPosteriorSamples` instance with the extracted
        samples.
        """
        i = slice(burnin, None, thinning)

        samples = source_samples.get_samples(...)[i]
        posterior_log_probs = source_samples.get_posterior_log_prob(...)[i]
        prior_log_probs = source_samples.get_posterior_log_prob(...)[i]

        n_samples, n_walkers, n_dim = samples.shape

        new_param_shape = n_samples*n_walkers, n_dim
        new_prob_shape = n_samples*n_walkers

        return H5CleanedPosteriorSamples.from_data(
            filename,
            source_samples.param_names,
            samples.reshape(new_param_shape),
            posterior_log_probs.reshape(new_prob_shape),
            prior_log_probs.reshape(new_prob_shape),
            metadata=source_samples.metadata,
            constants=source_samples.constants,
            duplicates=source_samples.duplicates,
            force=force,
        )

    @staticmethod
    def from_cleaned(
            filename: str,
            *source_samples: "H5CleanedPosteriorSamples",
            force: bool=False,
            shuffle: bool=False,
        ) -> "H5CleanedPosteriorSamples":
        r"""
        Combine samples from multiple :class:`H5CleanedPosteriorSamples`
        objects.  Metadata in first input is assumed to match all subsequent
        ones.
        """
        ## TODO: get shuffling to work
        if shuffle:
            raise NotImplementedError("Shuffling is not yet implemented")

        if len(source_samples) == 0:
            raise ValueError("Need at least one input.")

        # Pull off the first samples object, and extract its metadata.
        first_samples = source_samples[0]
        param_names = first_samples.param_names
        variable_names = first_samples.variable_names
        metadata = first_samples.metadata
        constants = first_samples.constants
        duplicates = first_samples.duplicates
        dtype = first_samples.dtype

        # Determine the dimensions of the arrays we'll need to make.
        n_samples = sum(len(s) for s in source_samples)
        n_dim = len(variable_names)

        # Initialize output arrays.
        samples = numpy.empty((n_samples, n_dim), dtype=dtype)
        posterior_log_probs = numpy.empty(n_samples, dtype=dtype)
        prior_log_probs = numpy.empty(n_samples, dtype=dtype)
        if any(s.is_weighted for s in source_samples):
            weights = numpy.ones(n_samples, dtype=dtype)
        else:
            weights = None

        # Iterate over each source file, and extract its samples.
        i = 0
        for s in source_samples:
            # Determine indices where we will store samples for this source.
            n = len(s)
            slc = slice(i, i+n)

            # Store samples.
            samples[slc] = s.get_samples(...)
            posterior_log_probs[slc] = s.get_posterior_log_prob(...)
            prior_log_probs[slc] = s.get_prior_log_prob(...)

            if s.is_weighted:
                weights[slc] = s.get_weights(...)

            # Move starting index for next source.
            i += n

        ## TODO: shuffle samples if requested.

        # Return newly created object.
        return H5CleanedPosteriorSamples.from_data(
            filename,
            param_names,
            samples, posterior_log_probs, prior_log_probs,
            weights=weights,
            metadata=metadata, constants=constants, duplicates=duplicates,
            force=force,
        )

    def summary(self) -> None:
        print("File name:", self.filename)
        print("Samples:", len(self))
        print("Metadata:", self.metadata)
        print("Variables:", self.variable_names)
        print("Constants:", self.constants)
        print("Duplicates:", self.duplicates)

    def to_ascii(
            self,
            filename: str, mode: str="w",
            suppress_metadata: bool=False, suppress_constants: bool=False,
            suppress_probs: bool=False,
            delimiter=" ", comments="#",
        ) -> None:
        # Validate output mode.
        supported_modes = {"w", "x"}
        if mode not in supported_modes:
            raise ValueError(
                "Mode must be one of: {}"
                .format(", ".join(supported_modes))
            )

        # Determine which parameters we're outputting explicitly.
        displayed_param_names = (
            self.variable_names if suppress_constants else self.param_names
        )
        column_names = displayed_param_names.copy()
        if not suppress_probs:
            column_names += ["log_post", "log_prior"]

        # Open file for writing output.
        with open(filename, mode) as ascii_file:
            # Write column header.
            print(comments, end=" ", file=ascii_file)
            print(*column_names, sep=delimiter, file=ascii_file)

            # Write an extra blank line for clarity.
            print(comments, file=ascii_file)

            # Write metadata if not suppressed
            if not suppress_metadata:
                items = [
                    ("Constants", self.constants),
                    ("Duplicates", self.duplicates),
                    ("Metadata", self.metadata),
                ] # type: typing.List[typing.Tuple[str,typing.Mapping[str, object]]]

                for name, data in items:
                    # Write header line for specific type of metadata.
                    print(
                        "{} {}:".format(comments, name),
                        file=ascii_file,
                    )
                    # Write each key, value pair in the metadata's dictionary,
                    # with extra indentation.
                    for k, v in data.items():
                        print(
                            "{}    {} = {}".format(comments, k, v),
                            file=ascii_file,
                        )
                    # Write an extra blank line for clarity.
                    print(comments, file=ascii_file)

            # Iterate over each sample, and write it to the file.
            for i in range(len(self)):
                # Print the parameters.
                parameters = self.get_params(i)
                print(
                    *(
                        numpy.asarray(parameters[name]).item()
                        for name in displayed_param_names
                    ),
                    sep=delimiter, end="",
                    file=ascii_file,
                )
                # Print the log probabilities if not suppressed.
                if not suppress_probs:
                    print(
                        "",
                        self.get_posterior_log_prob(i),
                        self.get_prior_log_prob(i),
                        sep=delimiter, end="",
                        file=ascii_file,
                    )
                # End the line.
                print("", file=ascii_file)


    @staticmethod
    def cli(raw_args=None):
        if raw_args is None:
            import sys
            raw_args = sys.argv[1:]

        import argparse

        parser = argparse.ArgumentParser()

        parser.add_argument(
            "filename",
            help="Posterior sample file's name.",
        )

        subparsers = parser.add_subparsers(dest="command")

        ## Summary ##
        subparser_summary = subparsers.add_parser("summary")

        ## Combiner ##
        subparser_combine = subparsers.add_parser("combine")

        subparser_combine.add_argument(
            "source_file",
            nargs="+",
            help="HDF5 file(s) to combine.",
        )

        subparser_combine.add_argument(
            "--force",
            action="store_true",
            help="Force write in event that output file exists.",
        )

        ## Copier ##
        subparser_copy = subparsers.add_parser("copy")

        subparser_copy.add_argument(
            "output_file",
            help="Save copy in this file.",
        )

        subparser_copy.add_argument(
            "--unique",
            action="store_true",
            help="If any samples are exact duplicates, removes all but one "
                 "copy.",
        )

        subparser_copy.add_argument(
            "--max-samples",
            type=int,
            help="Maximum number of samples to store in copy.",
        )
        subparser_copy.add_argument(
            "--seed",
            type=int,
            help="Seed the random number generator.",
        )
        subparser_copy.add_argument(
            "--force",
            action="store_true",
            help="Force overwrite if output file exists.",
        )

        ## ASCII Dump ##
        subparser_ascii = subparsers.add_parser("ascii_dump")

        subparser_ascii.add_argument(
            "output_file",
            help="Text file to write output to.",
        )

        subparser_ascii.add_argument(
            "--suppress-metadata",
            action="store_true",
            help="Suppress metadata in output header.",
        )
        subparser_ascii.add_argument(
            "--suppress-constants",
            action="store_true",
            help="Do not show constants/duplicates in output.",
        )
        subparser_ascii.add_argument(
            "--suppress-probs",
            action="store_true",
            help="Do not show log posterior/prior values in output.",
        )

        subparser_ascii.add_argument(
            "--delimiter",
            default=" ",
            help="Column delimiter to use in output.",
        )
        subparser_ascii.add_argument(
            "--comments",
            default="#",
            help="Comment character to use in output.",
        )

        ## Credible regions ##
        subparser_cr = subparsers.add_parser("credible_regions")

        subparser_cr.add_argument(
            "--include-median",
            action="store_true",
            help="Include median in output.",
        )
        subparser_cr.add_argument(
            "--quantiles",
            type=float, nargs="*", default=[0.05, 0.95],
            help="List the quantiles to include.",
        )
        subparser_cr.add_argument(
            "--equal-prob-bounds",
            type=float, nargs="*", default=[0.5, 0.9, 0.95],
            help="List the equal-probability bounds to include.",
        )

        subparser_cr.add_argument(
            "--interpolation",
            default="linear",
            choices=["linear", "lower", "higher", "midpoint", "nearest"],
            help="Interpolation method to use for quantiles.",
        )

        subparser_cr.add_argument(
            "--format",
            default="json",
            choices=["json"],
            help="Output format (default is json).",
        )

        ## Corner plot ##
        subparser_plot_corner = subparsers.add_parser("plot_corner")

        subparser_plot_corner.add_argument(
            "plot_filename",
            help="Name of file to store plot in.",
        )

        subparser_plot_corner.add_argument(
            "--set-scale",
            action="append", nargs=2, metavar=("PARAM_NAME", "SCALE"),
            help="Set the scale on a chosen parameter.  Scale can be one of "
                 "'linear' or 'log'.",
        )
        subparser_plot_corner.add_argument(
            "--truths",
            help="JSON file specifying specific values for the parameters, "
                 "typically the injected values in the case of synthetic data.",
        )

        subparser_plot_corner.add_argument(
            "--mpl-backend",
            default="Agg",
            help="Backend to use for matplotlib.",
        )

        cli_args = parser.parse_args(raw_args)

        if cli_args.command == "summary":
            with H5CleanedPosteriorSamples(cli_args.filename) as h5_samples:
                h5_samples.summary()

        elif cli_args.command == "combine":
            import contextlib
            with contextlib.ExitStack() as stack:
                source_files = (
                    stack.enter_context(H5CleanedPosteriorSamples(fname))
                    for fname in cli_args.source_file
                )
                h5_samples = H5CleanedPosteriorSamples.from_cleaned(
                    cli_args.filename, *source_files,
                    force=cli_args.force,
                )
                h5_samples.close()

        elif cli_args.command == "copy":
            with H5CleanedPosteriorSamples(cli_args.filename) as h5_source:
                if cli_args.seed is None:
                    random_state = None
                else:
                    random_state = numpy.random.RandomState(cli_args.seed)

                h5_target = h5_source.save_copy(
                    cli_args.output_file,
                    unique=cli_args.unique,
                    max_samples=cli_args.max_samples,
                    random_state=random_state,
                    force=cli_args.force,
                )
                h5_target.close()

        elif cli_args.command == "ascii_dump":
            with H5CleanedPosteriorSamples(cli_args.filename) as h5_samples:
                h5_samples.to_ascii(
                    cli_args.output_file, "w",
                    suppress_metadata=cli_args.suppress_metadata,
                    suppress_constants=cli_args.suppress_constants,
                    suppress_probs=cli_args.suppress_probs,
                    delimiter=cli_args.delimiter, comments=cli_args.comments,
                )

        elif cli_args.command == "credible_regions":
            with H5CleanedPosteriorSamples(cli_args.filename) as h5_samples:
                crs = h5_samples.credible_regions(
                    include_median=cli_args.include_median,
                    quantiles=cli_args.quantiles,
                    equal_prob_bounds=cli_args.equal_prob_bounds,
                    interpolation=cli_args.interpolation,
                )
                CredibleRegionsBasic.format_result(crs, format=cli_args.format)

        elif cli_args.command == "plot_corner":
            with H5CleanedPosteriorSamples(cli_args.filename) as h5_samples:
                import matplotlib
                matplotlib.use(cli_args.mpl_backend)

                rescale_parameters = (
                    None if cli_args.set_scale is None
                    else dict(cli_args.set_scale)
                )

                if cli_args.truths is None:
                    truths = None
                else:
                    import json
                    with open(cli_args.truths, "r") as truths_file:
                        truths = json.load(truths_file)


                fig = h5_samples.plot_corner(
                    rescale_parameters=rescale_parameters,
                    truths=truths,
                )
                fig.savefig(cli_args.plot_filename)

        else:
            raise KeyError(
                "Unknown command '{c}' provided.".format(c=cli_args.command)
            )


class PopulationInference(object):
    r"""
    Abstract representation of a class which can solve the Bayesian population
    inference problem, which adheres to the likelihood

    .. math::
       p(d_1, \ldots, d_N | \Lambda) \propto
       \exp[-\mu(\Lambda; \mathcal{S})] \,
       \prod_{n=1}^N
         \int p(d_n | \lambda) \, \rho(\lambda | \Lambda) \, \mathrm{d}\lambda
    """
    def __init__(
            self,
            poisson_mean: PoissonMean,
            detection_likelihoods: typing.Sequence[DetectionLikelihood],
            log_prior_fn: typing.Callable[...,Numeric],
            param_names: typing.List[str],
            constants: typing.Optional[typing.Dict[str,float]]=None,
            duplicates: typing.Optional[typing.Dict[str,str]]=None,
            before_prior_aux_fn: typing.Optional[AuxFnType]=None,
            after_prior_aux_fn: typing.Optional[AuxFnType]=None,
            args: typing.Optional[list]=None,
            kwargs: typing.Optional[dict]=None,
            xpy: types.ModuleType=numpy,
        ) -> None:
        self.poisson_mean = poisson_mean
        self.detection_likelihoods = detection_likelihoods
        self.log_prior_fn = log_prior_fn

        self.param_names = param_names
        self.constants = constants if constants is not None else {}
        self.duplicates = duplicates if duplicates is not None else {}

        self.variable_names = [
            name
            for name in self.param_names
            if (name not in self.constants) and (name not in self.duplicates)
        ]

        self.before_prior_aux_fn = before_prior_aux_fn
        self.after_prior_aux_fn = after_prior_aux_fn

        self.args = args if args is not None else []
        self.kwargs = kwargs if kwargs is not None else {}

        self.xpy = xpy

        self.n_dim = (
            len(self.param_names) - len(self.constants) - len(self.duplicates)
        )

    def log_posterior_from_params(
            self,
            parameters: Parameters,
        ) -> typing.Tuple[numpy.ndarray,numpy.ndarray]:
        log_post, log_prior = log_posterior(
            parameters,
            self.log_prior_fn, self.poisson_mean, self.detection_likelihoods,
            self.before_prior_aux_fn, self.after_prior_aux_fn,
            self.args, self.kwargs,
        )

        ## TODO: Make this more efficient -- no need for constructing two arrays
        ## and then constructing a 3rd array containing both.
        return numpy.stack((log_post, log_prior), axis=-1)


    def log_posterior(
            self,
            variables: numpy.ndarray,
        ) -> typing.Tuple[numpy.ndarray,numpy.ndarray]:
        parameters = get_params(
            variables,
            self.constants, self.duplicates, self.param_names,
        )
        return self.log_posterior_from_params(parameters)


class PopulationInferenceWithStorage(PopulationInference):
    r"""
    Abstract representation of a class which can solve the Bayesian population
    inference problem, which adheres to the likelihood

    .. math::
       p(d_1, \ldots, d_N | \Lambda) \propto
       \exp[-\mu(\Lambda; \mathcal{S})] \,
       \prod_{n=1}^N
         \int p(d_n | \lambda) \, \rho(\lambda | \Lambda) \, \mathrm{d}\lambda

    Requires that the output of this representation is done through some storage
    object, from the :py:class:`Posterior` class.
    """
    def __init__(
            self,
            out_post: Posterior,
            poisson_mean: PoissonMean,
            detection_likelihoods: typing.Sequence[DetectionLikelihood],
            log_prior_fn: typing.Callable[...,Numeric],
            before_prior_aux_fn: typing.Optional[AuxFnType]=None,
            after_prior_aux_fn: typing.Optional[AuxFnType]=None,
            args: typing.Optional[list]=None,
            kwargs: typing.Optional[dict]=None,
            xpy: types.ModuleType=numpy,
            dtype=numpy.float64,
        ) -> None:
        super().__init__(
            poisson_mean,
            detection_likelihoods,
            log_prior_fn,
            out_post.param_names,
            constants=out_post.constants, duplicates=out_post.duplicates,
            before_prior_aux_fn=before_prior_aux_fn,
            after_prior_aux_fn=after_prior_aux_fn,
            args=args, kwargs=kwargs,
            xpy=xpy,
        )

        self.out_post = out_post
        self.dtype = dtype


class PopulationInferenceGrid(PopulationInferenceWithStorage):
    r"""
    Concrete class for solving the Bayesian population inference problem on a
    grid of population parameters :math:`\Lambda`.  Evaluates the population
    posterior

    .. math::
       p(\Lambda | d_1, \ldots, d_N) \propto
       \pi(\Lambda) \,
       \exp[-\mu(\Lambda; \mathcal{S})] \,
       \prod_{n=1}^N
         \int p(d_n | \lambda) \, \rho(\lambda | \Lambda) \, \mathrm{d}\lambda

    on a grid, and stores the result in a :py:class:`PosteriorGrid` object
    passed to the constructor.
    """
    def __init__(
            self,
            out_grid: PosteriorGrid,
            poisson_mean: PoissonMean,
            detection_likelihoods: typing.Sequence[DetectionLikelihood],
            log_prior_fn: typing.Callable[...,Numeric],
            before_prior_aux_fn: typing.Optional[AuxFnType]=None,
            after_prior_aux_fn: typing.Optional[AuxFnType]=None,
            args: typing.Optional[list]=None,
            kwargs: typing.Optional[dict]=None,
            vectorize_grid_eval: bool=False,
            xpy: types.ModuleType=numpy,
            dtype=numpy.float64,
        ) -> None:
        super().__init__(
            out_grid,
            poisson_mean,
            detection_likelihoods,
            log_prior_fn,
            before_prior_aux_fn=before_prior_aux_fn,
            after_prior_aux_fn=after_prior_aux_fn,
            args=args, kwargs=kwargs,
            xpy=xpy,
            dtype=dtype,
        )

        assert isinstance(self.out_post, PosteriorGrid)

        # Compute log(probability) grids, either vectorized or non-vectorized
        # according to implementation.
        if vectorize_grid_eval:
            log_prob_grids = self._eval_vectorized()
        else:
            log_prob_grids = self._eval_nonvectorized()

        # Extract the posterior and prior components of the grids.
        posterior_log_prob_grid = log_prob_grids[...,0]
        prior_log_prob_grid = log_prob_grids[...,1]

        # Store the grids to ``out_grid``, where they can now be accessed.
        self.out_post.store_log_prob_grids(
            posterior_log_prob_grid, prior_log_prob_grid,
        )


    def _eval_vectorized(self):
        # Get the grids for each variable.
        grids = [
            self.out_post.variable_grids[name]
            for name in self.variable_names
        ]
        # Turn into a list of meshgrids for each variable.
        meshgrids = numpy.meshgrid(*grids, indexing="ij")
        # Join the meshgrids into a single array of variables.
        variables = numpy.stack(meshgrids, axis=-1)

        # Return the log(posterior) and log(prior) evaluated everywhere.
        return self.log_posterior(variables)


    def _eval_nonvectorized(self):
        # Initialize output grids array.
        grid_shape = self.out_post.grid_shape
        full_shape = grid_shape + (2,)
        log_prob_grids = numpy.empty(full_shape, self.dtype)

        # Fill in grid iteratively.
        for idx in numpy.ndindex(*grid_shape):
            variables = numpy.asarray([
                self.out_post.variable_grids[name][idx[i]]
                for i, name in enumerate(self.out_post.variable_names)
            ])
            log_prob_grids[idx] = self.log_posterior(variables)

        return log_prob_grids


class PopulationInferenceSampler(PopulationInferenceWithStorage):
    r"""
    Abstract class for solving the Bayesian population inference problem by
    drawing posterior samples of the population parameters :math:`\Lambda`,
    which adheres to the distribution

    .. math::
       p(\Lambda | d_1, \ldots, d_N) \propto
       \pi(\Lambda) \,
       \exp[-\mu(\Lambda; \mathcal{S})] \,
       \prod_{n=1}^N
         \int p(d_n | \lambda) \, \rho(\lambda | \Lambda) \, \mathrm{d}\lambda
    """
    def __init__(
            self,
            out_samples: PosteriorSamples,
            poisson_mean: PoissonMean,
            detection_likelihoods: typing.Sequence[DetectionLikelihood],
            log_prior_fn: typing.Callable[...,Numeric],
            before_prior_aux_fn: typing.Optional[AuxFnType]=None,
            after_prior_aux_fn: typing.Optional[AuxFnType]=None,
            args: typing.Optional[list]=None,
            kwargs: typing.Optional[dict]=None,
            xpy: types.ModuleType=numpy,
            dtype=numpy.float64,
            random_state: typing.Optional[numpy.random.RandomState]=None,
        ) -> None:
        super().__init__(
            out_samples,
            poisson_mean,
            detection_likelihoods,
            log_prior_fn,
            before_prior_aux_fn=before_prior_aux_fn,
            after_prior_aux_fn=after_prior_aux_fn,
            args=args, kwargs=kwargs,
            xpy=xpy,
            dtype=dtype,
        )

        if out_samples.state is None:
            raise ValueError(
                "State of output samples needs to be defined."
            )

        self.state = typing.cast(numpy.ndarray, out_samples.state)

        self.n_walkers = len(out_samples.state)
        self.dtype = dtype

        self.random_state = (
            random_state if random_state is not None
            else numpy.random.RandomState()
        )

    def posterior_samples(
            self,
            n_samples: int,
        ) -> numpy.ndarray:
        raise NotImplementedError()


from emcee.moves.red_blue import RedBlueMove
class RescaledRedBlueMove(RedBlueMove):
    """
    An abstract rescaled red-blue ensemble move.  Subclasses must
    define the rescaling function and its inverse.  Concrete class takes an
    existing RedBlueMove and a sequence of indices specifying the variables to
    rescale.

    Args:
        base_move (RedBlueMove): A concrete ``RedBlueMove`` instance to use as
            the move in the rescaled coordinate system.

        indices (sequence of int): A sequence of indices which specify the
            variables to rescale.
    """
    def __init__(self, base_move, indices):
        super().__init__(
            nsplits=base_move.nsplits,
            randomize_split=base_move.randomize_split,
            live_dangerously=base_move.live_dangerously,
        )

        self._base_move = base_move
        self._indices = tuple(indices)

        debug_verbose(
            "Set up rescaled move on indices", self._indices,
            mode="sampler", flush=True,
        )

    @property
    def base_move(self):
        return self._base_move

    @property
    def indices(self):
        return self._indices

    def rescale(self, samples):
        raise NotImplementedError(
            "The rescaling must be implemented by subclasses"
        )

    def rescale_inverse(self, samples):
        raise NotImplementedError(
            "The inverse rescaling must be implemented by subclasses"
        )

    def _rescale_all(self, sample):
        sample_rescaled = sample.copy()

        for i in self.indices:
            sample_rescaled[...,i] = self.rescale(sample_rescaled[...,i])

            debug_verbose(
                "Rescaled index", i,
                mode="sampler", flush=True,
            )

        return sample_rescaled

    def _rescale_inverse_all(self, sample_rescaled):
        sample = sample_rescaled.copy()

        for i in self.indices:
            sample[...,i] = self.rescale_inverse(sample[...,i])

            debug_verbose(
                "Inverse rescaled index", i,
                mode="sampler", flush=True,
            )

        return sample

    def log_jacobian(self, samples):
        raise NotImplementedError(
            "The log-Jacobian must be implemented by subclasses"
        )

    def _log_jacobian_determinant(self, sample):
        # Initialize output
        log_det = numpy.zeros(sample.shape[0])

        # Determinant is product of individual Jacobians
        for i in self.indices:
            log_det += self.log_jacobian(sample[...,i])

        return log_det

    def get_proposal(self, sample, complement, random):
        sample_rescaled = self._rescale_all(sample)
        complement_rescaled = [self._rescale_all(c) for c in complement]

        q_rescaled, factors_rescaled = self.base_move.get_proposal(
            sample_rescaled, complement_rescaled,
            random,
        )

        q = self._rescale_inverse_all(q_rescaled)
        factors = (
            factors_rescaled +
            self._log_jacobian_determinant(q) -
            self._log_jacobian_determinant(sample)
        )

        return q, factors

    def propose(self, model, state):
        self.base_move.setup(state.coords)

        return super().propose(model, state)

    def __str__(self):
        return "{cls}(base_move={base_move}, indices={indices})".format(
            cls=self.__class__, base_move=self.base_move, indices=self.indices,
        )

class LogScaledRedBlueMove(RescaledRedBlueMove):
    """
    An ensemble move which takes an existing move, and takes the logarithm of
    one or more of the state variables before passing to the move.

    Args:
        base_move (RedBlueMove): A concrete ``RedBlueMove`` instance to use as
            the move in the rescaled coordinate system.

        indices (sequence of int): A sequence of indices which specify the
            variables to rescale.
    """
    def rescale(self, samples):
        return numpy.log(samples)

    def rescale_inverse(self, samples):
        return numpy.exp(samples)

    def log_jacobian(self, samples):
        return numpy.log(samples)

class PopulationInferenceEmceeSampler(PopulationInferenceSampler):
    r"""
    Concrete class for solving the Bayesian population inference problem by
    drawing posterior samples of the population parameters :math:`\Lambda`,
    which adheres to the distribution

    .. math::
       p(\Lambda | d_1, \ldots, d_N) \propto
       \pi(\Lambda) \,
       \exp[-\mu(\Lambda; \mathcal{S})] \,
       \prod_{n=1}^N
         \int p(d_n | \lambda) \, \rho(\lambda | \Lambda) \, \mathrm{d}\lambda

    by using the Goodman and Weaver affine-invariant ensemble Markov chain Monte
    Carlo method provided by the ``emcee`` Python package, written by Daniel
    Foreman-Mackey.
    """
    def __init__(
            self,
            out_samples: PosteriorSamples,
            poisson_mean: PoissonMean,
            detection_likelihoods: typing.Sequence[DetectionLikelihood],
            log_prior_fn: typing.Callable[...,Numeric],
            before_prior_aux_fn: typing.Optional[AuxFnType]=None,
            after_prior_aux_fn: typing.Optional[AuxFnType]=None,
            args: typing.Optional[list]=None,
            kwargs: typing.Optional[dict]=None,
            moves=None, # TODO: add type annotation
            rescale_params: typing.Optional[typing.Dict[str,str]]=None,
            xpy: types.ModuleType=numpy,
            dtype=numpy.float64,
            random_state: typing.Optional[numpy.random.RandomState]=None,
            n_threads: int=1,
            pool=None,
            runtime_sortingfn=None,
            verbose: bool=False,
        ):
        import emcee

        super().__init__(
            out_samples,
            poisson_mean,
            detection_likelihoods,
            log_prior_fn,
            before_prior_aux_fn=before_prior_aux_fn,
            after_prior_aux_fn=after_prior_aux_fn,
            args=args, kwargs=kwargs,
            xpy=xpy,
            dtype=dtype,
            random_state=random_state,
        )

        if pool is None:
            from .utils import VectorizedPool
            pool = VectorizedPool()

        # Handle API changes between emcee 2.x and 3.x
        if int(emcee.__version__.split(".")[0]) >= 3:
            debug_verbose(
                "Using emcee>=3.0 API features",
                mode="sampler", flush=True,
            )
            # Provide default `moves` value.
            if moves is None:
                debug_verbose(
                    "Defaulting to StretchMove as no moves provided",
                    mode="sampler", flush=True,
                )
                moves = [(emcee.moves.StretchMove(), 1.0)]

            # Replace moves with rescaled moves if requested.
            if rescale_params is not None:
                debug_verbose(
                    "Rescaling some parameters for provided moves",
                    mode="sampler", flush=True,
                )
                log_scale_indices = []
                for param_name, scaling_name in rescale_params.items():
                    if scaling_name != "log":
                        raise ValueError("Only support log-scaling moves now.")
                    index = self.variable_names.index(param_name)

                    if index < 0:
                        raise ValueError(
                            "Requested rescaled key '{}' is not a variable."
                            .format(param_name)
                        )

                    log_scale_indices.append(index)
                    debug_verbose(
                        "Log-scaling parameter", param_name, "at index", index,
                        mode="sampler", flush=True,
                    )

                moves = [
                    (LogScaledRedBlueMove(move, log_scale_indices), weight)
                    for move, weight in moves
                ]
                debug_verbose(
                    "After rescaling, moves list is now", moves,
                    mode="sampler", flush=True,
                )


            self._ensemble_sampler_extra_kwargs = {
                "moves" : moves,
                "vectorize" : True,
            }
            self._sample_method_extra_kwargs = {"store" : False}
        else:
            debug_verbose(
                "Using emcee<3.0 API features",
                mode="sampler", flush=True,
            )

            # `moves` not supported by emcee versions <3.0.
            if moves is not None:
                raise RuntimeError(
                    "`moves` not supported for emcee versions < 3.0.  "
                    "Installed version is {}.  Please either upgrade or set "
                    "`moves=None`."
                    .format(emcee.__version__)
                )

            if rescale_params is not None:
                raise RuntimeError(
                    "`rescale_params` uses the `moves` API, which is not "
                    "supported for emcee versions < 3.0.  "
                    "Installed version is {}.  Please either upgrade or set "
                    "`rescale_params=None`."
                    .format(emcee.__version__)
                )

            self._ensemble_sampler_extra_kwargs = {}
            self._sample_method_extra_kwargs = {"storechain" : False}

        self.n_threads = n_threads
        self.pool = pool
        self.runtime_sortingfn = runtime_sortingfn

        self.verbose = verbose


    def posterior_samples(
            self,
            n_samples: int,
        ) -> numpy.ndarray:
        import emcee
        import datetime
        import sys

        out_samples = typing.cast(PosteriorSamples, self.out_post)

        # Handle API change for `EnsembleSampler` between versions 2 and 3.
        if int(emcee.__version__.split(".")[0]) >= 3:
            extra_kwargs = {"vectorize" : True}
        else:
            extra_kwargs = {}

        sampler = emcee.EnsembleSampler(
            self.n_walkers, self.n_dim, self.log_posterior,
            threads=self.n_threads, pool=self.pool,
            runtime_sortingfn=self.runtime_sortingfn,
            **self._ensemble_sampler_extra_kwargs
        )

        sample_iter = sampler.sample(
            self.state,
            iterations=n_samples, rstate0=self.random_state,
            **self._sample_method_extra_kwargs
        )

        if self.verbose:
            progress_pct = 0
            def display_progress(p, s):
                print(
                    "{time}; Progress: {prog}%; Samples: {samp}".format(
                        time=datetime.datetime.now(), prog=p, samp=s,
                    ),
                    file=sys.stderr,
                )
                sys.stderr.flush()
            display_progress(progress_pct, 0)

        for i, result in enumerate(sample_iter):
            if self.verbose:
                new_progress_pct = i / n_samples * 100
                if new_progress_pct >= progress_pct + 1:
                    progress_pct = int(new_progress_pct)
                    display_progress(progress_pct, i)

            sample, log_post, rstate, log_prior = result

            out_samples.append(sample, log_post, log_prior)

        if self.verbose:
            display_progress(100, n_samples)

        return out_samples.get_samples(slice(-n_samples,None))

    @staticmethod
    def parse_moves_from_file(file: typing.TextIO) -> list:
        """
        Parse a JSON file specifying MCMC moves to use.
        """
        # Parse raw JSON file.
        import json
        import emcee.moves
        move_info = json.load(file)

        # Validate structure.
        if not isinstance(move_info, list):
            raise TypeError(
                "Moves file must describe a list of move specifiers"
            )

        # Initialize output.
        moves = []

        # Load in each move into the output list.
        for i, move_spec in enumerate(move_info):
            # Validate structure.
            if not isinstance(move_spec, dict):
                raise TypeError(
                    "Move specifier #{} is not a dictionary.".format(i)
                )
            # Validate keys.
            missing_keys = {"name", "weight"} - move_spec.keys()
            if len(missing_keys) != 0:
                raise KeyError(
                    "Move specifier #{} is missing keys: {}"
                    .format(i, ", ".join(missing_keys))
                )
            # Parse out the pieces of the move specifier.
            move_name = move_spec["name"]
            move_weight = move_spec["weight"]
            move_args = move_spec.get("args", [])
            move_kwargs = move_spec.get("kwargs", {})

            # Validate args is a list.
            if not isinstance(move_args, list):
                raise ValueError(
                    "`args` for move specifier #{} is not a list."
                    .format(i)
                )
            # Validate kwargs is a list.
            if not isinstance(move_kwargs, dict):
                raise ValueError(
                    "`kwargs` for move specifier #{} is not a dict."
                    .format(i)
                )

            # If move has dots in it, it will require code that's not yet
            # implemented to import the appropriate move.
            if "." in move_name:
                raise NotImplementedError(
                    "Move {} cannot be used, as arbitrary moves not yet "
                    "supported.  Please only use moves from `emcee.moves`."
                )

            # Parse out the move.
            move_cls = getattr(emcee.moves, move_name)
            move = move_cls(*move_args, **move_kwargs)

            # Store the move and its weight.
            moves.append((move, move_weight))

        # Return final list of moves.
        return moves
