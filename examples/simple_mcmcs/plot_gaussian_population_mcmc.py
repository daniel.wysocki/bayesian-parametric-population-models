r"""
Gaussian population (MCMC)
==========================

A trivial example of using the PopModels population inference framework; return
to the installation page to get started.  This example shows how to use several
classes: ``Detection``, ``Population``, ``PoissonMean``,
``ReweightedPosteriorsDetectionLikelihood``,
``PopulationInferenceEmceeSampler``, ``H5RawPosteriorSamples``, and the
``Coordinate`` and ``CoordinateSystem`` framework.


This example consists of a population with one property ("mass" or "x"), whose
event rate and "mass distribution" parameters will be determined by fair draws.
The x distribution is assumed to be gaussian distributed with unknown mean and
(known) unit variance; the event rate is "1 per unit time" and we observe for
10 time units.  Inference assumes a uniform prior in :math:`x` and rate.

The plot produced at the end shows:

* Top panel: Fair draws of :math:`x` (thin lines), from underlying :math:`x`
  distribution (solid curve)
* Corner plot: The inferred shape parameter :math:`M` and rate
  :math:`\mathcal{R}`, both as one-dimensional marginal distributions, and as
  samples from the 2-D posterior.
* Bottom panels: The raw MCMC chains from this run, showing the
  :math:`\log(\mathrm{posterior}) + \mathrm{constant}` for each sample on top,
  and the parameters on the remaining panels.  One curve is shown for each
  "walker" in the chain.

The code  works as follows, referring to the ingredients on the main documentation page  (https://bayesian-parametric-population-models.readthedocs.io/)

* Define the population (``SimplePopulation``)
* Define the expected number of events
* Define a detection and parameter inference model (=perfect knowledge)
* Pick synthetic parameters, and define a synthetic population
* Define the likelihood of the observations, for the events
  (``detection_likelihoods``)
* Draw samples from the posterior distribution via MCMC
  * Chains are initialized by drawing from the prior
  * Samples are stored in a multi-file HDF5-based format, which splits samples
    across multiple files, in order to reduce odds of data corruption, and to
    allow for easy checkpointing / extending runs.  To demonstrate the
    splitting on such a small sample size, we make the size of these sub-files
    small.
  * To demonstrate checkpointing, the code is run for a number of samples, and
    then stopped/closed.  Then the output file is opened again and we resume
    where we left off.
* Plot the joint and marginal posterior distributions
"""

################################################################################
# Import parts of the scientific Python stack used in this example.

import numpy
import matplotlib
matplotlib.use("Agg")
from matplotlib import gridspec
import matplotlib.pyplot as plt
import scipy.stats

import pprint

numpy.set_printoptions(threshold=10)

################################################################################
# Defining the coordinate system
################################################################################
# Every use case will need to define the coordinates used by first creating
# instances of the ``Coordinate`` class for each coordinate, and then combining
# them with a ``CoordinateSystem`` instance.  In this example it seems like
# un-necessary overhead, but in more complicated examples (TODO: will link to
# one in the future) it will allow you to do things like combine mass and spin
# distributions, create mixture populations where one has parameters the other
# doesn't, and more, all with an automated system that ensures argument
# positions line up properly.
from pop_models.coordinate import Coordinate, CoordinateSystem

# Define a single coordinate called "x"
X = Coordinate("x")
# Define a 1-dimensional coordinate system containing only "x"
coord_system = CoordinateSystem(X)


################################################################################
# Defining the population
################################################################################
# The ``Population`` class is how we define the source population we're
# modeling.  There are a number of subclasses available for more complicated
# use cases (TODO: link to API), but in this simple case the base class
# suffices.
#
# We chose here to define a simple population model, with a Gaussian probability
# distribution, with known (unit) variance, and an unknown mean :math:`M`.
#
# .. math::
#    \mathrm{pdf}(x; M) =
#    \frac{1}{\sqrt{2 \pi}}
#    \exp\left( -\frac{1}{2} (x - M)^2 \right)
#
# And this population is scaled to an overall event rate of :math:`\mathcal{R}`.
#
# Whatever model you end up using, it will need to be a subclass of
# ``Population``.  These subclasses need to do several things, some of which are
# optional.
#
# Every ``Population`` needs to define a ``normalization`` method,
# which takes the parameters of the model (a dictionary mapping parameter names
# to values of those parameters) and returns the normalization of the intensity
# function (which is equivalent to the PDF scaled by the overall number in your
# search-space, i.e., your event rate, regardless of detectability).  It must
# also take an argument called ``where``, which allows the caller to provide an
# array of indices that it does and doesn't care about.  This exists solely for
# optimization purposes, and allows you to leave junk values in places where
# it's ``False``.  For simplicity we do no optimization here, and ``where`` is
# simply ignored.  It's also possible to have additional keyword arguments
# passed via `**kwargs``, but we do not make use of it here.
#
# Depending on how the ``Population`` object is used in your inference, you must
# also define at least one of two methods: ``pdf`` and ``rvs``.  In our specific
# example, only ``pdf`` is needed, so ``rvs`` is left undefined, and will raise
# a ``NotImplementedError`` if called.
#
# The ``pdf`` method takes, in addition to what ``normalization`` took, an
# ``observables`` argument.  This will be a list, with one entry for each
# ``Coordinate`` in the ``CoordinateSystem`` used, and those entries will
# contain an array of values of those coordinates.  The arrays may come in
# arbitrary shapes, and will likely be multi-dimensional, so the method should
# be vectorized.  In general, the observables will come in a multi-dimensional
# shape ``O``, and the parameters will come in a multi-dimensional shape ``P``,
# and the return value of ``pdf`` should be in the even higher-dimensional shape
# ``P + O``.
from pop_models.population import Population

param_names = ("rate", "mean")

class SimplePopulation(Population):
    """
    Defines a simple population with a single observable ("x"), which obey a
    Gaussian distribution with unknown mean "mean" and known unit variance.
    Normalization of this distribution is given by an unknown parameter "rate".
    """
    def __init__(self):
        # No special information needed, just pass the CoordinateSystem to the
        # parent's constructor method.
        super().__init__(coord_system, param_names)

    def normalization(self, parameters, where=True, **kwargs):
        # The normalization of the intensity function is just the "rate"
        # parameter.
        return parameters["rate"]

    def pdf(self, observables, parameters, where=True, **kwargs):
        # First argument is a list of observables, but there's only one, "x", so
        # we pull that out of the list.
        x, = observables
        # The only parameter that affects the PDF is the "mean", so we extract
        # that.
        mean = parameters["mean"]

        # PDF is a normal distribution with unit variance.  This is the
        # functional form for such a distribution.  Note that by using
        # `numpy.subtract.outer(mean, x)` instead of `mean - x`, we compute
        # all combinations of `mean[i_1,...,i_n] - x[j_1,...,j_m]`, and the
        # shape of the output array is `numpy.shape(mean) + numpy.shape(x)`.
        # Handling array shapes like this is a requirement of the PopModels API.
        delta = numpy.subtract.outer(mean, x)
        return (
            numpy.exp(-0.5*numpy.square(delta)) /
            numpy.sqrt(2.0*numpy.pi)
        )

################################################################################
# Initialize our ``Population`` object.
population = SimplePopulation()


################################################################################
# Defining the average detection rate
################################################################################
# In order to properly infer the event rate, we must define a subclass of
# ``PoissonMean`` which, once provided with our population model and any
# information about the survey performed, can evaluate the expected number of
# detections.  This may either make use of a population's ``pdf`` or ``rvs``
# methods, and is one of the factors in whether a ``Population`` needs to define
# either of those.
#
# For simplicity, we assume a perfect survey, with the only limiting factor
# being the observing time (``obs_time``).  We therefore need only know the
# ``Population``'s ``normalization``, though for any practical use-case this is
# unlikely to be the case.
#
# This object will be called as if it were a function (and in fact, one could
# simply define a function instead, short-circuiting the object-oriented
# approach), so one must define the ``__call__`` method such that it returns the
# expected number of detections, given the population parameters.
from pop_models.poisson_mean import PoissonMean

class SimpleExpectedDetections(PoissonMean):
    """
    We assume a perfect survey, so the expected number of detections is just the
    event rate multiplied by the observing time.
    """
    def __init__(self, obs_time, population):
        super().__init__(population)
        self.obs_time = obs_time

    def __call__(self, parameters, where=True, **kwargs):
        return (
            self.population.normalization(parameters, where=where) *
            self.obs_time
        )

################################################################################
# Set the observing time for the "survey".
obs_time = 10.0

################################################################################
# Initialize our ``PoissonMean`` object.
expval = SimpleExpectedDetections(obs_time, population)


################################################################################
# Defining the detections
################################################################################
# We need to represent the objects detected by defining a subclass of
# ``Detection``.  Depending on our use case, this must implement one of two
# methods: ``posterior_samples`` or ``likelihood``.
#
# For simplicity, we assume our detections come with perfect precision, and
# therefore the ``likelihood`` method would be a delta function, so we must
# instead define ``posterior_samples``, which will in fact always return the
# same value.
#
# Since this is a hierarchical Bayesian model, we must not double-prior, so when
# we use these posterior samples, it is essential to know the associated prior
# probability for each sample.  In the case of a delta-function posterior, any
# compatible prior will do, so it doesn't really matter what we provide, but
# PopModels doesn't actually know we have perfect measurements.  We thus make
# use of one special-case prior: a uniform prior.  PopModels knows that if
# ``None`` is returned in the prior slot, rather than an array of numbers, that
# the prior is uniform, and actually becomes more efficient.
from pop_models.detection import Detection

class SimpleDetection(Detection):
    """
    We assume detections with perfect measurements, so all posterior samples are
    of the correct value, and if the `likelihood` method were implemented, it
    would be a delta function at the true value (which would not integrate well
    numerically, so we must use `posterior_samples` since that makes a Monte
    Carlo integral perfectly adaptive).
    """
    def __init__(self, x_true):
        # Parent constructor needs to know the coordinate system.
        super().__init__(coord_system)
        # In addition, we need to know the true value.
        self.x_true = x_true

    def posterior_samples(self, n_samples, random_state=None):
        # Return the true value repeated once for each requested sample.
        x_samples = numpy.broadcast_to(self.x_true, n_samples)
        # CoordinateSystem is 1-D so we return a 1-element list.
        post_samples = [x_samples]
        # Specify that the prior is uniform.
        prior_samples = None

        return post_samples, prior_samples


################################################################################
# Seed the random number generator
################################################################################
seed = 15
random = numpy.random.RandomState(seed)


################################################################################
# Specify true population
################################################################################
# Specify the parameters of the true population.
rate_true = 1.0
mean_true = 0.0

################################################################################
# Store the true parameters in a ``Parameters`` dictionary.
parameters_true = {"rate": rate_true, "mean": mean_true}

################################################################################
# Compute :math:`\mu` for the true values -- this tells us the average number of
# detections we should make in our survey.
expval_true = expval(parameters_true)

################################################################################
# Generate observations
################################################################################
# Take one random (seeded for reproducibility) Poisson draw for the number of
# detections.  On average we will get ``expval_true`` detections, but may be
# higher or lower according to the randomness of the Poisson distribution.
n_detections = scipy.stats.poisson(expval_true).rvs(1, random_state=random)

################################################################################
# Draw the true values of the detected objects.
x_truths = (
    scipy.stats.norm(mean_true, 1.0)
    .rvs(n_detections, random_state=random)
)
################################################################################
# .. warning::
#    In a realistic use-case with selection biases, these would have to be drawn
#    and then potentially rejected according to the selection function.



################################################################################
# Plot the population and detections
n_plotting = 500

x_min_plotting = -10.0
x_max_plotting = +10.0

x_plotting = numpy.linspace(x_min_plotting, x_max_plotting, n_plotting)

fig_pop, ax_pop = plt.subplots(figsize=(6,4))

# Plot the population's intensity function.
ax_pop.plot(
    x_plotting,
    population.intensity((x_plotting,), parameters=parameters_true),
)
_, ax_pop_ymax = ax_pop.get_ylim()

# Plot the detected events, and where they lie on the population.
for x_truth in x_truths:
    ax_pop.plot(
        [x_truth, x_truth], [0.0, 0.1*ax_pop_ymax],
        color="black", linestyle="solid", alpha=0.4,
    )

ax_pop.set_xlabel(r"$x$")
ax_pop.set_ylabel(r"$\rho(x | \Lambda_{\mathrm{true}})$")

fig_pop.show()

################################################################################
# Convert the detections' true values into instances of our ``SimpleDetection``
# class defined earlier.
detections = [SimpleDetection(x_true) for x_true in x_truths]

################################################################################
# PopModels only makes use of detections through what we call a
# ``DetectionLikelihood``.  This is a function :math:`\mathcal{D}_n(\Lambda)`
# which, for the :math:`n`th detection, and population parameters
# :math:`\Lambda`, returns
#
# .. math::
#    \mathcal{D}_n(\Lambda) \equiv
#    \int \ell_n(\lambda) \rho(\lambda \mid \Lambda)
#
# Here we choose to implement this via Monte Carlo draws from the detection's
# posterior, weighted by the inverse prior, rather than working directly with
# the likelihood :math:`\ell_n`.  The
# ``ReweightedPosteriorsDetectionLikelihood`` class already implements this
# approach, and assumes you pass it a ``Population`` object which implements
# ``pdf``, and a ``Detection`` object which implements ``posterior_samples``.
#
# Since all of the posterior samples are the same, our integrator need only draw
# a single sample to get an accurate answer.  Though we set the integrator to
# draw two samples to avoid a division-by-zero that crops up.
from pop_models.detection import ReweightedPosteriorsDetectionLikelihood
from pop_models.integrators import AdaptiveMCIntegrator

mc_integrator = AdaptiveMCIntegrator(
    iter_start=1, iter_max=2,
)

detection_likelihoods = [
    ReweightedPosteriorsDetectionLikelihood(
        population, detection,
        mc_integrator=mc_integrator,
    )
    for detection in detections
]


################################################################################
# MCMC settings
################################################################################
# Number of "walkers" to use.
n_walkers = 8

################################################################################
# Number of samples to draw.
# Note that we draw samples in two batches, so we define half the number of
# samples here.
half_n_samples = 512

n_samples = 2*half_n_samples


################################################################################
# Settings for output files
################################################################################
# Directory to store raw posterior samples in.
output_directory = "posterior_samples"
# File to store cleaned posterior samples in.
output_samples_filename = "posterior_samples.hdf5"

################################################################################
# Maximum number of posterior samples to store in each file.
chunk_size = 64
################################################################################
# .. warning::
#    This is a very small chunk size, only done so multiple files can be
#    demonstrated for this example.  A more reasonable number would be 1024.
#    The higher you go, the more efficient everything will be, but also the more
#    possible data loss you may face if one chunk gets corrupted.


################################################################################
# Prior distribution
################################################################################
# Set the bounds for the prior on the parameters.  Note that this has already
# been tuned to contain the region with non-negligible probability.
rate_min_prior = 0.01
rate_max_prior = 3.50
mean_min_prior = -1.5
mean_max_prior = +1.0

################################################################################
# Define the ``log_prior`` function.
def log_prior(parameters, *args, **kwargs):
    """
    Assume a prior uniform in rate and mean.
    """
    rate = parameters["rate"]
    mean = parameters["mean"]

    has_support = (
        (rate >= rate_min_prior) &
        (rate <= rate_max_prior) &
        (mean >= mean_min_prior) &
        (mean <= mean_max_prior)
    )

    return numpy.where(has_support, 0.0, -numpy.inf)

################################################################################
# Initialize MCMC walkers by drawing from the prior.
rate_init = random.uniform(rate_min_prior, rate_max_prior, n_walkers)
mean_init = random.uniform(mean_min_prior, mean_max_prior, n_walkers)
init_state = numpy.column_stack((rate_init, mean_init))


################################################################################
# Sampling the population posterior
################################################################################
# To produce posterior samples, we need an instance of one of
# ``PosteriorSamples``'s sub-classes.  Here we demonstrate the most robust
# version available, ``H5RawPosteriorSamples``, which uses an HDF5 backend.
from pop_models.posterior import H5RawPosteriorSamples

# Create object for storing posterior samples, with an HDF5 backend.
post_samples = H5RawPosteriorSamples.create(
    output_directory,
    population.param_names,
    init_state,
    chunk_size=chunk_size,
)

################################################################################
# Notice it has created a new directory containing two files.
# ``master.hdf5`` is the entry point, which links to a growing number of
# files named ``subfile-XYZ.hdf5``.  Right now we only have
# ``subfile-000.hdf5``, which contains a single posterior sample -- the initial
# state of the MCMC chain.  As we sample the posterior further, this will grow.
post_samples.summary()

################################################################################
# Now we create a ``PopulationInference`` object, which is the workhorse of
# PopModels.  In this specific example, we want to use an MCMC-based approach,
# and use the ``emcee`` Python library to do this, which is already implemented
# in the ``PopulationInferenceEmceeSampler`` class.  If you would prefer to use
# a different method, feel free to subclass ``PopulationInference``, and in
# particular for sampling you should subclass ``PopulationInferenceSampler``.
from pop_models.posterior import PopulationInferenceEmceeSampler

pop_inference = PopulationInferenceEmceeSampler(
    post_samples,
    expval, detection_likelihoods, log_prior,
    random_state=random,
    verbose=True,
)

################################################################################
# Draw the first half of our desired set of posterior samples.
samples = pop_inference.posterior_samples(half_n_samples)

################################################################################
# Now notice that there are more sub-files.
post_samples.summary()

################################################################################
# Even though ``pop_inference.posterior_samples`` returns the samples, they can
# also be accessed from the ``H5RawPosteriorSamples`` object, since everything
# is stored in an HDF5 file.  Here we grab all the samples.  Note the argument
# ``slice(-half_n_samples, None)`` is like indexing an array with
# ``[-half_n_samples:]``, which gives the last ``half_n_samples`` entries.
samples_equiv = post_samples.get_samples(slice(-half_n_samples, None))

################################################################################
# Assert that they're the same.  This example would crash if they weren't
# precisely equal.
numpy.testing.assert_equal(
    samples,
    samples_equiv,
)

################################################################################
# Note we can also access the `init_state` array, which is the first sample.
init_state_equiv = post_samples.get_samples(0)

################################################################################
# Assert that they're precisely equal.
numpy.testing.assert_equal(
    init_state,
    init_state_equiv,
)

################################################################################
# We can also grab all samples, including the initial state, by using `...`
samples_all = post_samples.get_samples(...)

################################################################################
# Assert that all the subsets match
numpy.testing.assert_equal(
    init_state,
    samples_all[0],
)
numpy.testing.assert_equal(
    samples,
    samples_all[1:],
)

################################################################################
# Now to demonstrate checkpointing, we close out of the
# ``H5RawPosteriorSamples`` file, and re-open it.  One could imagine running
# their code for a while, then performing some diagnostics which determined more
# samples were needed, and then launching the code where it was left off.
#
# Note that you should open it with the ``"r+"`` mode if you want to continue
# sampling.  Otherwise you can use the ``"r"`` mode for read-only access.

# Close the old ``H5RawPosteriorSamples`` object.
post_samples.close()
# Create a new ``H5RawPosteriorSamples`` object, picking up where we left off.
post_samples = H5RawPosteriorSamples(output_directory, "r+")
# Create another ``PopulationInference`` object, which doesn't care if this is
# the beginning of a new run, or a resuming of an old run.  It just uses the
# last sample in ``post_samples`` as a starting point.
pop_inference = PopulationInferenceEmceeSampler(
    post_samples,
    expval, detection_likelihoods, log_prior,
    random_state=random,
    verbose=True,
)

################################################################################
# Draw the other half of the samples we want.
pop_inference.posterior_samples(half_n_samples)

################################################################################
# Now notice that there are even more sub-files.
post_samples.summary()


################################################################################
# Accessing probabilities for each posterior sample
################################################################################
# We can obtain the log(posterior) value associated with each sample (up to a
# normalizing constant), and likewise the log(prior).
log_post_samples = post_samples.get_posterior_log_prob(slice(-n_samples, None))
log_prior_samples = post_samples.get_prior_log_prob(slice(-n_samples, None))

################################################################################
# The prior was uniform, and the normalizing constant was chosen so that the
# log(prior) would be zero everywhere.  So we assert that this is recovered.
assert numpy.count_nonzero(log_prior_samples) == 0

################################################################################
# Making use of our posterior samples
################################################################################
# We can get all the parameters we want wrapped in a dictionary mapping
# param_name :math:`\to` samples of parameter.  The same indexing convention as
# ``get_samples`` is used.
parameter_samples = post_samples.get_params(slice(-10, None))

pprint.pprint(parameter_samples)

################################################################################
# Plot the MCMC chains
fig_chains, axes_chains = post_samples.plot_chains()

fig_chains.show()


################################################################################
# Cleaning chains
################################################################################
# Finite-length MCMC chains suffer from burnin, meaning the samples drawn early
# on are not representative of the posterior distribution, and one must let the
# sampler explore the space before trusting the samples.  The Markov chain
# aspect of the algorithm also means samples with few iterations between them
# are highly correlated, so the effective sample size is actually lower than
# it seems, and one may choose to thin the chains, only taking one sample for
# each auto-correlation length.
#
# While there are tools in PopModels for doing this robustly, the chains in this
# demo are too short, and would likely have every sample discarded.  So instead
# we will thin the chains by hand, removing the first 60% of samples, and
# ignoring the possibility of auto-correlation by setting the thinning to 1.
from pop_models.posterior import H5CleanedPosteriorSamples

post_samples_cleaned = H5CleanedPosteriorSamples.from_raw_manual(
    output_samples_filename,
    post_samples,
    burnin=int(0.6*n_samples), thinning=1,
)

################################################################################
# Plot the posterior distribution from these samples.
fig_post = post_samples_cleaned.plot_corner(
    levels=[0.50, 0.90], truths=parameters_true,
)

fig_post.show()
