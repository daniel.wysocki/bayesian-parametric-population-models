The pop_models API
==================

:Release: |version|
:Date: |today|

.. toctree::
   :maxdepth: 3

   pop_models.detection
   pop_models.poisson_mean
   pop_models.population
   pop_models.posterior
   pop_models.prob
   pop_models.sample_extraction
   pop_models.sample_extraction.max_posterior
   pop_models.sample_extraction.integrated_acorr
   pop_models.stats
   pop_models.utils
   pop_models.volume

   pop_models.applications
   pop_models.applications.bbh
   pop_models.applications.bbh.O2_model_AB
   pop_models.applications.bbh.broken_powerlaw_simple
   pop_models.applications.bbh.mixture_pl_gauss
   pop_models.applications.bns
   pop_models.applications.bns.iid_gaussian_mass_with_eos
   pop_models.astro_models
   pop_models.astro_models.building_blocks
   pop_models.astro_models.eos
   pop_models.astro_models.gw_ifo_vt
