Installation
============

This package can be installed like any other standard Python package, by downloading the source code, and running the ``setup.py`` script (after ensuring all dependencies described therein have been obtained). However, we recommend installing it with the ``pip`` package manager, included in most standard Python distributions (`link <https://pypi.python.org/pypi/pip>`_). This will download most dependencies for you. You will still need to install `LALSuite <https://wiki.ligo.org/DASWG/LALSuite>`_ yourself in order to use the (optional) module :py:mod:`pop_models.vt`, and you will need specifically the O2 build, as some API's have changed.

.. note::

   If you are familiar with ``pip``, you may be used to simply telling it the name of a package, and having it download the package for you. However, this package is currently not indexed in any of the standard places ``pip`` looks (e.g., `PyPI <pypi.python.org>`_), so you will need to download the source code yourself, and run ``pip`` from within the source directory.

To install with ``pip``, first clone the repository somewhere on your machine.

.. code-block:: bash

   git clone git@git.ligo.org:daniel.wysocki/bayesian-parametric-population-models.git

Now enter the directory you've cloned to

.. code-block:: bash

   cd bayesian-parametric-population-models

And run ``pip`` to install it. You may want to provide different options to ``pip`` than we do (``-U`` tells it to upgrade any existing installs)

.. code-block:: bash

   pip install -U .

If you do not have permissions to install to ``pip``'s default install location on your machine, you can either obtain permissions if possible (e.g., use ``sudo``), or install to a local user repository by instead running the following

.. code-block:: bash

   pip install --user -U .


And now you should be ready to go. Test that you installed correctly by opening a Python shell and trying to import the library

.. code-block:: bash

   python -c "import pop_models"

and also by running one of the command line scripts

.. code-block:: bash

   pop_models_powerlaw_mcmc --help
