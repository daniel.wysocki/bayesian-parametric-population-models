Sample Extraction Tool
======================

.. argparse::
    :module: pop_models.sample_extraction.cli
    :func: make_parser
    :prog: pop_models_extract_samples
