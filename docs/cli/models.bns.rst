Models for binary neutron star populations
==========================================

:Release: |version|
:Date: |today|

.. toctree::
   :maxdepth: 1

   models.bns.iid_gaussian_mass_with_eos
