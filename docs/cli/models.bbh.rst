Models for binary black hole populations
========================================

:Release: |version|
:Date: |today|

.. toctree::
   :maxdepth: 1

   models.bbh.O2_model_AB
   models.bbh.broken_powerlaw_simple
   models.bbh.mixture_pl_gauss
